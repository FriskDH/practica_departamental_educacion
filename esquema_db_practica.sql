USE [master]
GO
/****** Object:  Database [proyecto]    Script Date: 1/7/2021 12:19:47 AM ******/
CREATE DATABASE [proyecto]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'proyecto', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\proyecto.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'proyecto_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\proyecto_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [proyecto].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [proyecto] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [proyecto] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [proyecto] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [proyecto] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [proyecto] SET ARITHABORT OFF 
GO
ALTER DATABASE [proyecto] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [proyecto] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [proyecto] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [proyecto] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [proyecto] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [proyecto] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [proyecto] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [proyecto] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [proyecto] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [proyecto] SET  DISABLE_BROKER 
GO
ALTER DATABASE [proyecto] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [proyecto] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [proyecto] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [proyecto] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [proyecto] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [proyecto] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [proyecto] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [proyecto] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [proyecto] SET  MULTI_USER 
GO
ALTER DATABASE [proyecto] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [proyecto] SET DB_CHAINING OFF 
GO
ALTER DATABASE [proyecto] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [proyecto] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [proyecto] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [proyecto] SET QUERY_STORE = OFF
GO
USE [proyecto]
GO
/****** Object:  Table [dbo].[departamentos]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[departamentos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [int] NOT NULL,
	[departamento] [varchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detalle_visitas]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalle_visitas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_visitas] [int] NOT NULL,
	[fk_id_oficina] [int] NOT NULL,
	[fk_id_funcion] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[edificios]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[edificios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[edificio] [varchar](50) NOT NULL,
 CONSTRAINT [PK__edificio__3213E83F4589E70F] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[funcionarios]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[funcionarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dpi] [bigint] NOT NULL,
	[nombre1] [varchar](15) NOT NULL,
	[nombre2] [varchar](15) NULL,
	[apellido1] [varchar](15) NOT NULL,
	[apellido2] [varchar](15) NULL,
	[fk_id_oficina] [int] NOT NULL,
	[pass] [varbinary](max) NOT NULL,
	[fk_id_rol] [int] NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK__funciona__3213E83FAFFD2FE9] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__funciona__D8761965BC67881D] UNIQUE NONCLUSTERED 
(
	[dpi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[funciones]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[funciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[funcion] [varchar](550) NOT NULL,
 CONSTRAINT [PK__funcione__3213E83FC7B74EE4] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[funciones_funcionarios]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[funciones_funcionarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_funciones_FuncFunc] [int] NOT NULL,
	[fk_id_funcionarios_FuncFunc] [bigint] NOT NULL,
 CONSTRAINT [PK__funcione__3213E83FBF5DEC74] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[funciones_oficinas]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[funciones_oficinas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_funciones] [int] NOT NULL,
	[fk_id_oficinas] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[funciones_requerimientos]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[funciones_requerimientos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_funciones] [int] NOT NULL,
	[fk_id_requerimientos] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[modulos]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modulos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modulo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_modulos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[modulos_roles]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modulos_roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_modulos] [int] NOT NULL,
	[fk_id_roles] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[municipios]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[municipios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [int] NOT NULL,
	[municipio] [varchar](60) NOT NULL,
	[fk_id_departamentos] [int] NOT NULL,
 CONSTRAINT [PK__municipi__3213E83FBA6D29BD] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[numero_reporte]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[numero_reporte](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[oficinas]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oficinas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[oficina] [varchar](50) NOT NULL,
	[fk_id_edificio] [int] NOT NULL,
 CONSTRAINT [PK__oficinas__3213E83F1EDBC329] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[requerimientos]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[requerimientos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[papeleria] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rol] [varchar](50) NOT NULL,
 CONSTRAINT [PK__roles__3213E83F1B1CB20C] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[visitantes]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[visitantes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dpi] [bigint] NOT NULL,
	[huella] [varbinary](max) NOT NULL,
	[nombre1] [varchar](15) NOT NULL,
	[nombre2] [varchar](15) NULL,
	[apellido1] [varchar](15) NOT NULL,
	[apellido2] [varchar](15) NULL,
	[fk_id_municipio] [int] NOT NULL,
	[telefono] [varchar](8) NOT NULL,
 CONSTRAINT [PK_visitantes] PRIMARY KEY CLUSTERED 
(
	[dpi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__visitant__D87619653390182B] UNIQUE NONCLUSTERED 
(
	[dpi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[visitas]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[visitas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_dpi_visitantes] [bigint] NOT NULL,
	[fecha] [date] NOT NULL,
	[hora_entrada] [time](7) NOT NULL,
 CONSTRAINT [PK__visitas__3213E83FE2F22791] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [dpi_index]    Script Date: 1/7/2021 12:19:47 AM ******/
CREATE NONCLUSTERED INDEX [dpi_index] ON [dbo].[funcionarios]
(
	[dpi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[detalle_visitas]  WITH CHECK ADD FOREIGN KEY([fk_id_visitas])
REFERENCES [dbo].[visitas] ([id])
GO
ALTER TABLE [dbo].[detalle_visitas]  WITH CHECK ADD FOREIGN KEY([fk_id_oficina])
REFERENCES [dbo].[oficinas] ([id])
GO
ALTER TABLE [dbo].[detalle_visitas]  WITH CHECK ADD FOREIGN KEY([fk_id_funcion])
REFERENCES [dbo].[funciones] ([id])
GO
ALTER TABLE [dbo].[funcionarios]  WITH CHECK ADD  CONSTRAINT [const_fk_id_oficina] FOREIGN KEY([fk_id_oficina])
REFERENCES [dbo].[oficinas] ([id])
GO
ALTER TABLE [dbo].[funcionarios] CHECK CONSTRAINT [const_fk_id_oficina]
GO
ALTER TABLE [dbo].[funcionarios]  WITH CHECK ADD  CONSTRAINT [FK_funcionarios_roles] FOREIGN KEY([fk_id_rol])
REFERENCES [dbo].[roles] ([id])
GO
ALTER TABLE [dbo].[funcionarios] CHECK CONSTRAINT [FK_funcionarios_roles]
GO
ALTER TABLE [dbo].[funciones_funcionarios]  WITH CHECK ADD  CONSTRAINT [FK_funciones_funcionarios_funcionarios] FOREIGN KEY([fk_id_funcionarios_FuncFunc])
REFERENCES [dbo].[funcionarios] ([dpi])
GO
ALTER TABLE [dbo].[funciones_funcionarios] CHECK CONSTRAINT [FK_funciones_funcionarios_funcionarios]
GO
ALTER TABLE [dbo].[funciones_funcionarios]  WITH CHECK ADD  CONSTRAINT [FK_funciones_funcionarios_funciones] FOREIGN KEY([fk_id_funciones_FuncFunc])
REFERENCES [dbo].[funciones] ([id])
GO
ALTER TABLE [dbo].[funciones_funcionarios] CHECK CONSTRAINT [FK_funciones_funcionarios_funciones]
GO
ALTER TABLE [dbo].[funciones_oficinas]  WITH CHECK ADD  CONSTRAINT [const_fk_id_funcionesOf] FOREIGN KEY([fk_id_funciones])
REFERENCES [dbo].[funciones] ([id])
GO
ALTER TABLE [dbo].[funciones_oficinas] CHECK CONSTRAINT [const_fk_id_funcionesOf]
GO
ALTER TABLE [dbo].[funciones_oficinas]  WITH CHECK ADD  CONSTRAINT [const_fk_id_oficinas] FOREIGN KEY([fk_id_oficinas])
REFERENCES [dbo].[oficinas] ([id])
GO
ALTER TABLE [dbo].[funciones_oficinas] CHECK CONSTRAINT [const_fk_id_oficinas]
GO
ALTER TABLE [dbo].[funciones_requerimientos]  WITH CHECK ADD  CONSTRAINT [const_fk_id_funciones] FOREIGN KEY([fk_id_funciones])
REFERENCES [dbo].[funciones] ([id])
GO
ALTER TABLE [dbo].[funciones_requerimientos] CHECK CONSTRAINT [const_fk_id_funciones]
GO
ALTER TABLE [dbo].[funciones_requerimientos]  WITH CHECK ADD  CONSTRAINT [const_fk_id_requerimientos] FOREIGN KEY([fk_id_requerimientos])
REFERENCES [dbo].[requerimientos] ([id])
GO
ALTER TABLE [dbo].[funciones_requerimientos] CHECK CONSTRAINT [const_fk_id_requerimientos]
GO
ALTER TABLE [dbo].[modulos_roles]  WITH CHECK ADD  CONSTRAINT [FK_modulos_roles_modulos] FOREIGN KEY([fk_id_modulos])
REFERENCES [dbo].[modulos] ([id])
GO
ALTER TABLE [dbo].[modulos_roles] CHECK CONSTRAINT [FK_modulos_roles_modulos]
GO
ALTER TABLE [dbo].[modulos_roles]  WITH CHECK ADD  CONSTRAINT [FK_modulos_roles_roles] FOREIGN KEY([fk_id_roles])
REFERENCES [dbo].[roles] ([id])
GO
ALTER TABLE [dbo].[modulos_roles] CHECK CONSTRAINT [FK_modulos_roles_roles]
GO
ALTER TABLE [dbo].[municipios]  WITH CHECK ADD  CONSTRAINT [cont_fk_id_departamentos] FOREIGN KEY([fk_id_departamentos])
REFERENCES [dbo].[departamentos] ([id])
GO
ALTER TABLE [dbo].[municipios] CHECK CONSTRAINT [cont_fk_id_departamentos]
GO
ALTER TABLE [dbo].[oficinas]  WITH CHECK ADD  CONSTRAINT [FK__oficinas__fk_id___15702A09] FOREIGN KEY([fk_id_edificio])
REFERENCES [dbo].[edificios] ([id])
GO
ALTER TABLE [dbo].[oficinas] CHECK CONSTRAINT [FK__oficinas__fk_id___15702A09]
GO
ALTER TABLE [dbo].[visitantes]  WITH CHECK ADD  CONSTRAINT [cont_fk_id_municipio] FOREIGN KEY([fk_id_municipio])
REFERENCES [dbo].[municipios] ([id])
GO
ALTER TABLE [dbo].[visitantes] CHECK CONSTRAINT [cont_fk_id_municipio]
GO
ALTER TABLE [dbo].[visitas]  WITH CHECK ADD  CONSTRAINT [const_fk_dpi_visitantes] FOREIGN KEY([fk_dpi_visitantes])
REFERENCES [dbo].[visitantes] ([dpi])
GO
ALTER TABLE [dbo].[visitas] CHECK CONSTRAINT [const_fk_dpi_visitantes]
GO
/****** Object:  StoredProcedure [dbo].[adminUsuarios]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adminUsuarios]
@opc int,
@dpi bigint,
@funcion int,
@oficina int
as
begin

--SEARCH ALL USERS WITHOUT ANY FILTER
if @opc=1
begin
	select funcionarios.dpi, nombre1, nombre2, apellido1, apellido2, oficina, fk_id_oficina, funcionarios.fk_id_rol, rol
		from funcionarios
		inner join oficinas on fk_id_oficina=oficinas.id
		inner join roles on fk_id_rol=roles.id
		where estado=1;
end;

if @opc=11
begin
	select 
	funciones.funcion from funciones_funcionarios
	inner join funcionarios on fk_id_funcionarios_FuncFunc= funcionarios.id
	inner join funciones on fk_id_funciones_FuncFunc=funciones.id
	where estado=1;
	/*where funcionarios.dpi=@dpi;*/

end;
--HERE ENDS SEARCH WITHOUT ANY FILTER

--SEARCH ALL OFICINAS AND ALL FUNCIONES FILTERED ONLY BY DPI
if @opc=2
begin
	select funcionarios.dpi, nombre1, nombre2, apellido1, apellido2, oficina, fk_id_oficina, funcionarios.fk_id_rol, rol
		from funcionarios
		inner join oficinas on fk_id_oficina=oficinas.id
		inner join roles on fk_id_rol=roles.id
	where funcionarios.dpi=@dpi and estado=1;
end


/*
--SEARCH ALL OFICINAS FILTERED BY FUNCIONES AND DPI
if @opc=3
begin
end
*/
--SEARCH ALL FUNCIONES FILTERED BY OFICINA AND DPI
if @opc=4
begin
	select funcionarios.dpi, nombre1, nombre2, apellido1, apellido2, oficina, fk_id_oficina, funcionarios.fk_id_rol, rol
	from funcionarios
	inner join oficinas on fk_id_oficina=oficinas.id
	inner join roles on fk_id_rol=roles.id
	where funcionarios.dpi=@dpi and funcionarios.fk_id_oficina=@oficina and estado=1;;
end


--SEARCH ONLY BY OFICINA
if @opc=5
begin
	select funcionarios.dpi, nombre1, nombre2, apellido1, apellido2, oficina, fk_id_oficina, funcionarios.fk_id_rol, rol
	from funcionarios
	inner join oficinas on fk_id_oficina=oficinas.id
	inner join roles on fk_id_rol=roles.id
	where @oficina=fk_id_oficina and estado=1;
	
end

--SEARCH USER FUNCTIONS 
if @opc=6
begin
	select funciones.id/*, funciones.funcion*/
	from funciones_funcionarios
	inner join funciones on fk_id_funciones_FuncFunc= funciones.id
	--inner join funcionarios on fk_id_funcionarios_FuncFunc= funcionarios.dpi
	where funciones_funcionarios.fk_id_funcionarios_FuncFunc=@dpi;
end


end;
GO
/****** Object:  StoredProcedure [dbo].[crudDetalleVisitas]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudDetalleVisitas]
@opc int,
@idVisita int,
@idOficina int,
@idFuncion int
as
begin
	if @opc=1
	begin
		begin transaction
		insert into detalle_visitas values(@idVisita, @idOficina, @idFuncion);
		commit;
	end;
	
	/*SELECCIONAR SEGUN DPI DE PERSONA*/
	if @opc=2
	begin
		begin transaction
		select 
		fk_id_funcion, funcion, fk_id_oficina, oficina
		/*oficina*/
		from detalle_visitas
		inner join oficinas on fk_id_oficina=oficinas.id
		inner join funciones on fk_id_funcion=funciones.id
		where fk_id_visitas= @idVisita
		commit;
	end;

end;
GO
/****** Object:  StoredProcedure [dbo].[crudDptos]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[crudDptos]
@opc int,
@id int,
@codigo int,
@departamento varchar(25)
as
begin
	if @opc=1
	begin
		begin transaction
		insert into departamentos values(@codigo, @departamento);
		commit;
	end;

	if @opc=2
	begin
		begin transaction
		select * from departamentos where id=@id;
		commit;
	end;

	if @opc=3
	begin
		begin transaction
		update departamentos set codigo=@codigo, departamento=@departamento where id=@id;
		commit;
	end;
	
	if @opc=4
	begin
		begin transaction
		delete from departamentos where id=@id;
		commit;
	end;

	if @opc=5
	begin
		begin transaction
		select * from departamentos;
		commit;
	end;
end;
GO
/****** Object:  StoredProcedure [dbo].[crudEdificios]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[crudEdificios]
@opc int,
@id int,
@edificio varchar(80)
as
begin

if @opc=1
	begin
		begin transaction
			insert into edificios values(@edificio);
		commit;
	end;

if @opc=2
	begin
		select * from edificios;
	end;

if @opc=3
	begin
		begin transaction
			update edificios set edificio=@edificio where id=@id;
		commit;
	end;

if @opc=4
	begin
		begin transaction
			delete from edificios where id=@id;
		commit;
	end;
end;
GO
/****** Object:  StoredProcedure [dbo].[crudFuncionarios]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudFuncionarios]
@opc int,
@id int,
@dpi bigint,
@nombre1 varchar(15),
@nombre2 varchar(15),
@apellido1 varchar(15),
@apellido2 varchar(15),
@oficina int,
@passProv varchar(max),
@idRol int
as
begin
	if @opc=1
	begin
		begin transaction
		insert into funcionarios values(@dpi, @nombre1, @nombre2, @apellido1, @apellido2, @oficina, ENCRYPTBYPASSPHRASE('departamental$#645^&', @passProv), @idRol, 1);
		commit;
	end;

	if @opc=2
	begin
		begin transaction
		select * from funcionarios where dpi=@dpi and estado=1;
		commit;
	end;

	if @opc=3
	begin
		begin transaction
			update funcionarios set nombre1=@nombre1, nombre2=@nombre2, apellido1=@apellido1, apellido2=@apellido2,
			fk_id_oficina=@oficina, pass=ENCRYPTBYPASSPHRASE('departamental$#645^&', @passProv), fk_id_rol=@idRol where dpi=@dpi;
		
		commit;
	end;
	
	if @opc=4
	begin
		begin transaction
		--delete from funcionarios where dpi=@dpi;
			update funcionarios set estado=0 where dpi=@dpi;
		commit;
	end;

	if @opc=5
	begin
		select * from funcionarios where estado=1;
	end;

	--select para verificar login
	if @opc=6
	begin
		begin transaction
		--select @opc, @id, @dpi, @passProv;
		--select @passProv;
		select funcionarios.id, nombre1, apellido1, dpi, oficina, funcionarios.fk_id_rol, rol from funcionarios 
		inner join oficinas on fk_id_oficina=oficinas.id
		inner join roles on fk_id_rol=roles.id
		where estado=1 and dpi=@dpi and convert(varchar, DECRYPTBYPASSPHRASE('departamental$#645^&', pass))=@passProv;
		--convert (varchar, DECRYPTBYPASSPHRASE('departamental$#645^&', pass))
		commit;
	end;

	--update desde administracion usuarios (no cambiar dpi ni password)
	if @opc=7
	begin
		begin transaction
			update funcionarios set nombre1=@nombre1, nombre2=@nombre2, apellido1=@apellido1, apellido2=@apellido2,
			fk_id_oficina=@oficina, fk_id_rol=@idRol where dpi=@dpi;		
		commit;
	end;

	--update only password
	if @opc=8
	begin
		begin transaction
			update funcionarios set pass= ENCRYPTBYPASSPHRASE('departamental$#645^&', @passProv) where dpi=@dpi;		
		commit;
	end;

	--hacer inner join para mostrar la oficinas actual del funcionario y elegir del cmbBox la actual
	if @opc=9
	begin
		select oficinas.id, oficinas.oficina, edificio from funcionarios
		inner join oficinas on funcionarios.fk_id_oficina = oficinas.id
		inner join edificios on oficinas.fk_id_edificio = edificios.id
		where funcionarios.dpi= @dpi
	end;

end;
GO
/****** Object:  StoredProcedure [dbo].[crudFunciones]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudFunciones]
@opc int,
@id int,
@funcion varchar(550)
as
begin
	if @opc=1
	begin
		begin transaction
		insert into funciones values(@funcion);
		commit;
	end;

	if @opc=2
	begin
		begin transaction
		select * from funciones where id=@id;
		commit;
	end;

	if @opc=3
	--update also the funciones_oficinas table
	--update also de the funciones_requerimientos table
	begin
		begin transaction
		update funciones set funcion=@funcion where id=@id;
		--update funciones_oficinas set fk_id_oficinas=@idOficina where id=@id;
		commit;
	end;
	
	if @opc=4
	begin
		begin transaction
		delete from funciones where id=@id;
		commit;
	end;

	if @opc=5
	begin
		select * from funciones;
	end;

	if @opc=6
	begin
		select * from funciones where funcion like @funcion;
	end;
	
end;
GO
/****** Object:  StoredProcedure [dbo].[crudFunciones_Funcionarios]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudFunciones_Funcionarios]
@opc int,
@id int,
@idFuncion int,
@dpiFuncionario bigint
as
begin
	if @opc=1
	begin
		begin transaction
		insert into funciones_funcionarios values( @idFuncion, @dpiFuncionario );
		commit;
	end;

	--select por dpi de funcionario
	if @opc=2
	begin
		begin transaction
		select * from funciones_funcionarios where funciones_funcionarios.fk_id_funcionarios_FuncFunc = @dpiFuncionario;
		commit;
	end;

	if @opc=3
	begin
		begin transaction
		update funciones_funcionarios set fk_id_funcionarios_FuncFunc=@dpiFuncionario, 
		fk_id_funciones_FuncFunc=@idFuncion where id=@id;
		commit;
	end;
	
	if @opc=4
	begin
		begin transaction
		delete from funciones_funcionarios where id=@id;
		commit;
	end;

	if @opc=5
	begin
		begin transaction
		delete from funciones_funcionarios where fk_id_funcionarios_FuncFunc=@dpiFuncionario
		and fk_id_funciones_FuncFunc=@idFuncion;
		commit;
	end;

	--delete based on dpiFuncionario
	if @opc=6
	begin
		begin transaction
		delete from funciones_funcionarios where fk_id_funcionarios_FuncFunc=@dpiFuncionario
		commit;
	end;

	if @opc=7
	begin
		begin transaction
		select fk_id_funciones_FuncFunc, funcion from funciones_funcionarios
		inner join funciones on funciones_funcionarios.fk_id_funciones_FuncFunc=funciones.id
		where funciones_funcionarios.fk_id_funcionarios_FuncFunc=@dpiFuncionario
		commit;
	end;


end;
GO
/****** Object:  StoredProcedure [dbo].[crudFunciones_Oficinas]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudFunciones_Oficinas]
@opc int,
@id int,
@idFuncion int,
@idOficina int
as
begin
	if @opc=1
	begin
		begin transaction
		insert into funciones_oficinas values( @idFuncion, @idOficina );
		commit;
	end;

	if @opc=2
	begin
		select * from funciones_oficinas where fk_id_funciones=@idFuncion and fk_id_oficinas=@idOficina;
	end;

	if @opc=3
	--update also the funciones_oficinas table
	--update also de the funciones_requerimientos table
	begin
		begin transaction
		update funciones_oficinas set fk_id_funciones=@idFuncion, fk_id_oficinas=@idOficina where id=@id;
		commit;
	end;
	
	if @opc=4
	begin
		begin transaction
		delete from funciones_oficinas where id=@id;
		commit;
	end;

	if @opc=5
	begin
		--begin transaction
		select fk_id_funciones, fk_id_oficinas, funcion, oficina, edificio from funciones_oficinas
		inner join funciones on fk_id_funciones=funciones.id
		inner join oficinas on fk_id_oficinas=oficinas.id
		inner join edificios on oficinas.fk_id_edificio=edificios.id
		where fk_id_funciones=@idFuncion;
		--commit;
	end;
	
	--SELECT ONLY BY FUNCTION
	if @opc=6
	begin
		select fk_id_oficinas from funciones_oficinas where fk_id_funciones=@idFuncion;
	end;

	if @opc=7
	begin
		delete from funciones_oficinas where fk_id_funciones=@idFuncion and fk_id_oficinas=@idOficina;
	end;

	if @opc=8
	begin
	begin transaction
		insert into funciones_oficinas values( (select max(id) from funciones), @idOficina );
	commit;
	end;

	if @opc=9
	begin
	begin transaction
		delete from funciones_oficinas where fk_id_funciones=@idFuncion;
	commit;
	end;

end;
GO
/****** Object:  StoredProcedure [dbo].[crudModulos]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudModulos]
@opc int,
@id int,
@modulo varchar(50)
as
begin
	if @opc=1
	begin
		begin transaction
			insert into modulos values(@modulo);
		commit;
	end;

	if @opc=2
	begin
		select * from modulos;
	end;

	if @opc=3
	begin
		begin transaction
			update modulos set modulo=@modulo where @id=id;
		commit;
	end;


	if @opc=4
	begin
		begin transaction
			delete from modulos where @id=id;
		commit;
	end;

	if @opc=5
	begin
		select * from modulos where modulo=@modulo;
	end;
end;
GO
/****** Object:  StoredProcedure [dbo].[crudModulos_Roles]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudModulos_Roles]
@opc int,
@idModulo int,
@idRol int
as
begin
if @opc=1
	begin
		begin transaction
		insert into modulos_roles values (@idModulo, @idRol);
		commit;
	end;

	if @opc=2
	begin
		select * from modulos_roles where fk_id_roles=@idRol and fk_id_modulos=@idModulo;
	end;

	--AVOID UPDATE BECAUSE IN OTHER CASES I DIDN'T USE IT
	/*
	if @opc=3
	begin
		begin transaction
		
		commit;
	end;
	*/
	
	--DELETE BY ROL
	if @opc=4
	begin
		begin transaction
		delete from modulos_roles where fk_id_roles=@idRol;
		commit;
	end;

	--DELETE BY MODULO
	if @opc=5
	begin
		begin transaction
		delete from modulos_roles where fk_id_roles=@idModulo;
		commit;
	end;

	if @opc=6
	begin
	--SELECT BY ROL
		select modulos_roles.fk_id_modulos, modulo, modulos_roles.fk_id_roles, rol from modulos_roles
		inner join modulos on modulos_roles.fk_id_modulos=modulos.id
		inner join roles on modulos_roles.fk_id_roles=roles.id
		where fk_id_roles=@idRol
	end;

	if @opc=6
	begin
	--SELECT BY MODULO
		select modulos_roles.fk_id_modulos, modulo, modulos_roles.fk_id_roles, rol from modulos_roles
		inner join modulos on modulos_roles.fk_id_modulos=modulos.id
		inner join roles on modulos_roles.fk_id_roles=roles.id
		where fk_id_modulos=@idModulo
	end;

	if @opc=7
	begin
	begin transaction
		insert into modulos_roles values( @idModulo, (select max(id) from roles) );
	commit;
	end;

	if @opc=8
	begin
	begin transaction
		delete from modulos_roles where fk_id_modulos=@idModulo and fk_id_roles=@idRol;
	commit;
	end;

end;
GO
/****** Object:  StoredProcedure [dbo].[crudMuni]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudMuni]
@opc int,
@id int,
@codigo int,
@municipio varchar(25),
@idDpto int
as
begin
	if @opc=1
	begin
		begin transaction
		insert into municipios values(@codigo, @municipio, @idDpto);
		commit;
	end;

	if @opc=2
	begin
		begin transaction
		select * from municipios where id=@id;
		commit;
	end;

	if @opc=3
	begin
		begin transaction
		update municipios set codigo=@codigo, municipio=@municipio, fk_id_departamentos=@idDpto where id=@id;
		commit;
	end;
	
	if @opc=4
	begin
		begin transaction
		delete from municipios where id=@id;
		commit;
	end;

	if @opc=5
	begin
		begin transaction
		select * from municipios;
		commit;
	end;
end;
GO
/****** Object:  StoredProcedure [dbo].[crudOficinas]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudOficinas]
@opc int,
@id int,
@oficina varchar(80),
@idEdificio int
as
begin

if @opc=1
begin
	begin transaction
	insert into oficinas values(@oficina, @idEdificio);
	commit;
end;

if @opc=2
begin
	begin transaction
	select * from oficinas where id=@id;
	commit;
end;

if @opc=3
begin
	begin transaction
	update oficinas set oficina=@oficina, fk_id_edificio=@idEdificio where id=@id;
	commit;
end;

if @opc=4
begin
	begin transaction
	delete from oficinas where id=@id;
	commit;
end;

if @opc=5
begin
	begin transaction
	select * from oficinas;
	commit;
end;

end;
GO
/****** Object:  StoredProcedure [dbo].[crudPrecedencia]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[crudPrecedencia]
@opc int,
@id int,
@departamento int,
@municipio int
as
begin
	if @opc=1
	begin
		begin transaction
		insert into lugar_precedencia values(@departamento, @municipio);
		commit;
	end;

	if @opc=2
	begin
		begin transaction
		select * from lugar_precedencia where id=@id;
		commit;
	end;

	if @opc=3
	begin
		begin transaction
		update lugar_precedencia set fk_id_depto=@departamento, fk_id_mun=@municipio where id=@id;
		commit;
	end;
	
	if @opc=4
	begin
		begin transaction
		delete from lugar_precedencia where id=@id;
		commit;
	end;

	if @opc=5
	begin
		begin transaction
		select * from lugar_precedencia;
		commit;
	end;
end;
GO
/****** Object:  StoredProcedure [dbo].[crudRequerimientos]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudRequerimientos]
@opc int,
@id int,
@papeleria varchar
as
begin
if @opc=1
	begin
		begin transaction
		insert into requerimientos values(@papeleria);
		commit;
	end;

	if @opc=2
	begin
		begin transaction
		select * from requerimientos where id=@id;
		commit;
	end;

	if @opc=3
	begin
		begin transaction
		update requerimientos set papeleria=@papeleria where id=@id;
		commit;
	end;
	
	if @opc=4
	begin
		begin transaction
		delete from requerimientos where id=@id;
		commit;
	end;

	if @opc=5
	begin
		begin transaction
		select * from requerimientos;
		commit;
	end;


end;
GO
/****** Object:  StoredProcedure [dbo].[crudRoles]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudRoles]
@opc int,
@id int,
@rol varchar(50)
as
begin
if @opc=1
	begin
		begin transaction
		insert into roles values (@rol);
		commit;
	end;

	if @opc=2
	begin
		select * from roles;
	end;
	
	if @opc=3
	begin
		begin transaction
			update roles set rol=@rol where id=@id;
		commit;
	end;

	if @opc=4
	begin
		begin transaction
		delete from roles where id=@id;
		commit;
	end;

	if @opc=5
	begin
		begin transaction
			select * from roles where rol=@rol;
		commit;
	end;

end;
GO
/****** Object:  StoredProcedure [dbo].[crudVisitantes]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudVisitantes]
@opc int,
@id int,
@dpi bigint,
@huella varbinary(max),
@nombre1 varchar(15),
@nombre2 varchar(15),
@apellido1 varchar(15),
@apellido2 varchar(15),
@idMun int,
@tel varchar(8)
as
begin
if @opc=1
begin
	begin transaction
	insert into visitantes values(@dpi, @huella, @nombre1, @nombre2, @apellido1, @apellido2, 
	@idMun, @tel);
	commit;
end;

if @opc=2
begin
	begin transaction
	select * from visitantes where huella=@huella;
	commit;
end;

if @opc=3
begin
	begin transaction
	update visitantes set dpi=@dpi, nombre1=@nombre1, nombre2=@nombre2, apellido1=@apellido1, apellido2=@apellido2,
	fk_id_municipio=@idMun where id=@id;
	commit;
end;

if @opc=4
begin
	begin transaction
	delete from visitantes where id=@id;
	commit;
end;

if @opc=5
begin
	begin transaction
	select visitantes.id, visitantes.dpi, visitantes.huella, visitantes.nombre1,
	visitantes.nombre2, visitantes.apellido1, visitantes.apellido2, departamentos.id,
	municipios.id, departamento, municipio, telefono 
	from visitantes 
	inner join municipios on fk_id_municipio= municipios.id
	inner join departamentos on fk_id_departamentos=departamentos.id
	where dpi=@dpi;
	commit;
end;

if @opc=6
begin
	begin transaction
	select visitantes.id, dpi, huella, nombre1, nombre2, apellido1, apellido2, fk_id_municipio, 
	fk_id_departamentos, municipio, departamento, telefono 
	from visitantes
	inner join municipios on fk_id_municipio=municipios.id
	inner join departamentos on fk_id_departamentos=departamentos.id
	where visitantes.id=@id;
	commit;
end;


end;
GO
/****** Object:  StoredProcedure [dbo].[crudVisitas]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[crudVisitas]
@opc int,
@id int,
@dpi bigint
as
begin
	if @opc=1
	begin
		begin transaction
		insert into visitas values(@dpi, convert(DATE, GETDATE()), CONVERT(TIME,GETDATE(),114));
		commit;
	end;
	
	/*SELECCIONAR SEGUN DPI DE PERSONA*/
	if @opc=2
	begin
		begin transaction
		select funcion, fecha, hora_entrada, oficina 
		from detalle_visitas
		inner join visitas on detalle_visitas.fk_id_visitas= visitas.id
		inner join funciones on detalle_visitas.fk_id_funcion= funciones.id
		inner join oficinas on detalle_visitas.fk_id_oficina= oficinas.id
		where visitas.fk_dpi_visitantes= @dpi
		commit;
	end;


	if @opc=3
	begin
		begin transaction
		SELECT MAX(ID) FROM visitas;
		commit;
	end;

end;
GO
/****** Object:  StoredProcedure [dbo].[curdFunciones_Requerimientos]    Script Date: 1/7/2021 12:19:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[curdFunciones_Requerimientos]
@opc int,
@id int,
@idFuncion int,
@idRequerimiento int
as
begin
	if @opc=1
	begin
		begin transaction
		insert into funciones_requerimientos values(@idFuncion, @idRequerimiento);
		commit;
	end;

	if @opc=2
	begin
		begin transaction
		select * from funciones_requerimientos where id=@id;
		commit;
	end;

	if @opc=3
	--update also the funciones_oficinas table
	--update also de the funciones_requerimientos table
	begin
		begin transaction
		update funciones_requerimientos set fk_id_funciones=@idFuncion, 
		fk_id_requerimientos=@idRequerimiento where id=@id;
		commit;
	end;
	
	if @opc=4
	begin
		begin transaction
		delete from funciones_requerimientos where id=@id;
		commit;
	end;

	
	if @opc=5
	begin
		
		SELECT fk_id_requerimientos, papeleria FROM funciones_requerimientos
                INNER JOIN requerimientos on fk_id_requerimientos=requerimientos.id
                WHERE fk_id_funciones=@idFuncion
	end;


end;
/*
	if @opc=6
	begin
		begin transaction
		SELECT fk_id_requerimientos, papeleria FROM funciones_requerimientos
                INNER JOIN requerimientos on fk_id_requerimientos=requerimientos.id
                WHERE fk_id_funciones=@idF
		commit;
	end;
*/
GO
USE [master]
GO
ALTER DATABASE [proyecto] SET  READ_WRITE 
GO
