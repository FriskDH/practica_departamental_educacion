package view.login;

import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.*;
import javax.swing.border.Border;

public class login extends JFrame {
  public JFrame frameLogin;
  public JLabel lblUser, lblPass, lblIcon;
  public JButton btnLogin;
  public JTextField txtUser;
  public JPasswordField txtPass;

  public login() throws MalformedURLException {
//    try {
//      UIManager.setLookAndFeel(
//        "com.sun.java.swing.plaf.windows.WindowsLookAndFeel"
//      );
//    } catch (Exception ignored) {}
    frameLogin = new JFrame();
    frameLogin.setLayout(null);
    frameLogin.getContentPane().setBackground(new Color(192, 192, 192));
    frameLogin.setSize(500, 500);
    frameLogin.setLocationRelativeTo(null);
    frameLogin.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    lblUser = new JLabel();
    lblPass = new JLabel();
    lblIcon = new JLabel();
    txtUser = new JTextField();
    txtPass = new JPasswordField();
    btnLogin = new JButton();
    lblUser.setText("ID: ");
    lblPass.setText("Contraseña: ");
    lblIcon.setIcon(new ImageIcon((ClassLoader.getSystemResource("geography.png"))));
    btnLogin.setText("Ingresar");

    /*Insets insets = frameLogin.getInsets();
        Dimension sizeBtn = btnLogin.getPreferredSize();
        Dimension sizeLbl = lblUser.getPreferredSize();
        Dimension sizeTxt= txtUser.getPreferredSize();*/

    lblIcon.setSize(300, 300);

    lblUser.setFont(new Font("Carlito", Font.PLAIN, 20));
    //        lblUser.setForeground(new Color(255,255,255));
    txtUser.setForeground(Color.GRAY);
    txtPass.setForeground(Color.GRAY);
    lblPass.setFont(new Font("Carlito", Font.PLAIN, 20));
    //        lblPass.setForeground(new Color(255,255,255));
    btnLogin.setFont(new Font("Carlito", Font.PLAIN, 20));

    lblUser.setLocation(70, 200);
    lblPass.setLocation(70, 250);
    txtUser.setLocation(200, 200);
    txtPass.setLocation(200, 250);
    btnLogin.setLocation(170, 340);
    lblIcon.setLocation(170, -60);

    lblUser.setSize(120, 35);
    lblPass.setSize(120, 35);
    txtUser.setSize(170, 35);
    txtPass.setSize(170, 35);
    btnLogin.setSize(150, 50);

    /*lblUser.setBounds(80+insets.left, 280+insets.top, sizeLbl.width+90, sizeLbl.height);
        lblPass.setBounds(80+insets.left, 320+insets.top, sizeLbl.width+90, sizeLbl.height);
        txtPass.setBounds(200+insets.left, 320+insets.top, sizeLbl.width+90, sizeLbl.height);
        txtUser.setBounds(200+insets.left, 280+insets.top, sizeLbl.width+90, sizeLbl.height);*/
    frameLogin.add(lblUser).revalidate();
    frameLogin.add(lblPass).revalidate();
    frameLogin.add(lblIcon).revalidate();
    frameLogin.add(txtUser).revalidate();
    frameLogin.add(txtPass).revalidate();
    frameLogin.add(btnLogin).revalidate();
    /*        lblPass.repaint();
        lblUser.repaint();
        lblIcon.repaint();
        btnLogin.repaint();
        txtUser.repaint();
        txtPass.repaint();
        */
    //        panelLogin.setFont();

    frameLogin.setVisible(true);
  }
}
