package view.menu;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class edicionEdificios {
    public JFrame framePrimario;
    private JLabel lblModulo;
    public static JTextField txtEdificio;
    private Font font, font2;
    public JButton btnGuardar, btnCrear;
    
    public edicionEdificios(){
        font = new Font("Carlito", Font.BOLD, 17);
        font2 = new Font("Carlito", Font.PLAIN, 14);

        framePrimario = new JFrame();
        framePrimario.setLayout(null);
        framePrimario.getContentPane().setBackground(new Color(230, 230, 230));
        framePrimario.setSize(450, 230);
        framePrimario.setLocationRelativeTo(null);
        framePrimario.setLocationRelativeTo(null);
        framePrimario.setFont(font);

        txtEdificio = new JTextField();
        txtEdificio.setSize(200, 30);
        txtEdificio.setFont(font2);
        txtEdificio.setLocation(70, 15);
        txtEdificio.setVisible(true);

        lblModulo = new JLabel("Edificio");
        lblModulo.setSize(50, 35);
        lblModulo.setLocation(15, 15);
        lblModulo.setVisible(true);

        btnGuardar = new JButton("Guardar");
        btnGuardar.setFont(font2);
        btnGuardar.setIcon(new ImageIcon("extras\\images\\form.png"));
        btnCrear = new JButton("Crear");
        btnCrear.setFont(font2);
        btnCrear.setIcon(new ImageIcon("extras\\images\\form.png"));

        btnGuardar.setLocation(50, 100);
        btnCrear.setLocation(250, 100);

        btnGuardar.setSize(160, 70);
        btnCrear.setSize(160, 70);

        framePrimario.add(lblModulo);
        framePrimario.add(btnCrear);
        framePrimario.add(btnGuardar);
        framePrimario.add(txtEdificio);
    }
}
