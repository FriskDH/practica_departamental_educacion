package view.menu;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class edicionFunciones {
  public JFrame framePrimario;
  private JLabel lblFunciones;
  public static JTextArea txtFuncion;
  private Font font, font2;
  public JButton btnGuardar, btnCrear;
  public JTable tableFuncionees;
  private JScrollPane txtScroll;

  public edicionFunciones() {
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);

    framePrimario = new JFrame();
    framePrimario.setLayout(null);
    framePrimario.getContentPane().setBackground(new Color(230, 230, 230));
    framePrimario.setSize(550, 500);
    framePrimario.setLocationRelativeTo(null);
    framePrimario.setLocationRelativeTo(null);
    framePrimario.setFont(font);


    txtFuncion = new JTextArea();
//    txtFuncion.setSize(650, 80);
    txtFuncion.setFont(font);
//    txtFuncion.setLocation(120, 40);
//    txtFuncion.setVisible(true);
    txtScroll= new JScrollPane(txtFuncion);
    txtScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    txtScroll.setSize(350, 80);
    txtScroll.setLocation(150, 10);
    txtScroll.setVisible(true);


    btnGuardar = new JButton("Guardar");
    btnGuardar.setFont(font2);
    btnGuardar.setIcon(new ImageIcon("extras\\images\\form.png"));
    btnCrear = new JButton("Crear");
    btnCrear.setFont(font2);
    btnCrear.setIcon(new ImageIcon("extras\\images\\form.png"));
    lblFunciones = new JLabel("Funcion");
    lblFunciones.setSize(250, 30);
    lblFunciones.setLocation(15, 15);
    lblFunciones.setVisible(true);
    btnGuardar.setLocation(50, 350);
    btnCrear.setLocation(250, 350);

    btnGuardar.setSize(160, 70);
    btnCrear.setSize(160, 70);

    tableFuncionees =
      new JTable(
        new DefaultTableModel(
          new Object[][] { null },
          new String[] { "No.", "Oficina", "Si/No" }
        )
      );
    tableFuncionees.setVisible(true);
    tableFuncionees.setFont(new Font("Carlito", Font.PLAIN, 15));
    JScrollPane scrollPane = new JScrollPane(tableFuncionees);
    scrollPane.setVisible(true);
    scrollPane.setSize(400, 200);
    scrollPane.setLocation(50, 100);

    framePrimario.add(lblFunciones);
    framePrimario.add(txtScroll);
    framePrimario.add(btnCrear);
    framePrimario.add(btnGuardar);
    framePrimario.add(scrollPane);
  }
}
