package view.menu;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class edicionRoles {
    public JFrame framePrimario;
    private JLabel lblRoles;
    public static JTextField txtFuncion;
    private Font font, font2;
    public JButton btnGuardar, btnCrear;
    public JTable tableRolees;
    
    public edicionRoles(){
        font = new Font("Carlito", Font.BOLD, 17);
        font2 = new Font("Carlito", Font.PLAIN, 14);

        framePrimario = new JFrame();
        framePrimario.setLayout(null);
        framePrimario.getContentPane().setBackground(new Color(230, 230, 230));
        framePrimario.setSize(550, 500);
        framePrimario.setLocationRelativeTo(null);
        framePrimario.setLocationRelativeTo(null);
        framePrimario.setFont(font);

        txtFuncion= new JTextField();
        txtFuncion.setLocation(150, 15);
        txtFuncion.setSize(150, 35);
        txtFuncion.setVisible(true);

        btnGuardar = new JButton("Guardar");
        btnGuardar.setFont(font2);
        btnGuardar.setIcon(new ImageIcon("extras\\images\\form.png"));
        btnCrear = new JButton("Crear");
        btnCrear.setFont(font2);
        btnCrear.setIcon(new ImageIcon("extras\\images\\form.png"));
        lblRoles = new JLabel("Rol");
        lblRoles.setSize(250, 30);
        lblRoles.setLocation(15, 15);
        lblRoles.setVisible(true);
        btnGuardar.setLocation(50, 350);
        btnCrear.setLocation(250, 350);

        btnGuardar.setSize(160, 70);
        btnCrear.setSize(160, 70);

        tableRolees =
                new JTable(
                        new DefaultTableModel(
                                new Object[][] { null },
                                new String[] { "No.", "Módulo", "Si/No" }
                        )
                );
        tableRolees.setVisible(true);
        tableRolees.setFont(new Font("Carlito", Font.PLAIN, 15));
        JScrollPane scrollPane = new JScrollPane(tableRolees);
        scrollPane.setVisible(true);
        scrollPane.setSize(400, 200);
        scrollPane.setLocation(50, 100);

        framePrimario.add(lblRoles);
        framePrimario.add(txtFuncion);
        framePrimario.add(btnCrear);
        framePrimario.add(btnGuardar);
        framePrimario.add(scrollPane);
    }
}

