package view.menu;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Modulos {
    public JFrame framePrincipal;
    private Font font, font2;
    public JTable tableModulos;
    public JButton btnBuscar, btnEditar, btnCrear, btnEliminar;
    private JLabel lblOficina;
    public JTextField txtModulo;
    
    public Modulos(){

        font = new Font("Carlito", Font.BOLD, 17);
        font2 = new Font("Carlito", Font.PLAIN, 14);

        framePrincipal = new JFrame();
        framePrincipal.setLayout(null);
        framePrincipal.setSize(1000, 700);
        framePrincipal.setLocationRelativeTo(null);
        framePrincipal.setLocationRelativeTo(null);
        framePrincipal.setFont(font);

        lblOficina = new JLabel("Módulo: ");
        lblOficina.setSize(100, 50);
        lblOficina.setFont(font);
        lblOficina.setLocation(15, 30);
        lblOficina.setVisible(true);

        txtModulo = new JTextField();
        txtModulo.setSize(150, 30);
        txtModulo.setFont(font);
        txtModulo.setLocation(120, 40);
        txtModulo.setVisible(true);

        btnBuscar = new JButton("Buscar");
        btnBuscar.setSize(100, 50);
        btnBuscar.setFont(font);
        btnBuscar.setLocation(15, 150);
        btnBuscar.setVisible(true);

        btnEditar = new JButton("Editar");
        btnEditar.setSize(100, 50);
        btnEditar.setFont(font);
        btnEditar.setLocation(175, 150);
        btnEditar.setVisible(true);

        btnCrear = new JButton("Crear");
        btnCrear.setSize(100, 50);
        btnCrear.setFont(font);
        btnCrear.setLocation(330, 150);
        btnCrear.setVisible(true);

        btnEliminar = new JButton("Eliminar");
        btnEliminar.setSize(100, 50);
        btnEliminar.setFont(font);
        btnEliminar.setLocation(485, 150);
        btnEliminar.setVisible(true);

        tableModulos =
                new JTable() {

                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };
        tableModulos =
                new JTable(
                        new DefaultTableModel(
                                new Object[][] { null },
                                new String[] { "Módulo"}
                        )
                );

        tableModulos.setVisible(true);
        tableModulos.setFont(new Font("Arial", Font.PLAIN, 17));
        //
        JScrollPane scrollPane = new JScrollPane(tableModulos);
        scrollPane.setVisible(true);
        scrollPane.setSize(950, 380);
        scrollPane.setLocation(15, 250);
        //        scrollPane.setFont(new Font("Arial", Font.BOLD, 18));

        framePrincipal.add(btnBuscar);
        framePrincipal.add(btnEditar);
        framePrincipal.add(btnCrear);
        framePrincipal.add(lblOficina);
        //        framePrincipal.add(lblFuncion);
        framePrincipal.add(txtModulo);
        //        framePrincipal.add(cmbFuncion);
        framePrincipal.add(btnEliminar);
        //        framePrincipal.add(tableModulos);
        framePrincipal.add(scrollPane);
        //        framePrincipal.add(tableModulos.getTableHeader());


    }

    public void getFrame(){
        this.framePrincipal.setVisible(true);
    }
}
