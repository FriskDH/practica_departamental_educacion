package view.menu;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Funcionarios {
  public JFrame framePrincipal;
  private Font font, font2;
  public JTable tableUsers;
  public JButton btnBuscar, btnEditar, btnCrear, btnEliminar;
  private JLabel lblDPI, lblOficina, lblFuncion;
  public JTextField txtDPI;
  public JComboBox cmbOficina, cmbFuncion;

  public Funcionarios() {
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);

    framePrincipal = new JFrame();
    framePrincipal.setLayout(null);
    framePrincipal.setSize(1000, 700);
    framePrincipal.setLocationRelativeTo(null);
    framePrincipal.setLocationRelativeTo(null);
    framePrincipal.setFont(font);

    lblDPI = new JLabel("ID:");
    lblDPI.setSize(50, 50);
    lblDPI.setFont(font);
    lblDPI.setLocation(15, 30);
    lblDPI.setVisible(true);

    txtDPI = new JTextField();
    txtDPI.setSize(150, 30);
    txtDPI.setFont(font);
    txtDPI.setLocation(100, 40);
    txtDPI.setVisible(true);

    lblOficina = new JLabel("Oficina:");
    lblOficina.setSize(100, 50);
    lblOficina.setFont(font);
    lblOficina.setLocation(315, 30);
    lblOficina.setVisible(true);

    cmbOficina = new JComboBox();
    cmbOficina.setSize(150, 30);
    cmbOficina.setFont(font2);
    cmbOficina.setLocation(400, 40);
    cmbOficina.setVisible(true);

    //        lblFuncion= new JLabel("Funcion:");
    //        lblFuncion.setSize(100, 50);
    //        lblFuncion.setFont(font);
    //        lblFuncion.setLocation(615, 30);
    //        lblFuncion.setVisible(true);

    //        cmbFuncion= new JComboBox();
    //        cmbFuncion.setSize(150, 30);
    //        cmbFuncion.setFont(font2);
    //        cmbFuncion.setLocation(710, 40);
    //        cmbFuncion.setVisible(true);

    btnBuscar = new JButton("Buscar");
    btnBuscar.setSize(100, 50);
    btnBuscar.setFont(font);
    btnBuscar.setLocation(15, 150);
    btnBuscar.setVisible(true);

    btnEditar = new JButton("Editar");
    btnEditar.setSize(100, 50);
    btnEditar.setFont(font);
    btnEditar.setLocation(175, 150);
    btnEditar.setVisible(true);

    btnCrear = new JButton("Crear");
    btnCrear.setSize(100, 50);
    btnCrear.setFont(font);
    btnCrear.setLocation(330, 150);
    btnCrear.setVisible(true);

    btnEliminar = new JButton("Eliminar");
    btnEliminar.setSize(100, 50);
    btnEliminar.setFont(font);
    btnEliminar.setLocation(485, 150);
    btnEliminar.setVisible(true);

    tableUsers =
      new JTable() {

        public boolean isCellEditable(int rowIndex, int colIndex) {
          return false;
        }
      };
    tableUsers =
      new JTable(
        new DefaultTableModel(
          new Object[][] { null },
          new String[] {
            "ID",
            "P. NOMBRE",
            "S. NOMBRE",
            "P. APELLIDO",
            "S. APELLIDO",
            "OFICINA",
            "ROL"
          }
        )
      );

    //        tableUsers.setLocation(15, 250);
    //        tableUsers.setSize(900, 380);
    //        tableUsers.setFillsViewportHeight(true);
    tableUsers.setVisible(true);
    tableUsers.setFont(new Font("Arial", Font.PLAIN, 17));
    //
    JScrollPane scrollPane = new JScrollPane(tableUsers);
    scrollPane.setVisible(true);
    scrollPane.setSize(950, 380);
    scrollPane.setLocation(15, 250);
    //        scrollPane.setFont(new Font("Arial", Font.BOLD, 18));

    framePrincipal.add(btnBuscar);
    framePrincipal.add(btnEditar);
    framePrincipal.add(btnCrear);
    framePrincipal.add(lblDPI);
    //        framePrincipal.add(lblFuncion);
    framePrincipal.add(lblOficina);
    framePrincipal.add(txtDPI);
    //        framePrincipal.add(cmbFuncion);
    framePrincipal.add(cmbOficina);
    framePrincipal.add(btnEliminar);
    //        framePrincipal.add(tableUsers);
    framePrincipal.add(scrollPane);
    //        framePrincipal.add(tableUsers.getTableHeader());

  }

  public void getFrame(){
    this.framePrincipal.setVisible(true);
  }
}
