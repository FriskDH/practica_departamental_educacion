package view.menu;

import java.awt.*;
import javax.swing.*;

public class Visitantes {
  public JFrame framePrimario;
  private JLabel lblTelefono, lblNombre1, lblNombre2, lblApellido1, lblApellido2, lblDPI, lblLugarPrecedencia, lblIcon;
  public static JTextField txtNombre1, txtNombre2, txtApellido1, txtApellido2, txtDPI, txtTelefono;
  public static JComboBox cmbPrecedencia, cmbPrecedencica2;
  private Font font, font2;
  public JButton btnLimpiar, btnImprimir, btnBuscarDPI, btnDesbloquear;

  public Visitantes() {
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);

    framePrimario = new JFrame();
    framePrimario.setLayout(null);
    framePrimario.getContentPane().setBackground(new Color(230, 230, 230));
    framePrimario.setSize(550, 630);
    framePrimario.setLocationRelativeTo(null);
    //        framePrimario.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    framePrimario.setLocationRelativeTo(null);
    framePrimario.setFont(font);
    lblIcon = new JLabel();
    //        lblIcon.setIcon(new ImageIcon("extras\\images\\"));
    lblIcon.setSize(550, 700);

    lblDPI = new JLabel("DPI");
    lblNombre1 = new JLabel("Primer Nombre");
    lblNombre2 = new JLabel("Segundo Nombre");
    lblApellido1 = new JLabel("Primer Apellido");
    lblApellido2 = new JLabel("Segundo Apellido");
    lblLugarPrecedencia = new JLabel("Lugar de Precedencia");
    lblTelefono= new JLabel("Teléfono");

    lblDPI.setFont(font);
    lblNombre1.setFont(font);
    lblNombre2.setFont(font);
    lblApellido1.setFont(font);
    lblApellido2.setFont(font);
    lblLugarPrecedencia.setFont(font);
    lblTelefono.setFont(font);

    lblDPI.setLocation(40, 50);
    lblNombre1.setLocation(40, 120);
    lblNombre2.setLocation(40, 190);
    lblApellido1.setLocation(40, 260);
    lblApellido2.setLocation(40, 330);
    lblLugarPrecedencia.setLocation(40, 400);
    lblTelefono.setLocation(40, 500);

    lblDPI.setSize(120, 35);
    lblNombre1.setSize(200, 35);
    lblNombre2.setSize(200, 35);
    lblApellido1.setSize(200, 35);
    lblApellido2.setSize(200, 35);
    lblLugarPrecedencia.setSize(200, 35);
    lblTelefono.setSize(200, 35);

    txtNombre1 = new JTextField();
    txtNombre2 = new JTextField();
    txtApellido1 = new JTextField();
    txtApellido2 = new JTextField();
    txtDPI = new JTextField();
    cmbPrecedencia = new JComboBox();
    cmbPrecedencica2 = new JComboBox();
    txtTelefono= new JTextField();

    txtDPI.setLocation(280, 50);
    txtNombre1.setLocation(280, 120);
    txtNombre2.setLocation(280, 190);
    txtApellido1.setLocation(280, 260);
    txtApellido2.setLocation(280, 330);
    cmbPrecedencia.setLocation(280, 400);
    cmbPrecedencica2.setLocation(280, 430);
    txtTelefono.setLocation(280, 500);


    txtDPI.setSize(120, 35);
    txtNombre1.setSize(120, 35);
    txtNombre2.setSize(120, 35);
    txtApellido1.setSize(120, 35);
    txtApellido2.setSize(120, 35);
    cmbPrecedencia.setSize(120, 35);
    cmbPrecedencica2.setSize(120, 35);
    txtTelefono.setSize(120, 35);

    txtDPI.setFont(font2);
    txtNombre1.setFont(font2);
    txtNombre2.setFont(font2);
    txtApellido1.setFont(font2);
    txtApellido2.setFont(font2);
    cmbPrecedencia.setFont(font2);
    cmbPrecedencica2.setFont(font2);
    txtTelefono.setFont(font2);

    btnLimpiar = new JButton("Limpiar");
    btnImprimir = new JButton("");
    btnBuscarDPI = new JButton("Buscar");

    btnLimpiar.setFont(font2);
    btnBuscarDPI.setFont(font2);

    btnImprimir.setIcon(new ImageIcon(ClassLoader.getSystemResource("pdf.png")));

    btnLimpiar.setLocation(450, 50);
    btnImprimir.setLocation(480, 120);
    btnBuscarDPI.setLocation(450, 220);

    btnLimpiar.setSize(80, 30);
    btnImprimir.setSize(50, 80);
    btnBuscarDPI.setSize(80, 30);

    framePrimario.add(lblDPI);
    framePrimario.add(lblNombre1);
    framePrimario.add(lblNombre2);
    framePrimario.add(lblApellido1);
    framePrimario.add(lblApellido2);
    framePrimario.add(lblLugarPrecedencia);
    framePrimario.add(txtDPI);
    framePrimario.add(txtNombre1);
    framePrimario.add(txtNombre2);
    framePrimario.add(txtApellido1);
    framePrimario.add(txtApellido2);
    framePrimario.add(cmbPrecedencia);
    framePrimario.add(cmbPrecedencica2);
    framePrimario.add(btnLimpiar);
    framePrimario.add(btnImprimir);
    framePrimario.add(btnBuscarDPI);
    framePrimario.add(lblTelefono);
    framePrimario.add(txtTelefono);
  }

  public void getFrame(){
    this.framePrimario.setVisible(true);
  }
}
