package view.menu;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Edificios {
    public JFrame framePrincipal;
    private Font font, font2;
    public JTable tableEdificios;
    public JButton btnBuscar, btnEditar, btnCrear, btnEliminar;
    private JLabel lblEdificio;
    public JTextField txtEdificio;
    
    public Edificios(){
        font = new Font("Carlito", Font.BOLD, 17);
        font2 = new Font("Carlito", Font.PLAIN, 14);

        framePrincipal = new JFrame();
        framePrincipal.setLayout(null);
        framePrincipal.setSize(1000, 700);
        framePrincipal.setLocationRelativeTo(null);
        framePrincipal.setLocationRelativeTo(null);
        framePrincipal.setFont(font);

        lblEdificio = new JLabel("Edificio: ");
        lblEdificio.setSize(100, 50);
        lblEdificio.setFont(font);
        lblEdificio.setLocation(15, 30);
        lblEdificio.setVisible(true);

        txtEdificio = new JTextField();
        txtEdificio.setSize(150, 30);
        txtEdificio.setFont(font);
        txtEdificio.setLocation(120, 40);
        txtEdificio.setVisible(true);

        btnBuscar = new JButton("Buscar");
        btnBuscar.setSize(100, 50);
        btnBuscar.setFont(font);
        btnBuscar.setLocation(15, 150);
        btnBuscar.setVisible(true);

        btnEditar = new JButton("Editar");
        btnEditar.setSize(100, 50);
        btnEditar.setFont(font);
        btnEditar.setLocation(175, 150);
        btnEditar.setVisible(true);

        btnCrear = new JButton("Crear");
        btnCrear.setSize(100, 50);
        btnCrear.setFont(font);
        btnCrear.setLocation(330, 150);
        btnCrear.setVisible(true);

        btnEliminar = new JButton("Eliminar");
        btnEliminar.setSize(100, 50);
        btnEliminar.setFont(font);
        btnEliminar.setLocation(485, 150);
        btnEliminar.setVisible(true);

        tableEdificios =
                new JTable() {

                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };
        tableEdificios =
                new JTable(
                        new DefaultTableModel(
                                new Object[][] { null },
                                new String[] { "Edificio" }
                        )
                );

        //        tableEdificios.setLocation(15, 250);
        //        tableEdificios.setSize(900, 380);
        //        tableEdificios.setFillsViewportHeight(true);
        tableEdificios.setVisible(true);
        tableEdificios.setFont(new Font("Arial", Font.PLAIN, 17));
        //
        JScrollPane scrollPane = new JScrollPane(tableEdificios);
        scrollPane.setVisible(true);
        scrollPane.setSize(950, 380);
        scrollPane.setLocation(15, 250);
        //        scrollPane.setFont(new Font("Arial", Font.BOLD, 18));

        framePrincipal.add(btnBuscar);
        framePrincipal.add(btnEditar);
        framePrincipal.add(btnCrear);
        framePrincipal.add(lblEdificio);
        //        framePrincipal.add(lblFuncion);
        framePrincipal.add(txtEdificio);
        //        framePrincipal.add(cmbFuncion);
        framePrincipal.add(btnEliminar);
        //        framePrincipal.add(tableEdificios);
        framePrincipal.add(scrollPane);
        //        framePrincipal.add(tableEdificios.getTableHeader());

    }

    public void getFrame(){
        this.framePrincipal.setVisible(true);
    }
}
