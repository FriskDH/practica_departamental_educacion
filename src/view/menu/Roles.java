package view.menu;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Roles {
    public JFrame framePrincipal;
    private Font font, font2;
    public JTable tableRoles;
    public JButton btnBuscar, btnEditar, btnCrear, btnEliminar;
    private JLabel lblOficina;
    public JTextField txtRol;

    public Roles () {
        font = new Font("Carlito", Font.BOLD, 17);
        font2 = new Font("Carlito", Font.PLAIN, 14);

        framePrincipal = new JFrame();
        framePrincipal.setLayout(null);
        framePrincipal.setSize(1000, 700);
        framePrincipal.setLocationRelativeTo(null);
        framePrincipal.setLocationRelativeTo(null);
        framePrincipal.setFont(font);

        lblOficina = new JLabel("Rol: ");
        lblOficina.setSize(100, 50);
        lblOficina.setFont(font);
        lblOficina.setLocation(15, 30);
        lblOficina.setVisible(true);

        txtRol = new JTextField();
        txtRol.setSize(150, 30);
        txtRol.setFont(font);
        txtRol.setLocation(120, 40);
        txtRol.setVisible(true);

        btnBuscar = new JButton("Buscar");
        btnBuscar.setSize(100, 50);
        btnBuscar.setFont(font);
        btnBuscar.setLocation(15, 150);
        btnBuscar.setVisible(true);

        btnEditar = new JButton("Editar");
        btnEditar.setSize(100, 50);
        btnEditar.setFont(font);
        btnEditar.setLocation(175, 150);
        btnEditar.setVisible(true);

        btnCrear = new JButton("Crear");
        btnCrear.setSize(100, 50);
        btnCrear.setFont(font);
        btnCrear.setLocation(330, 150);
        btnCrear.setVisible(true);

        btnEliminar = new JButton("Eliminar");
        btnEliminar.setSize(100, 50);
        btnEliminar.setFont(font);
        btnEliminar.setLocation(485, 150);
        btnEliminar.setVisible(true);

        tableRoles =
                new JTable() {

                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };
        tableRoles =
                new JTable(
                        new DefaultTableModel(
                                new Object[][] { null },
                                new String[] { "Rol" }
                        )
                );

        //        tableRoles.setLocation(15, 250);
        //        tableRoles.setSize(900, 380);
        //        tableRoles.setFillsViewportHeight(true);
        tableRoles.setVisible(true);
        tableRoles.setFont(new Font("Arial", Font.PLAIN, 17));
        //
        JScrollPane scrollPane = new JScrollPane(tableRoles);
        scrollPane.setVisible(true);
        scrollPane.setSize(950, 380);
        scrollPane.setLocation(15, 250);
        //        scrollPane.setFont(new Font("Arial", Font.BOLD, 18));

        framePrincipal.add(btnBuscar);
        framePrincipal.add(btnEditar);
        framePrincipal.add(btnCrear);
        framePrincipal.add(lblOficina);
        //        framePrincipal.add(lblFuncion);
        framePrincipal.add(txtRol);
        //        framePrincipal.add(cmbFuncion);
        framePrincipal.add(btnEliminar);
        //        framePrincipal.add(tableRoles);
        framePrincipal.add(scrollPane);
        //        framePrincipal.add(tableRoles.getTableHeader());


    }

    public void getFrame(){
        this.framePrincipal.setVisible(true);
    }
}