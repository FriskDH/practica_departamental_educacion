package view.menu;

import java.awt.*;
import javax.swing.*;

//formulario para los administrativos

public class menuPrincipal {
  public JFrame framePrincipal;
  public JMenuBar menuBar;
  public JMenu menuVisitas, menuData, menuAdmin;
//  public JMenuItem mItmReportes, mItmConsIndiv, mItmGraficos, mItmAdministrar, mItmAdministrarOficinas, mItmAdministrarFunciones, mItmIngreso;
  public JMenu menus;
  private Font font, font2;
  private JLabel lblIcon;

  public menuPrincipal() {
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);

    framePrincipal = new JFrame();
    framePrincipal.setLayout(null);
    //framePrincipal.getContentPane().setBackground(new Color(245,245,245));
    framePrincipal.setSize(1400, 700);
    framePrincipal.setLocationRelativeTo(null);
    framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    framePrincipal.setLocationRelativeTo(null);
    framePrincipal.setFont(font);
    lblIcon = new JLabel();
    lblIcon.setIcon(new ImageIcon(ClassLoader.getSystemResource("blackboard.jpg")));
    lblIcon.setSize(1400, 700);

    menuBar = new JMenuBar();
    menus= new JMenu("Menús");
    menus.setFont(font);
//    menuVisitas = new JMenu();
//    menuData = new JMenu();
//    menuAdmin = new JMenu();

    //ITEMS
//    mItmAdministrar = new JMenuItem("Administrar Usuarios");
//    mItmConsIndiv = new JMenuItem("Consultar Veisitante");
//
//    mItmReportes = new JMenuItem("Reportes");
//    mItmAdministrarFunciones = new JMenuItem("Administrar Funciones");
//    mItmAdministrarOficinas = new JMenuItem("Administrar Oficinas");
//    mItmIngreso= new JMenuItem("Atención al Público");
//
//    mItmAdministrar.setName("Administrar Usuarios");
//    mItmConsIndiv.setName("Consultar Veisitante");
//    mItmAdministrarOficinas.setName("Administrar Oficinas");
//    mItmAdministrarFunciones.setName("Administrar Funciones");
//
//    mItmReportes.setName("Reportes");
//    mItmIngreso.setName("Atención al Público");
//
//    mItmAdministrar.setFont(font2);
//    mItmConsIndiv.setFont(font2);
//    mItmAdministrarFunciones.setFont(font2);
//    mItmAdministrarOficinas.setFont(font2);
//    mItmGraficos.setFont(font2);
//    mItmReportes.setFont(font2);
//    mItmIngreso.setFont(font2);
//
//    mItmAdministrar.setForeground(Color.BLACK);
//    mItmConsIndiv.setForeground(Color.BLACK);
//    mItmAdministrarOficinas.setForeground(Color.BLACK);
//    mItmAdministrarFunciones.setForeground(Color.BLACK);
//
//    mItmReportes.setForeground(Color.BLACK);
//    mItmIngreso.setForeground(Color.BLACK);
//
//
//    menuVisitas.setText("Visitas");
//    menuData.setText("Data");
//    menuAdmin.setText("Administracion");
//
//    menuVisitas.setFont(font);
//    menuVisitas.addSeparator();
//    menuVisitas.setForeground(Color.BLACK);
//
//    menuData.add(mItmReportes);
//    menuData.add(mItmConsIndiv);
//    menuData.setFont(font);
//    menuData.addSeparator();
//    menuData.setForeground(Color.BLACK);
//
//    menuAdmin.add(mItmAdministrar);
//    menuAdmin.add(mItmAdministrarOficinas);
//    menuAdmin.add(mItmAdministrarFunciones);
//    menuAdmin.setFont(font);
//    menuAdmin.addSeparator();
//    menuAdmin.setForeground(Color.BLACK);
//
//    menuBar.add(menuVisitas);
//    menuBar.add(menuData);
//    menuBar.add(menuAdmin);
    menuBar.add(menus);
    menuBar.setLocation(0, 0);
    menuBar.setSize(1400, 32);
    menuBar.setOpaque(true);

    framePrincipal.add(menuBar);
    framePrincipal.add(lblIcon);

  }
}
