package view.menu;


import java.awt.*;
import javax.swing.*;

public class Visitas {
  public static JFrame framePrimario;
  private JLabel lblNombre1, lblNombre2, lblApellido1, lblApellido2, lblDPI, lblLugarPrecedencia, lblIcon, lblSanitizacion;
  public static JLabel lblNoHuella, lblOficina, lblFuncion, lblHuella, lblTelefono, lblListaFunciones;
  public static JTextField txtNombre1, txtNombre2, txtApellido1, txtApellido2, txtDPI, txtSearchFuncion, txtTelefono;
  public static JComboBox cmbPrecedencia, cmbPrecedencica2;
  public static JComboBox cmbOficina, cmbFuncion, cmbSanitizacion, cmbHuella;
  private Font font, font2, fontLblHuella;
  public static JButton btnAgregarFuncion, btnRegistrarHuella, btnVerificarHuella, btnGuardarVisita, btnDesbloquear, btnLimpiar, btnImprimir, btnBuscarDPI;
//  public static JScrollPane txtScroll;
//  public static JTextArea txtFuncion;

  public Visitas(){
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);
    fontLblHuella = new Font("Carlito", Font.PLAIN, 50);

    framePrimario = new JFrame();
    framePrimario.setLayout(null);
    framePrimario.getContentPane().setBackground(new Color(230, 230, 230));
    framePrimario.setSize(860, 930);
    framePrimario.setLocationRelativeTo(null);
    framePrimario.setLocationRelativeTo(null);
    framePrimario.setFont(font);
    lblIcon = new JLabel();
    lblIcon.setSize(550, 700);


    lblDPI = new JLabel("DPI");
    lblNombre1 = new JLabel("Primer Nombre");
    lblNombre2 = new JLabel("Segundo Nombre");
    lblApellido1 = new JLabel("Primer Apellido");
    lblApellido2 = new JLabel("Segundo Apellido");
    lblLugarPrecedencia = new JLabel("Lugar de Precedencia");
    lblOficina = new JLabel("Oficina");
    lblFuncion = new JLabel("Funcion");
    lblSanitizacion = new JLabel("Sanitización");
    lblNoHuella = new JLabel();
    lblHuella= new JLabel("Registrar Huella");
    lblTelefono= new JLabel("Teléfono");

    lblDPI.setFont(font);
    lblNombre1.setFont(font);
    lblNombre2.setFont(font);
    lblApellido1.setFont(font);
    lblApellido2.setFont(font);
    lblLugarPrecedencia.setFont(font);
    lblNoHuella.setFont(font);
    lblFuncion.setFont(font);
    lblOficina.setFont(font);
    lblSanitizacion.setFont(font);
    lblHuella.setFont(font);
    lblTelefono.setFont(font);

    lblDPI.setLocation(40, 50);
    lblNombre1.setLocation(40, 120);
    lblNombre2.setLocation(40, 190);
    lblApellido1.setLocation(40, 260);
    lblApellido2.setLocation(40, 330);
    lblLugarPrecedencia.setLocation(40, 400);
    lblNoHuella.setLocation(40, 630);
    lblNoHuella.setForeground(Color.RED);
    lblFuncion.setLocation(40, 580);
    lblOficina.setLocation(40, 540);
    lblSanitizacion.setLocation(40, 615);
    lblTelefono.setLocation(40, 480);
    lblHuella.setLocation(40, 640);

    lblDPI.setSize(120, 35);
    lblNombre1.setSize(200, 35);
    lblNombre2.setSize(200, 35);
    lblApellido1.setSize(200, 35);
    lblApellido2.setSize(200, 35);
    lblLugarPrecedencia.setSize(200, 35);
    lblNoHuella.setSize(500, 60);
    lblOficina.setSize(120, 35);
    lblFuncion.setSize(120, 35);
    lblSanitizacion.setSize(120, 35);
    lblHuella.setSize(120, 35);
    lblTelefono.setSize(120, 35);

    txtNombre1 = new JTextField();
    txtNombre2 = new JTextField();
    txtApellido1 = new JTextField();
    txtApellido2 = new JTextField();
    txtDPI = new JTextField();
    txtSearchFuncion= new JTextField();
    cmbPrecedencia = new JComboBox();
    cmbPrecedencica2 = new JComboBox();
    cmbOficina = new JComboBox();
    cmbFuncion = new JComboBox();
    cmbSanitizacion = new JComboBox();
    txtTelefono= new JTextField();

    cmbSanitizacion.addItem("Si");
    cmbSanitizacion.addItem("No");

    txtDPI.setLocation(580, 50);
    txtNombre1.setLocation(580, 120);
    txtNombre2.setLocation(580, 190);
    txtApellido1.setLocation(580, 260);
    txtApellido2.setLocation(580, 330);
    txtSearchFuncion.setLocation(120, 540);
    cmbPrecedencia.setLocation(580, 400);
    cmbPrecedencica2.setLocation(580, 430);
    cmbOficina.setLocation(540, 585);
    cmbFuncion.setLocation(205, 540);
    cmbSanitizacion.setLocation(580, 615);
    txtTelefono.setLocation(580, 480);

    txtDPI.setSize(120, 35);
    txtNombre1.setSize(120, 35);
    txtNombre2.setSize(120, 35);
    txtApellido1.setSize(120, 35);
    txtApellido2.setSize(120, 35);
    txtSearchFuncion.setSize(80, 35);
    cmbPrecedencia.setSize(120, 35);
    cmbPrecedencica2.setSize(120, 35);
    cmbFuncion.setSize(500, 30);
    cmbOficina.setSize(160, 30);
    cmbSanitizacion.setSize(120, 30);
    txtTelefono.setSize(120, 30);

    txtDPI.setFont(font2);
    txtNombre1.setFont(font2);
    txtNombre2.setFont(font2);
    txtApellido1.setFont(font2);
    txtApellido2.setFont(font2);
    txtSearchFuncion.setFont(font2);
    cmbPrecedencia.setFont(font2);
    cmbPrecedencica2.setFont(font2);
    cmbFuncion.setFont(font2);
    cmbOficina.setFont(font2);
    cmbSanitizacion.setFont(font2);
    txtTelefono.setFont(font2);

    cmbOficina.setVisible(false);
    cmbFuncion.setVisible(false);
    cmbSanitizacion.setVisible(false);

    btnGuardarVisita = new JButton("Guardar");
    btnRegistrarHuella = new JButton("Registrar");
    btnVerificarHuella = new JButton("Verificar");
    btnDesbloquear = new JButton("Desbloquear");
    btnLimpiar = new JButton("Limpiar");
    btnImprimir = new JButton("");
    btnBuscarDPI = new JButton("Buscar");
    btnAgregarFuncion= new JButton("");

    btnVerificarHuella.setFont(font2);
    btnRegistrarHuella.setFont(font2);
    btnGuardarVisita.setFont(font2);
    btnDesbloquear.setFont(font2);
    btnLimpiar.setFont(font2);
    btnBuscarDPI.setFont(font2);

    btnVerificarHuella.setIcon(new ImageIcon(ClassLoader.getSystemResource("tick.png")));
    btnRegistrarHuella.setIcon(new ImageIcon(ClassLoader.getSystemResource("mas.png")));
    btnGuardarVisita.setIcon(new ImageIcon(ClassLoader.getSystemResource("form.png")));
    btnImprimir.setIcon(new ImageIcon(ClassLoader.getSystemResource("impresion.png")));
    btnAgregarFuncion.setIcon(new ImageIcon(ClassLoader.getSystemResource("proximo.png")));

    btnGuardarVisita.setLocation(555, 780);
    btnRegistrarHuella.setLocation(300, 780);
    btnVerificarHuella.setLocation(40, 780);
    btnDesbloquear.setLocation(700, 10);
    btnLimpiar.setLocation(750, 50);
    btnImprimir.setLocation(780, 120);
    btnBuscarDPI.setLocation(750, 220);
    btnAgregarFuncion.setLocation(720, 500);

    btnGuardarVisita.setSize(260, 70);
    btnRegistrarHuella.setSize(260, 70);
    btnVerificarHuella.setSize(260, 70);
    btnDesbloquear.setSize(130, 30);
    btnLimpiar.setSize(80, 30);
    btnImprimir.setSize(50, 80);
    btnBuscarDPI.setSize(80, 30);
    btnAgregarFuncion.setSize(40,40);
    cmbHuella= new JComboBox();
    cmbHuella.setLocation(580, 645);
    cmbHuella.setSize(120, 30);
    cmbHuella.addItem("Si");
    cmbHuella.addItem("No");
    cmbHuella.setVisible(false);
    cmbHuella.setEnabled(false);


    lblListaFunciones= new JLabel("");
    lblListaFunciones.setSize(650, 60);
    lblListaFunciones.setLocation(40, 650);
    lblListaFunciones.setVisible(true);

    JPanel panelContainer= new JPanel();
    panelContainer.setLayout(new BorderLayout());
    panelContainer.setPreferredSize(new Dimension(850, 930));

    JLabel idw= new JLabel("");
    idw.setSize(1, 1);
    idw.setLocation(0, 0);
    idw.setVisible(true);

    panelContainer.add(lblListaFunciones);
    panelContainer.add(cmbHuella);
    panelContainer.add(lblDPI);
    panelContainer.add(lblNombre1);
    panelContainer.add(lblNombre2);
    panelContainer.add(lblApellido1);
    panelContainer.add(lblApellido2);
    panelContainer.add(lblLugarPrecedencia);
    panelContainer.add(txtDPI);
    panelContainer.add(lblTelefono);
    panelContainer.add(txtTelefono);
    panelContainer.add(txtNombre1);
    panelContainer.add(txtNombre2);
    panelContainer.add(txtApellido1);
    panelContainer.add(txtApellido2);
    panelContainer.add(txtSearchFuncion);
    panelContainer.add(cmbPrecedencia);
    panelContainer.add(btnGuardarVisita);
    panelContainer.add(btnRegistrarHuella);
    panelContainer.add(btnVerificarHuella);
    panelContainer.add(cmbPrecedencica2);
    panelContainer.add(btnDesbloquear);
    panelContainer.add(lblNoHuella);
    panelContainer.add(lblFuncion);
    panelContainer.add(lblOficina);
    panelContainer.add(btnLimpiar);
    panelContainer.add(cmbFuncion);
    panelContainer.add(cmbOficina);
    panelContainer.add(btnImprimir);
    panelContainer.add(cmbSanitizacion);
    panelContainer.add(lblSanitizacion);
    panelContainer.add(lblHuella);
    panelContainer.add(btnBuscarDPI);
    panelContainer.add(btnAgregarFuncion);
    panelContainer.add(idw);

    panelContainer.setVisible(true);

    JScrollPane scrollPane= new JScrollPane(panelContainer, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setWheelScrollingEnabled(true);
    scrollPane.setVisible(true);
    scrollPane.setLocation(0, 0);
    scrollPane.setSize(850, 850);

    framePrimario.add(scrollPane);

  }

  public void getFrame(){
    this.framePrimario.setVisible(true);
  }

}
