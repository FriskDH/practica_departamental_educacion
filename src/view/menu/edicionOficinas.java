package view.menu;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class edicionOficinas {
  public JFrame framePrimario;
  private JLabel lblOficina;
  public static JTextField txtOficina;
  private Font font, font2;
  public JButton btnGuardar, btnCrear;
  public JComboBox cmbEdificio;

  public edicionOficinas() {
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);

    framePrimario = new JFrame();
    framePrimario.setLayout(null);
    framePrimario.getContentPane().setBackground(new Color(230, 230, 230));
    framePrimario.setSize(450, 230);
    framePrimario.setLocationRelativeTo(null);
    framePrimario.setLocationRelativeTo(null);
    framePrimario.setFont(font);

    txtOficina = new JTextField();
    txtOficina.setSize(250, 30);
    txtOficina.setFont(font2);
    txtOficina.setLocation(70, 15);
    txtOficina.setVisible(true);

    lblOficina = new JLabel("Oficina");
    lblOficina.setSize(50, 35);
    lblOficina.setLocation(15, 15);
    lblOficina.setVisible(true);

    btnGuardar = new JButton("Guardar");
    btnGuardar.setFont(font2);
    btnGuardar.setIcon(new ImageIcon("extras\\images\\form.png"));
    btnCrear = new JButton("Crear");
    btnCrear.setFont(font2);
    btnCrear.setIcon(new ImageIcon("extras\\images\\form.png"));

    btnGuardar.setLocation(50, 100);
    btnCrear.setLocation(250, 100);

    btnGuardar.setSize(160, 70);
    btnCrear.setSize(160, 70);

    cmbEdificio= new JComboBox();
    cmbEdificio.setLocation(15, 50);
    cmbEdificio.setSize(150, 30);
    cmbEdificio.setVisible(true);

    framePrimario.add(cmbEdificio);
    framePrimario.add(lblOficina);
    framePrimario.add(btnCrear);
    framePrimario.add(btnGuardar);
    framePrimario.add(txtOficina);
  }
}
