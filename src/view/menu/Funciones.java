package view.menu;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Funciones {
  public JFrame framePrincipal;
  private Font font, font2;
  public JTable tableFunciones;
  public JButton btnBuscar, btnEditar, btnCrear, btnEliminar;
  private JLabel lblFuncion;
  private JScrollPane txtScroll;
  public JTextArea txtFuncion;

  public Funciones() {
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);

    framePrincipal = new JFrame();
    framePrincipal.setLayout(null);
    framePrincipal.setSize(1000, 700);
    framePrincipal.setLocationRelativeTo(null);
    framePrincipal.setLocationRelativeTo(null);
    framePrincipal.setFont(font);

    lblFuncion = new JLabel("Función: ");
    lblFuncion.setSize(100, 50);
    lblFuncion.setFont(font);
    lblFuncion.setLocation(15, 30);
    lblFuncion.setVisible(true);

    txtFuncion = new JTextArea();
//    txtFuncion.setSize(650, 80);
    txtFuncion.setFont(font);
//    txtFuncion.setLocation(120, 40);
//    txtFuncion.setVisible(true);
    txtScroll= new JScrollPane(txtFuncion);
    txtScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    txtScroll.setSize(650, 80);
    txtScroll.setLocation(120, 40);
    txtScroll.setVisible(true);

    btnBuscar = new JButton("Buscar");
    btnBuscar.setSize(100, 50);
    btnBuscar.setFont(font);
    btnBuscar.setLocation(15, 150);
    btnBuscar.setVisible(true);

    btnEditar = new JButton("Editar");
    btnEditar.setSize(100, 50);
    btnEditar.setFont(font);
    btnEditar.setLocation(175, 150);
    btnEditar.setVisible(true);

    btnCrear = new JButton("Crear");
    btnCrear.setSize(100, 50);
    btnCrear.setFont(font);
    btnCrear.setLocation(330, 150);
    btnCrear.setVisible(true);

    btnEliminar = new JButton("Eliminar");
    btnEliminar.setSize(100, 50);
    btnEliminar.setFont(font);
    btnEliminar.setLocation(485, 150);
    btnEliminar.setVisible(true);

    tableFunciones =
      new JTable() {

        public boolean isCellEditable(int rowIndex, int colIndex) {
          return false;
        }
      };
    tableFunciones =
      new JTable(
        new DefaultTableModel(
          new Object[][] { null },
          new String[] { "Funcion" }
        )
      );
    tableFunciones.setVisible(true);
    tableFunciones.setFont(new Font("Arial", Font.PLAIN, 17));
    //
    JScrollPane scrollPane = new JScrollPane(tableFunciones);
    scrollPane.setVisible(true);
    scrollPane.setSize(950, 380);
    scrollPane.setLocation(15, 250);
    scrollPane.setFont(font2);
    //        scrollPane.setFont(new Font("Arial", Font.BOLD, 18));

//    txtFuncion.setColumns(700);
    framePrincipal.add(btnBuscar);
    framePrincipal.add(btnEditar);
    framePrincipal.add(btnCrear);
    framePrincipal.add(lblFuncion);
    //        framePrincipal.add(lblFuncion);
    framePrincipal.add(txtScroll);
    //        framePrincipal.add(cmbFuncion);
    framePrincipal.add(btnEliminar);
    //        framePrincipal.add(tableFunciones);
    framePrincipal.add(scrollPane);
    //        framePrincipal.add(tableFunciones.getTableHeader());

  }

  public void getFrame(){
    this.framePrincipal.setVisible(true);
  }
}
