package view.menu;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Oficinas {
  public JFrame framePrincipal;
  private Font font, font2;
  public JTable tableOficinas;
  public JButton btnBuscar, btnEditar, btnCrear, btnEliminar;
  private JLabel lblOficina;
  public JTextField txtOficina;

  public Oficinas() {
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);

    framePrincipal = new JFrame();
    framePrincipal.setLayout(null);
    framePrincipal.setSize(1000, 700);
    framePrincipal.setLocationRelativeTo(null);
    framePrincipal.setLocationRelativeTo(null);
    framePrincipal.setFont(font);

    lblOficina = new JLabel("Oficina: ");
    lblOficina.setSize(100, 50);
    lblOficina.setFont(font);
    lblOficina.setLocation(15, 30);
    lblOficina.setVisible(true);

    txtOficina = new JTextField();
    txtOficina.setSize(150, 30);
    txtOficina.setFont(font);
    txtOficina.setLocation(120, 40);
    txtOficina.setVisible(true);

    btnBuscar = new JButton("Buscar");
    btnBuscar.setSize(100, 50);
    btnBuscar.setFont(font);
    btnBuscar.setLocation(15, 150);
    btnBuscar.setVisible(true);

    btnEditar = new JButton("Editar");
    btnEditar.setSize(100, 50);
    btnEditar.setFont(font);
    btnEditar.setLocation(175, 150);
    btnEditar.setVisible(true);

    btnCrear = new JButton("Crear");
    btnCrear.setSize(100, 50);
    btnCrear.setFont(font);
    btnCrear.setLocation(330, 150);
    btnCrear.setVisible(true);

    btnEliminar = new JButton("Eliminar");
    btnEliminar.setSize(100, 50);
    btnEliminar.setFont(font);
    btnEliminar.setLocation(485, 150);
    btnEliminar.setVisible(true);

    tableOficinas =
      new JTable() {

        public boolean isCellEditable(int rowIndex, int colIndex) {
          return false;
        }
      };
    tableOficinas =
      new JTable(
        new DefaultTableModel(
          new Object[][] { null },
          new String[] { "Oficina", "Edificio" }
        )
      );

    //        tableOficinas.setLocation(15, 250);
    //        tableOficinas.setSize(900, 380);
    //        tableOficinas.setFillsViewportHeight(true);
    tableOficinas.setVisible(true);
    tableOficinas.setFont(new Font("Arial", Font.PLAIN, 17));
    //
    JScrollPane scrollPane = new JScrollPane(tableOficinas);
    scrollPane.setVisible(true);
    scrollPane.setSize(950, 380);
    scrollPane.setLocation(15, 250);
    //        scrollPane.setFont(new Font("Arial", Font.BOLD, 18));

    framePrincipal.add(btnBuscar);
    framePrincipal.add(btnEditar);
    framePrincipal.add(btnCrear);
    framePrincipal.add(lblOficina);
    //        framePrincipal.add(lblFuncion);
    framePrincipal.add(txtOficina);
    //        framePrincipal.add(cmbFuncion);
    framePrincipal.add(btnEliminar);
    //        framePrincipal.add(tableOficinas);
    framePrincipal.add(scrollPane);
    //        framePrincipal.add(tableOficinas.getTableHeader());

  }

  public void getFrame(){
    this.framePrincipal.setVisible(true);
  }
}
