package view.menu;

import java.awt.*;
import javax.swing.*;

public class Reportes {
  public JFrame framePrincipal;
  public JButton btnOficina, btnDepartamentos, btnFecha, btnGenerarReporte, btnReset;
  public JLabel lblTipoReporte;
  public JComboBox cmbTipoReporte, cmbOficina, cmbDepartamento;
  private Font font, font2;
  private JLabel lblIcon;
  public JRadioButton rbtnOficinas1Date, rbtnOficinas2Dates, rbtnDptos1Date, rbtnDptos2Dates, rbtn1Date, rbtn2Dates;
  public JTextField txtFecha;

  public Reportes() {
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);

    framePrincipal = new JFrame();
    framePrincipal.setLayout(null);
    //framePrincipal.getContentPane().setBackground(new Color(245,245,245));
    framePrincipal.setSize(600, 500);
    framePrincipal.setLocationRelativeTo(null);
    //        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    framePrincipal.setLocationRelativeTo(null);
    framePrincipal.setFont(font);
    lblIcon = new JLabel();
    lblTipoReporte = new JLabel("Tipo de Reporte");
    lblTipoReporte.setFont(font);
    //lblIcon.setIcon(new ImageIcon("extras\\images\\blackboard.jpg"));
    lblIcon.setSize(1400, 700);

    lblTipoReporte.setLocation(240, 25);
    lblTipoReporte.setSize(200, 30);
    lblTipoReporte.setVisible(true);

    //DEPARTAMENTO
    cmbDepartamento = new JComboBox();
    cmbDepartamento.setLocation(200, 150);
    cmbDepartamento.setSize(150, 40);
    cmbDepartamento.setFont(font2);
    cmbDepartamento.setVisible(false);

    //OFICINA
    cmbOficina = new JComboBox();
    cmbOficina.setLocation(200, 150);
    cmbOficina.setSize(150, 40);
    cmbOficina.setFont(font2);
    cmbOficina.setVisible(false);

    btnGenerarReporte = new JButton("Generar Reporte");
    btnGenerarReporte.setSize(250, 50);
    btnGenerarReporte.setFont(font2);
    btnGenerarReporte.setLocation(300, 300);

    btnFecha = new JButton("Fecha");
    btnFecha.setSize(140, 50);
    btnFecha.setFont(font2);
    btnFecha.setLocation(25, 90);

    rbtn1Date = new JRadioButton("Fecha unica");
    rbtn1Date.setLocation(50, 210);
    rbtn1Date.setSize(100, 20);
    rbtn1Date.setVisible(false);

    rbtn2Dates = new JRadioButton("Plazo definido");
    rbtn2Dates.setLocation(50, 240);
    rbtn2Dates.setSize(100, 20);
    rbtn2Dates.setVisible(false);

    btnDepartamentos = new JButton("Departamento");
    btnDepartamentos.setSize(140, 50);
    btnDepartamentos.setFont(font2);
    btnDepartamentos.setLocation(220, 90);

    rbtnDptos1Date = new JRadioButton("Fecha unica");
    rbtnDptos1Date.setLocation(250, 210);
    rbtnDptos1Date.setSize(100, 20);
    rbtnDptos1Date.setVisible(false);

    rbtnDptos2Dates = new JRadioButton("Plazo definido");
    rbtnDptos2Dates.setLocation(250, 240);
    rbtnDptos2Dates.setSize(100, 20);
    rbtnDptos2Dates.setVisible(false);

    btnOficina = new JButton("Oficina");
    btnOficina.setSize(140, 50);
    btnOficina.setFont(font2);
    btnOficina.setLocation(400, 90);

    rbtnOficinas1Date = new JRadioButton("Fecha unica");
    rbtnOficinas1Date.setLocation(250, 210);
    rbtnOficinas1Date.setSize(100, 20);
    rbtnOficinas1Date.setVisible(false);

    rbtnOficinas2Dates = new JRadioButton("Plazo definido");
    rbtnOficinas2Dates.setLocation(250, 240);
    rbtnOficinas2Dates.setSize(100, 20);
    rbtnOficinas2Dates.setVisible(false);

    btnReset = new JButton("Reset");
    btnReset.setSize(100, 30);
    btnReset.setFont(font2);
    btnReset.setLocation(450, 250);
    btnReset.setVisible(true);

    framePrincipal.add(lblTipoReporte);
    framePrincipal.add(btnGenerarReporte);
    //        framePrincipal.add(cmbTipoReporte);
    framePrincipal.add(btnDepartamentos);
    framePrincipal.add(btnFecha);
    framePrincipal.add(btnOficina);
    framePrincipal.add(cmbDepartamento);
    framePrincipal.add(cmbOficina);
    framePrincipal.add(btnReset);
    framePrincipal.add(rbtnDptos1Date);
    framePrincipal.add(rbtnDptos2Dates);
    framePrincipal.add(rbtnOficinas1Date);
    framePrincipal.add(rbtnOficinas2Dates);
    framePrincipal.add(rbtn1Date);
    framePrincipal.add(rbtn2Dates);
  }

  public void getFrame(){
    this.framePrincipal.setVisible(true);
  }
}
