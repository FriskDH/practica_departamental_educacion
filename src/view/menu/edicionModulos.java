package view.menu;

import javax.swing.*;
import java.awt.*;

public class edicionModulos {
    public JFrame framePrimario;
    private JLabel lblModulo;
    public static JTextField txtModulo;
    private Font font, font2;
    public JButton btnGuardar, btnCrear;
    
    public edicionModulos(){
        font = new Font("Carlito", Font.BOLD, 17);
        font2 = new Font("Carlito", Font.PLAIN, 14);

        framePrimario = new JFrame();
        framePrimario.setLayout(null);
        framePrimario.getContentPane().setBackground(new Color(230, 230, 230));
        framePrimario.setSize(450, 230);
        framePrimario.setLocationRelativeTo(null);
        framePrimario.setLocationRelativeTo(null);
        framePrimario.setFont(font);

        txtModulo = new JTextField();
        txtModulo.setSize(200, 30);
        txtModulo.setFont(font2);
        txtModulo.setLocation(70, 15);
        txtModulo.setVisible(true);

        lblModulo = new JLabel("Módulo");
        lblModulo.setSize(50, 35);
        lblModulo.setLocation(15, 15);
        lblModulo.setVisible(true);

        btnGuardar = new JButton("Guardar");
        btnGuardar.setFont(font2);
        btnGuardar.setIcon(new ImageIcon("extras\\images\\form.png"));
        btnCrear = new JButton("Crear");
        btnCrear.setFont(font2);
        btnCrear.setIcon(new ImageIcon("extras\\images\\form.png"));

        btnGuardar.setLocation(50, 100);
        btnCrear.setLocation(250, 100);

        btnGuardar.setSize(160, 70);
        btnCrear.setSize(160, 70);

        framePrimario.add(lblModulo);
        framePrimario.add(btnCrear);
        framePrimario.add(btnGuardar);
        framePrimario.add(txtModulo);
    }
}
