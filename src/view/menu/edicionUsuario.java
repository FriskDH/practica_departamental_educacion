package view.menu;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class edicionUsuario {
  public JFrame framePrimario;
  private JLabel lblNombre1, lblNombre2, lblApellido1, lblApellido2, lblDPI, lblLugarPrecedencia, lblIcon;
  public static JLabel lblNoHuella, lblOficina, lblOficinaActual, lblPassword, lblRol, lblRolActual;
  public static JTextField txtNombre1, txtNombre2, txtApellido1, txtApellido2, txtDPI;
  public static JPasswordField txtPassword;
  public static JComboBox cmbOficina, cmbRol;
  private Font font, font2;
  public JButton btnGuardar, btnCrear;
  public JTable tableFuncionees;

  public edicionUsuario() {
    font = new Font("Carlito", Font.BOLD, 17);
    font2 = new Font("Carlito", Font.PLAIN, 14);

    framePrimario = new JFrame();
    framePrimario.setLayout(null);
    framePrimario.getContentPane().setBackground(new Color(230, 230, 230));
    framePrimario.setSize(550, 930);
    framePrimario.setLocationRelativeTo(null);
    framePrimario.setLocationRelativeTo(null);
    framePrimario.setFont(font);
    lblIcon = new JLabel();
    //        lblIcon.setIcon(new ImageIcon("extras\\images\\"));
    lblIcon.setSize(550, 800);

    lblDPI = new JLabel("ID");
    lblNombre1 = new JLabel("Primer Nombre");
    lblNombre2 = new JLabel("Segundo Nombre");
    lblApellido1 = new JLabel("Primer Apellido");
    lblApellido2 = new JLabel("Segundo Apellido");
    lblLugarPrecedencia = new JLabel("Lugar de Precedencia");
    lblOficina = new JLabel("Oficina");
    lblPassword = new JLabel("Contraseña");
    lblRol= new JLabel("Rol");
    lblOficinaActual = new JLabel();
    lblRolActual= new JLabel();

    lblDPI.setFont(font);
    lblNombre1.setFont(font);
    lblNombre2.setFont(font);
    lblApellido1.setFont(font);
    lblApellido2.setFont(font);
    lblOficinaActual.setFont(font2);
    lblRolActual.setFont(font2);
    lblPassword.setFont(font);
    lblOficina.setFont(font);
    lblRol.setFont(font);
    //        txtAreaRequerimientos.setFont(font);

    lblDPI.setLocation(40, 50);
    lblNombre1.setLocation(40, 120);
    lblNombre2.setLocation(40, 190);
    lblApellido1.setLocation(40, 260);
    lblApellido2.setLocation(40, 330);
    lblOficinaActual.setLocation(280, 420);
    lblRolActual.setLocation(280, 475);
    lblPassword.setLocation(40, 500);
    lblOficina.setLocation(40, 390);
    lblRol.setLocation(40, 450);

    lblDPI.setSize(120, 35);
    lblOficina.setSize(120, 35);
    lblRol.setSize(120, 35);
    lblNombre1.setSize(200, 35);
    lblNombre2.setSize(200, 35);
    lblApellido1.setSize(200, 35);
    lblApellido2.setSize(200, 35);
    lblOficinaActual.setSize(250, 35);
    lblRolActual.setSize(250, 35);
    lblPassword.setSize(250, 35);

    txtNombre1 = new JTextField();
    txtNombre2 = new JTextField();
    txtApellido1 = new JTextField();
    txtApellido2 = new JTextField();
    txtDPI = new JTextField();
    txtPassword = new JPasswordField();

    cmbOficina = new JComboBox();
    cmbRol= new JComboBox();

    txtDPI.setLocation(280, 50);
    txtNombre1.setLocation(280, 120);
    txtNombre2.setLocation(280, 190);
    txtApellido1.setLocation(280, 260);
    txtApellido2.setLocation(280, 330);
    cmbOficina.setLocation(280, 390);
    cmbRol.setLocation(280, 450);
    txtPassword.setLocation(280, 500);

    txtDPI.setSize(120, 35);
    txtNombre1.setSize(120, 35);
    txtNombre2.setSize(120, 35);
    txtApellido1.setSize(120, 35);
    txtApellido2.setSize(120, 35);
    cmbOficina.setSize(170, 35);
    cmbRol.setSize(140, 35);
    txtPassword.setSize(120, 35);

    txtDPI.setFont(font2);
    txtNombre1.setFont(font2);
    txtNombre2.setFont(font2);
    txtApellido1.setFont(font2);
    txtApellido2.setFont(font2);
    cmbOficina.setFont(font2);
    cmbRol.setFont(font2);
    cmbOficina.setVisible(true);
    cmbRol.setVisible(true);
    btnGuardar = new JButton("Guardar");
    btnGuardar.setFont(font2);
    btnGuardar.setIcon(new ImageIcon("extras\\images\\form.png"));
    btnCrear = new JButton("Crear");
    btnCrear.setFont(font2);
    btnCrear.setIcon(new ImageIcon("extras\\images\\form.png"));

    btnGuardar.setLocation(50, 780);
    btnCrear.setLocation(250, 780);

    btnGuardar.setSize(160, 70);
    btnCrear.setSize(160, 70);

    tableFuncionees =
      new JTable(
        new DefaultTableModel(
          new Object[][] { null },
          new String[] { "No.", "Funcion", "Si / No" }
        )
      );
    tableFuncionees.setVisible(true);
    tableFuncionees.setFont(new Font("Carlito", Font.PLAIN, 15));
    JScrollPane scrollPane = new JScrollPane(tableFuncionees);
    scrollPane.setVisible(true);
    scrollPane.setSize(500, 180);
    scrollPane.setLocation(15, 580);

    framePrimario.add(lblDPI);
    framePrimario.add(lblNombre1);
    framePrimario.add(lblNombre2);
    framePrimario.add(lblApellido1);
    framePrimario.add(lblApellido2);
    framePrimario.add(txtDPI);
    framePrimario.add(txtNombre1);
    framePrimario.add(txtNombre2);
    framePrimario.add(txtApellido1);
    framePrimario.add(txtApellido2);
    framePrimario.add(lblOficina);
    framePrimario.add(lblRol);
    framePrimario.add(cmbOficina);
    framePrimario.add(btnCrear);
    framePrimario.add(btnGuardar);
    framePrimario.add(lblOficinaActual);
    framePrimario.add(lblRolActual);
    framePrimario.add(lblPassword);
    framePrimario.add(txtPassword);
    framePrimario.add(scrollPane);
    framePrimario.add(cmbRol);
  }
}
