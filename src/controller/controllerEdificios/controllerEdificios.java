package controller.controllerEdificios;

import controller.controllerRoles.controllerMenuEditar_Crear_Roles;
import model.connection.connection;
import view.menu.Edificios;
import view.menu.Roles;
import view.menu.edicionEdificios;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

public class controllerEdificios implements ActionListener{
    DefaultTableModel edificiosTable, edificiosTableShow;
    connection conn;
    String[] rowStrings, rowStringsShow;
    Edificios adminEdificios;
    
    public controllerEdificios(Edificios adminEdificios){
        this.adminEdificios = adminEdificios;
        this.adminEdificios.btnBuscar.addActionListener(this);
        this.adminEdificios.btnCrear.addActionListener(this);
        this.adminEdificios.btnEditar.addActionListener(this);
        this.adminEdificios.btnEliminar.addActionListener(this);
        rowStrings = new String[2];
        rowStringsShow = new String[1];
        edificiosTable = new DefaultTableModel();
        edificiosTable =
                new DefaultTableModel(
                        new Object[][]{null},
                        new String[]{"ID", "Edificio"}
                ) {

                    //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };

        edificiosTableShow = new DefaultTableModel();
        edificiosTableShow =
                new DefaultTableModel(
                        new Object[][]{null},
                        new String[]{"Edificio"}
                ) {

                    //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.adminEdificios.btnBuscar == e.getSource()) {
            deleteData();

            if (this.adminEdificios.txtEdificio.getText().equals("")) {
                searchEmpty();
            } else {
                searchNotEmpty();
            }
        }

        if (this.adminEdificios.btnCrear == e.getSource()) {
            create();
        }

        if (this.adminEdificios.btnEditar == e.getSource()) {
            edit();
        }

        if (this.adminEdificios.btnEliminar == e.getSource()) {
            try {
                delete();
                deleteData();
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(null, exception.toString());
            }
        }
    }


    /*===============
    BORRAR
    ===============*/
    protected void delete() throws Exception {
        try {
            this.conn = new connection();
            conn.deleteEdificio(Integer.parseInt(edificiosTable.getValueAt(this.adminEdificios.tableEdificios.getSelectedRow(), 0).toString()));
            conn.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            if (conn != null) {
                try {
                    conn.getConn().rollback();
                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                    JOptionPane.showMessageDialog(null,sqlException.toString());
                }
            }
            JOptionPane.showMessageDialog(null, ex.toString());
//      ex.printStackTrace();
        }
    }

    /*============================
    BORRAR DATOS EN TABLA
    ============================*/
    protected void deleteData() {
        for (
                int i = this.edificiosTable.getRowCount() - 1;
                i >= 0;
                i--
        ){
            this.edificiosTable.removeRow(i);
            this.edificiosTableShow.removeRow(i);}
    }


    /*===============
    CREAR
    ===============*/
    protected void create() {
        edicionEdificios edEd = new edicionEdificios();
        controllerMenuEditar_Crear_Edificios editarCrear = new controllerMenuEditar_Crear_Edificios(
                edEd,
                "C",
                "",
                0
        );

        edEd.framePrimario.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
                Frame frame = (Frame) e.getSource();
                refreshTable();
            }
        });

        edEd.framePrimario.setVisible(true);
    }


    /*===============
    EDITAR
    ===============*/
    protected void edit() {
        edicionEdificios edEd = new edicionEdificios();

        controllerMenuEditar_Crear_Edificios editarCrear = new controllerMenuEditar_Crear_Edificios(
                edEd,
                "E",
                this.edificiosTable.getValueAt(
                        this.adminEdificios.tableEdificios.getSelectedRow(),
                        1
                )
                        .toString(),
                Integer.parseInt(
                        this.edificiosTable.getValueAt(
                                this.adminEdificios.tableEdificios.getSelectedRow(),
                                0
                        )
                                .toString()
                )
        );

        edEd.framePrimario.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
                Frame frame = (Frame) e.getSource();
                refreshTable();
            }
        });

        edEd.framePrimario.setVisible(true);
    }

    /*=============================
    BUSCAR CUANDO TXT VACIO
    =============================*/
    protected void searchEmpty(){
        try {

            conn = new connection();
            ResultSet res = conn.crudEdificios(2, 0, "");
            while (res.next()) {
                rowStrings[0] = res.getString(1);
                rowStrings[1] = res.getString(2);
                rowStringsShow[0]= res.getString(2);
                this.edificiosTable.addRow(rowStrings);
                this.edificiosTableShow.addRow(rowStringsShow);
            }
            res.close();
            conn.closeConnection();
            this.adminEdificios.tableEdificios.setModel(edificiosTableShow);
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(
                    null,
                    "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
            );
            //                    exc.printStackTrace();
        }
    }

    /*=============================
    BUSCAR CUANDO TXT NO VACIO
    =============================*/
    protected void searchNotEmpty(){
        try {
            conn = new connection();
            ResultSet res = conn.crudRoles(5, 0,
                    this.adminEdificios.txtEdificio.getText()
            );
            if (res == null) {
                JOptionPane.showMessageDialog(
                        null,
                        "Error, el edificio buscado no existe."
                );
            } else {
                while (res.next()) {
                    rowStrings[0] = res.getString(1);
                    rowStrings[1] = res.getString(2);
                    rowStringsShow[0]= res.getString(2);
                    this.edificiosTable.addRow(rowStrings);
                    this.edificiosTableShow.addRow(rowStringsShow);
                }
            }
            res.close();
            conn.closeConnection();
            this.adminEdificios.tableEdificios.setModel(edificiosTable);
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(
                    null,
                    "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
            );
            //                   exc.printStackTrace();
        }
    }

    /*===================
    REFRESCAR TABLA
    ===================*/
    protected void refreshTable(){
        deleteData();
        searchEmpty();
    }
}
