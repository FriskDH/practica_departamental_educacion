package controller.controllerEdificios;

//import com.sun.corba.se.spi.activation.ActivatorOperations;
import model.connection.connection;
import view.menu.edicionEdificios;
import view.menu.edicionModulos;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class controllerMenuEditar_Crear_Edificios implements ActionListener {
    int idedificio;
    connection conn;
    edicionEdificios edEd;
            
    public controllerMenuEditar_Crear_Edificios(
            edicionEdificios edEd,
            String mode,
            String modulo,
            int idedificio
    ) {
        this.edEd = edEd;
        this.idedificio = idedificio;

        if (mode.equals("C")) {
            this.edEd.btnCrear.addActionListener(this);
            this.edEd.btnGuardar.setVisible(false);
            this.edEd.btnCrear.setVisible(true);
        }
        if (mode.equals("E")) {
            this.edEd.btnGuardar.addActionListener(this);
            this.edEd.txtEdificio.setText(modulo);
            this.edEd.btnGuardar.setVisible(true);
            this.edEd.btnCrear.setVisible(false);
            this.edEd.txtEdificio.setText(modulo);
        }



    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.edEd.btnCrear == e.getSource()) {

            try {
                conn = new connection();
                conn.createEdificio(this.edEd.txtEdificio.getText());
                    this.edEd.framePrimario.dispose();
                    conn.closeConnection();
            }

            catch (Exception throwables) {
                try {
                    if (conn != null)
                        conn.getConn().rollback();
                } catch (SQLException se2) {
                    //se2.printStackTrace();
                    JOptionPane.showMessageDialog(null, throwables.toString());
                }
            }
        }

        if (this.edEd.btnGuardar == e.getSource()) {
            try {
                conn = new connection();
                conn.updateEdificio(this.idedificio, this.edEd.txtEdificio.getText());
                    this.edEd.framePrimario.dispose();
                    conn.closeConnection();
            }  //        classNotFoundException.printStackTrace();
            catch (Exception throwables) {
                if (conn != null) {
                    try {
                        conn.getConn().rollback();
                    } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                        JOptionPane.showMessageDialog(null,sqlException.toString());
                    }
                }
                JOptionPane.showMessageDialog(null, throwables.toString());
            }
        }
    }
    
}
