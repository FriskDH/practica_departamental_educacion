package controller.controllerMenus;

import controller.*;
import controller.scanner.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import model.userData.*;
import view.menu.*;

public class controllerMenuPrincipal {
    menuPrincipal fPrincipal = new menuPrincipal();
    fingerPrint newFingerPrint;
    verifyFingerPrint scanFingerPrint;
    LinkedList<String> modules;
    LinkedList<Class> controllers;
    LinkedList<Class> views;


    public controllerMenuPrincipal(menuPrincipal framePrincipal) {
        this.fPrincipal = framePrincipal;
        this.modules = (LinkedList<String>) Data.getAccessNameList();
        this.controllers = new LinkedList<>();
        this.views = new LinkedList<>();

    /*=======================================================================
    COMPROBAR SI LAS VISTAS Y CONTROLADORES DE LOS MODULOS ASIGNADOS EXISTEN
    ========================================================================*/
        Iterator it2 = this.modules.iterator();
        while (it2.hasNext()) {
            String t = it2.next().toString();
            try {
                Class temp = Class.forName("controller." + "controller" + t + "." + "controller" + t);
                try {
                    Class tempView = Class.forName("view." + "menu" + "." + t);
                    this.controllers.add(temp);
                    this.views.add(tempView);

                    /*=================================================
                    GENERAR EL ITEMMENU DEL MODULO Y AGREGAR EL LISTENER
                    =================================================*/
                    String newItemName = t;
                    this.fPrincipal.menus.add(new JMenuItem(newItemName)).addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            try {
                                Object invokeObject= tempView.newInstance();
                                Object controller = temp.getDeclaredConstructor(tempView).newInstance(invokeObject);
                                tempView.getMethod("getFrame").invoke(invokeObject);

                            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException noSuchMethodException) {
                                JOptionPane.showMessageDialog(null, noSuchMethodException.toString());

                            }  //                                invocationTargetException.printStackTrace();

                        }
                    });

                } catch (Exception | NoClassDefFoundError ex2) {

                    JOptionPane.showMessageDialog(null, "Error, no se han implementado los menús: " + t);
                }
            } catch (Exception | NoClassDefFoundError ex1) {

                JOptionPane.showMessageDialog(null, "Error, no se han implementado los controladores del modulo: " + t);
            }

        }

    }

}
