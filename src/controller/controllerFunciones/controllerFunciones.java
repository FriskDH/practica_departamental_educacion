package controller.controllerFunciones;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import model.connection.connection;
import view.menu.Funciones;
import view.menu.edicionFunciones;

public class controllerFunciones implements ActionListener {
    DefaultTableModel funcionesTable, funcionesTableShow;
    connection conn;
    String[] rowStrings, rowStringsShow;
    Funciones adminFunciones;


    public controllerFunciones(Funciones adminFunciones) {
        this.adminFunciones = adminFunciones;
        this.adminFunciones.btnBuscar.addActionListener(this);
        this.adminFunciones.btnCrear.addActionListener(this);
        this.adminFunciones.btnEditar.addActionListener(this);
        this.adminFunciones.btnEliminar.addActionListener(this);
        rowStrings = new String[2];
        rowStringsShow = new String[1];
        funcionesTableShow = new DefaultTableModel();
        funcionesTableShow =
                new DefaultTableModel(
                        new Object[][]{null},
                        new String[]{"Funciones"}
                ) {

                    //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };

        funcionesTable = new DefaultTableModel();
        funcionesTable =
                new DefaultTableModel(
                        new Object[][]{null},
                        new String[]{"ID", "Funciones"}
                ) {

                    //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.adminFunciones.btnBuscar == e.getSource()) {
            deleteData();

            if (this.adminFunciones.txtFuncion.getText().equals("")) {
                searchEmpty();
            } else {
                searchNotEmpty();
            }
        }


        if (this.adminFunciones.btnCrear == e.getSource()) {
            create();
        }

        if (this.adminFunciones.btnEditar == e.getSource()) {
            edit();
        }

        if (this.adminFunciones.btnEliminar == e.getSource()) {
            delete();

        }
    }

    /*========================
    BORRAR
    =========================*/
    protected void delete() {
        try {
            this.conn = new connection();
            conn.assignFuncionesToOficinas(9, Integer.parseInt(this.funcionesTable.getValueAt(this.adminFunciones.tableFunciones.getSelectedRow(), 0).toString()),
                    0);
            conn.closeConnection();
        } catch (Exception ex) {
            if (conn != null) {
                try {
                    conn.getConn().rollback();
                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                    JOptionPane.showMessageDialog(null,sqlException.toString());
                }
            }
            JOptionPane.showMessageDialog(null, ex.toString());
//      ex.printStackTrace();
        }
        try {
            this.conn = new connection();
            conn.deleteFunciones(Integer.parseInt(this.funcionesTable.getValueAt(this.adminFunciones.tableFunciones.getSelectedRow(), 0).toString()));

            refreshTable();

        } catch (Exception ex) {
            if (conn != null) {
                try {
                    conn.getConn().rollback();
                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                    JOptionPane.showMessageDialog(null,sqlException.toString());
                }
            }
            JOptionPane.showMessageDialog(null, ex.toString());
//      ex.printStackTrace();
        }
    }

    /*========================
    BORRAR DATOS EN LA TABLA
    =========================*/
    protected void deleteData() {

        for (
                int i = this.funcionesTable.getRowCount() - 1;
                i >= 0;
                i--
        ) {
            this.funcionesTable.removeRow(i);
            this.funcionesTableShow.removeRow(i);
        }
    }

    /*========================
    CREAR
    =========================*/
    protected void create() {
        edicionFunciones edFn = new edicionFunciones();
        controllerMenuEditar_Crear_Funciones editarCrear = new controllerMenuEditar_Crear_Funciones(
                edFn,
                "C",
                "",
                0
        );

        edFn.framePrimario.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
                Frame frame = (Frame) e.getSource();
                refreshTable();
            }
        });

        edFn.framePrimario.setVisible(true);


    }


    /*========================
    EDITAR
    =========================*/
    protected void edit() {
        edicionFunciones edFn = new edicionFunciones();

        controllerMenuEditar_Crear_Funciones editarCrear = new controllerMenuEditar_Crear_Funciones(
                edFn,
                "E",
                this.funcionesTable.getValueAt(
                        this.adminFunciones.tableFunciones.getSelectedRow(),
                        1
                )
                        .toString(),
                Integer.parseInt(
                        this.funcionesTable.getValueAt(
                                this.adminFunciones.tableFunciones.getSelectedRow(),
                                0
                        )
                                .toString()
                )
        );

        edFn.framePrimario.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
                Frame frame = (Frame) e.getSource();
                refreshTable();
            }
        });

        edFn.framePrimario.setVisible(true);


    }

    /*=============================
    SEARCH WHEN TEXTFIELD IS EMPTY
    =============================*/
    protected void searchEmpty() {
        try {
            conn = new connection();
            ResultSet res = conn.getFunciones();
            while (res.next()) {
                rowStrings[0] = res.getString(1);
                rowStrings[1] = res.getString(2);
                rowStringsShow[0] = res.getString(2);
                this.funcionesTable.addRow(rowStrings);
                this.funcionesTableShow.addRow(rowStringsShow);
            }
            res.close();
            conn.closeConnection();
            this.adminFunciones.tableFunciones.setModel(funcionesTableShow);

        } catch (Exception exc) {
            JOptionPane.showMessageDialog(
                    null,
                    "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
            );
//      exc.printStackTrace();
        }
    }

    /*==================================
    SEARCH WHEN TEXTFIELD IS NOT EMPTY
    ==================================*/
    protected void searchNotEmpty() {
        try {
            conn = new connection();
            ResultSet res = conn.getFuncionesByName(
                    this.adminFunciones.txtFuncion.getText()
            );
            if (res == null) {
                JOptionPane.showMessageDialog(
                        null,
                        "Error, la función buscada no existe."
                );
            } else {
                while (res.next()) {
                    rowStrings[0] = res.getString(1);
                    rowStrings[1] = res.getString(2);
                    rowStringsShow[0] = res.getString(2);
                    this.funcionesTable.addRow(rowStrings);
                    this.funcionesTableShow.addRow(rowStringsShow);
                }
            }
            res.close();
            conn.closeConnection();
            this.adminFunciones.tableFunciones.setModel(funcionesTableShow);
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(
                    null,
                    "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
            );
            //                   exc.printStackTrace();
        }
    }

    /*=======================================================================
    REFRESCAR TABLA
    (COMBINACION DE BORRAR DATOS EN TABLA Y DESPUES LLENARLA)
    SOLO PARA CUANDO EL FUNCIONARIO (USUARIO) GUARDA O EDITAR ALGUNA FUNCION
    =======================================================================*/
    public void refreshTable() {
        deleteData();
        searchEmpty();
    }

}

