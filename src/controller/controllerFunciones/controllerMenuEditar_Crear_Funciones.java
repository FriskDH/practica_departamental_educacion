package controller.controllerFunciones;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import model.connection.connection;
import view.menu.Funciones;
import view.menu.edicionFunciones;

public class controllerMenuEditar_Crear_Funciones implements ActionListener {
  edicionFunciones edFn;
  connection conn;
  int idFuncion;
  DefaultTableModel tableFunciones, tableFuncionesShow;
  List<Integer> listFuncionesAprobadas;
  List<Object> listSendNewFunciones;
  List<Object> listIndexSendNewFunciones;
  Vector<Object> indexFuncionesOriginales;
  Vector<Object> funcionesCambiadas;
  Vector<Object> funcionesOriginales;

  public controllerMenuEditar_Crear_Funciones(
    edicionFunciones edFn,
    String mode,
    String funcion,
    int idFuncion
  ) {
    this.edFn = edFn;
    this.idFuncion = idFuncion;

    listFuncionesAprobadas = new LinkedList<>();
    listIndexSendNewFunciones = new LinkedList<>();
    listSendNewFunciones = new LinkedList<>();
    tableFunciones = new DefaultTableModel();
    
    tableFunciones =
      new DefaultTableModel(
        new Object[][] { null },
        new String[] { "ID", "Oficina", "Si/No" }
      ) {

        @Override
        public boolean isCellEditable(int rowIndex, int colIndex) {
          switch (colIndex) {
            case 0:
              return false;
            case 1:
              return false;
            case 2:
              return true;
            default:
              return false;
          }
        }

        public Class<?> getColumnClass(int column) {
          switch (column) {
            case 0:
              return String.class;
            case 1:
              return String.class;
            case 2:
              return Boolean.class;
            default:
              return String.class;
          }
        }
      };
    
    tableFuncionesShow =
      new DefaultTableModel(
        new Object[][] { null },
        new String[] { "Oficina", "Si/No" }
      ) {

        @Override
        public boolean isCellEditable(int rowIndex, int colIndex) {
          switch (colIndex) {
            case 0:
              return false;
//            case 1:
//              return false;
            case 1:
              return true;
            default:
              return false;
          }
        }

        public Class<?> getColumnClass(int column) {
          switch (column) {
            case 0:
              return String.class;
//            case 1:
//              return String.class;
            case 1:
              return Boolean.class;
            default:
              return String.class;
          }
        }
      };

    if (mode.equals("C")) {
      this.edFn.btnCrear.addActionListener(this);
      this.edFn.btnGuardar.setVisible(false);
      this.edFn.btnCrear.setVisible(true);
    }
    if (mode.equals("E")) {
      this.edFn.btnGuardar.addActionListener(this);
      this.edFn.txtFuncion.setText(funcion);
      this.edFn.btnGuardar.setVisible(true);
      this.edFn.btnCrear.setVisible(false);
    }

    fillTable();
    setOficinasTrueOrFalse();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (this.edFn.btnCrear == e.getSource()) {
      boolean verificationNewFunciones = false;
      boolean verificationFunciones = false;
      this.listIndexSendNewFunciones.clear();
      this.listSendNewFunciones.clear();

      /*=================
      CREAR FUNCION
      =================*/
      if (verifyFields()) {
        try {

          conn = new connection();
//          System.out.println(this.edFn.txtFuncion.getText());
          conn.createFunciones(this.edFn.txtFuncion.getText());
          conn.closeConnection();

        } catch (Exception throwables) {
          if (conn != null) {
            try {
              conn.getConn().rollback();
            } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
              JOptionPane.showMessageDialog(null,sqlException.toString());
            }
          }
          JOptionPane.showMessageDialog(null, throwables.toString());
        }

        for (int i = 0; i <= this.tableFunciones.getRowCount() - 1; i++) {
          //INSERTAR FUNCIONES NUEVAS
          if (
                  this.edFn.tableFuncionees.getValueAt(i, 1)
                          .toString()
                          .equals("true")
          ) {
            try {

              conn = new connection();
              conn.assignFuncionesToOficinas(8, 0, Integer.parseInt(this.tableFunciones.getValueAt(i, 0).toString()));
              conn.closeConnection();
              
            } catch (Exception throwables) {
              if (conn != null) {
                try {
                  conn.getConn().rollback();
                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                  JOptionPane.showMessageDialog(null,sqlException.toString());
                }
              }
              JOptionPane.showMessageDialog(null, throwables.toString());
            }
          }
        }

        this.edFn.framePrimario.dispose();
      } else {
        JOptionPane.showMessageDialog(
          null,
          "Todos los campos son requeridos, y se debe asignar al menos una función"
        );
      }
    }
    
    /*=====================
    GUARDAR CAMBIOS
    =====================*/
    if (this.edFn.btnGuardar == e.getSource()) {
      boolean verificationOficina = false;
      boolean verificationNewFunciones = false;
      boolean verificationFunciones = false;
      this.funcionesCambiadas.clear();
      this.listIndexSendNewFunciones.clear();
      this.listSendNewFunciones.clear();

      //GUARDAR NOMBRE
      try {
        conn = new connection();
        conn.updateFunciones(this.idFuncion, this.edFn.txtFuncion.getText());
        conn.closeConnection();
      } catch (Exception throwables) {
        if (conn != null) {
          try {
            conn.getConn().rollback();
          } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
            JOptionPane.showMessageDialog(null,sqlException.toString());
          }
        }
        JOptionPane.showMessageDialog(null, throwables.toString());
      }

      //CICLO PARA AGREGAR LAS OFICINAS NUEVAS A LA LISTA
      for (int i = 0; i <= tableFunciones.getRowCount() - 1; i++) {
        this.funcionesCambiadas.add(this.edFn.tableFuncionees.getValueAt(i, 1));
      }

      for (int i = 0; i <= indexFuncionesOriginales.size() - 1; i++) {
        if (funcionesOriginales.get(i) != funcionesCambiadas.get(i)) {
          this.listIndexSendNewFunciones.add(indexFuncionesOriginales.get(i));
          this.listSendNewFunciones.add(this.funcionesCambiadas.get(i));
        }
      }

      //CICLO PARA INSERTAR/BORRAR
      for (int i = 0; i <= this.listIndexSendNewFunciones.size() - 1; i++) {
        //INSERTAR FUNCIONES NUEVAS
        if (this.listSendNewFunciones.get(i).toString().equals("true")) {
          try {
            conn = new connection();
            conn.assignFuncionesToOficinas(
              1,
              this.idFuncion,
              Integer.parseInt(this.listIndexSendNewFunciones.get(i).toString())
            );
            conn.closeConnection();
          } catch (Exception throwables) {
            if (conn != null) {
              try {
                conn.getConn().rollback();
              } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                JOptionPane.showMessageDialog(null,sqlException.toString());
              }
            }
            JOptionPane.showMessageDialog(null, throwables.toString());
          }
        }
        
        //BORRAR FUNCIONES
        else {
          try {
            conn = new connection();
            conn.assignFuncionesToOficinas(
              7,
              this.idFuncion,
              Integer.parseInt(this.listIndexSendNewFunciones.get(i).toString())
            );
            conn.closeConnection();
          } catch (Exception throwables) {
            if (conn != null) {
              try {
                conn.getConn().rollback();
              } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                JOptionPane.showMessageDialog(null,sqlException.toString());
              }
            }
            JOptionPane.showMessageDialog(null, throwables.toString());
          }
        }
      }
      this.edFn.framePrimario.dispose();
    }
  }

  protected boolean verifyFields() {
    int tempCont = 0;
    for (int i = 0; i <= this.edFn.tableFuncionees.getRowCount() - 1; i++) {
      if (
        this.edFn.tableFuncionees.getValueAt(i, 1).toString().equals("true")
      ) {
        tempCont += 1;
      }
    }
    if (this.edFn.txtFuncion.getText().equals("") || (tempCont == 0)) {
      return false;
    }
    return true;
  }

  /*======================
  LLENAR TABLA
  ======================*/
  protected void fillTable() {
    Object[] rowFunciones = new Object[3];
    Object[] rowFuncionesShow= new Object[2];

    try {
      conn = new connection();
      ResultSet res = conn.getOficinasInner();
      while (res.next()) {
        rowFunciones[0] = res.getString(1);
        rowFunciones[1] = res.getString(2);
        rowFunciones[2] = false;
        rowFuncionesShow[0]= res.getString(2)+ "  ("+ res.getString(4) + ")";
        rowFuncionesShow[1]= false;
        
        tableFunciones.addRow(rowFunciones);
        tableFuncionesShow.addRow(rowFuncionesShow);
      }
      res.close();
      conn.closeConnection();
    } catch (SQLException | ClassNotFoundException ex) {
      JOptionPane.showMessageDialog(null, ex.toString());
    }

    tableFunciones.removeRow(0);
    tableFuncionesShow.removeRow(0);
    this.edFn.tableFuncionees.setModel(tableFuncionesShow);
  }
  
  /*=========================================
  CHECKEAR LAS OFICINAS APROBADAS ACTUALMENTE
  =========================================*/
  protected void setOficinasTrueOrFalse() {
    this.funcionesCambiadas = new Vector<>();
    this.funcionesOriginales = new Vector<>();
    this.indexFuncionesOriginales = new Vector<>();

    //OBTENER FUNCIONES APROBADAS Y ALMACENARLAS EN LA LINKED LIST
    try {
      conn = new connection();
      ResultSet res = conn.getFuncionesOficinas(this.idFuncion, 0);
      while (res.next()) {
        this.listFuncionesAprobadas.add(res.getInt(1));
      }
      res.close();
      conn.closeConnection();
    } catch (SQLException | ClassNotFoundException ex) {
      JOptionPane.showMessageDialog(null, ex.toString());
    }

    //CICLO PARA PALOMEAR LAS OFICINAS ASIGNADAS A LA TABLA
    for (int i = 0; i <= this.tableFunciones.getRowCount() - 1; i++) {
      for (int j = 0; j <= this.listFuncionesAprobadas.size() - 1; j++) {
        if (
          Integer.parseInt(this.tableFunciones.getValueAt(i, 0).toString()) ==
          this.listFuncionesAprobadas.get(j)
        ) {
          tableFunciones.setValueAt(true, i, 2);
          tableFuncionesShow.setValueAt(true, i, 1);
        }
      }
    }

    this.edFn.tableFuncionees.setModel(this.tableFuncionesShow);

    //CICLO PARA AGREGAR LAS ACTUALES ASIGNADAS DE LA FUNCION A LA LISTA
    for (int i = 0; i <= this.tableFunciones.getRowCount() - 1; i++) {
      indexFuncionesOriginales.add(this.tableFunciones.getValueAt(i, 0));
      this.funcionesOriginales.add(this.tableFunciones.getValueAt(i, 2));
    }
  }


}
