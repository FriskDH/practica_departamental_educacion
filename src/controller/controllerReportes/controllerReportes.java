package controller.controllerReportes;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import javax.swing.*;


import model.connection.connection;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;
import view.menu.Reportes;

public class controllerReportes implements ActionListener {
  connection conn;
  Reportes reportes = new Reportes();
  Map<String, Object> hashDataSource = new HashMap<String, Object>();
  JasperPrint jasperPrint;
  JasperDesign jasperDesign;
  JasperReport jasperReport;
  JasperViewer jasperViewer;
  List<String> dpi = new LinkedList<>();
  List<String> nombre1 = new LinkedList<>();
  List<String> nombre2 = new LinkedList<>();
  List<String> apellido1 = new LinkedList<>();
  List<String> apellido2 = new LinkedList<>();
  List<String> precedencia = new LinkedList<>();
  List<String> hora = new LinkedList<>();
  List<String> oficina= new LinkedList<>();
  LinkedList<Integer> listDptos= new LinkedList<>();
  LinkedList<Integer> listOficinas= new LinkedList<>();
  LinkedList<String> listTotalVisitas= new LinkedList<>();

  public controllerReportes(Reportes reportes)
    throws SQLException, ClassNotFoundException {
    this.reportes = reportes;
    this.reportes.btnGenerarReporte.addActionListener(this);
    this.reportes.btnReset.addActionListener(this);
    this.reportes.btnDepartamentos.addActionListener(this);
    this.reportes.btnOficina.addActionListener(this);
    this.reportes.btnFecha.addActionListener(this);
    this.reportes.btnGenerarReporte.setEnabled(false);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    /*=================
    RESETEAR
    =================*/
    if (this.reportes.btnReset == e.getSource()) {
      reset();
    }

    /*=================
    FECHAS
    =================*/
    if (this.reportes.btnFecha == e.getSource()) {

      this.reportes.btnFecha.setEnabled(false);
      this.reportes.btnDepartamentos.setEnabled(false);
      this.reportes.btnOficina.setEnabled(false);
      this.reportes.btnGenerarReporte.setEnabled(true);
      this.reportes.cmbOficina.setVisible(false);
      this.reportes.cmbOficina.removeAllItems();
      this.reportes.rbtnOficinas1Date.setVisible(false);
      this.reportes.rbtnOficinas2Dates.setVisible(false);
      this.reportes.rbtn1Date.setVisible(true);
      this.reportes.rbtn2Dates.setVisible(true);
    }

    /*=================
    OFICINA
    =================*/
    if (this.reportes.btnOficina == e.getSource()) {
      this.reportes.btnFecha.setEnabled(false);
      this.reportes.btnDepartamentos.setEnabled(false);
      this.reportes.btnGenerarReporte.setEnabled(true);
      this.reportes.cmbOficina.setVisible(true);
      this.reportes.cmbOficina.removeAllItems();
      this.reportes.rbtnOficinas1Date.setVisible(true);
      this.reportes.rbtnOficinas2Dates.setVisible(true);
      this.reportes.btnOficina.setEnabled(false);
      try {
        conn = new connection();
        ResultSet res = conn.getOficinasInner();

        while (res.next()) {
          this.listOficinas.add(res.getInt(1));
          this.reportes.cmbOficina.addItem(
              res.getString(2) + " (" + res.getString(4) +")"
            );
        }
        res.close();
        conn.closeConnection();
      } catch (SQLException | ClassNotFoundException throwables) {
        JOptionPane.showMessageDialog(null, throwables.toString());
      }
    }

    /*=================
    DEPARTAMENTOS
    =================*/
    if (this.reportes.btnDepartamentos == e.getSource()) {
      this.reportes.btnFecha.setEnabled(false);
      this.reportes.btnOficina.setEnabled(false);
      this.reportes.btnDepartamentos.setEnabled(false);
      this.reportes.btnGenerarReporte.setEnabled(true);
      this.reportes.cmbDepartamento.setVisible(true);
      this.reportes.cmbDepartamento.removeAllItems();
      this.reportes.rbtnDptos1Date.setVisible(true);
      this.reportes.rbtnDptos2Dates.setVisible(true);
      try {
        conn = new connection();
        ResultSet res = conn.getDptos();
        while (res.next()) {
          listDptos.add(res.getInt(1));
          this.reportes.cmbDepartamento.addItem(
              res.getString(3)
            );
        }
        res.close();
        conn.closeConnection();
      } catch (SQLException | ClassNotFoundException throwables) {
        JOptionPane.showMessageDialog(null, throwables.toString());
      }
    }

    /*=================
    GENERAR REPORTES
    =================*/
    if (this.reportes.btnGenerarReporte == e.getSource()) {
      if (this.reportes.rbtn1Date.isSelected()) {
        String ddate = JOptionPane.showInputDialog(
          null,
          "Ingrese fecha (2020-12-31): "
        );
        generateDateReport(ddate);
      }

      if (this.reportes.rbtn2Dates.isSelected()) {
        String ddate1 = JOptionPane.showInputDialog(
          null,
          "Ingrese fecha inicial (2020-12-31): "
        );
        String ddate2 = JOptionPane.showInputDialog(
          null,
          "Ingrese fecha final (2020-12-31): "
        );
        try {
          generateReportBetweenDates(ddate1, ddate2);
        } catch (
          SQLException | ClassNotFoundException | JRException throwables
        ) {
          JOptionPane.showMessageDialog(
            null,
            "Error: " + throwables.toString()
          );
        }
      }

      if (this.reportes.rbtnOficinas1Date.isSelected()) {
        String ddate = JOptionPane.showInputDialog(
          null,
          "Ingrese fecha (2020-12-31): "
        );
        generateReportOffice(this.listOficinas.get(this.reportes.cmbOficina.getSelectedIndex()), (String) this.reportes.cmbOficina.getSelectedItem(), ddate);
      }

      if (this.reportes.rbtnOficinas2Dates.isSelected()) {
        String ddate1 = JOptionPane.showInputDialog(
          null,
          "Ingrese fecha inicial (2020-12-31): "
        );
        String ddate2 = JOptionPane.showInputDialog(
          null,
          "Ingrese fecha final (2020-12-31): "
        );
        generateReportBetweenDatesOffice(
          this.listOficinas.get(this.reportes.cmbOficina.getSelectedIndex()),
          (String) this.reportes.cmbOficina.getSelectedItem(),
          ddate1,
          ddate2
        );
      }

      if (this.reportes.rbtnDptos1Date.isSelected()) {
        String ddate = JOptionPane.showInputDialog(
          null,
          "Ingrese fecha (2020-12-31): "
        );
        generateReportDptos(this.listDptos.get(this.reportes.cmbDepartamento.getSelectedIndex()), (String) this.reportes.cmbDepartamento.getSelectedItem(), ddate);
      }

      if (this.reportes.rbtnDptos2Dates.isSelected()) {
        String ddate1 = JOptionPane.showInputDialog(
          null,
          "Ingrese fecha inicial (2020-12-31): "
        );
        String ddate2 = JOptionPane.showInputDialog(
          null,
          "Ingrese fecha final (2020-12-31): "
        );
        generateReportBetweenDatesDptos(
          this.listDptos.get(this.reportes.cmbDepartamento.getSelectedIndex()),
          (String) this.reportes.cmbDepartamento.getSelectedItem(),
          ddate1,
          ddate2
        );
      }
      reset();
    }
  }

  protected void generateDateReport(String ddate) {
    try {
      conn = new connection();
      int cont= 0;
      ResultSet res = conn.reportsByDate(ddate);
      while (res.next()) {
        dpi.add(res.getString(1));
        nombre1.add(res.getString(2));
        nombre2.add(res.getString(3));
        apellido1.add(res.getString(4));
        apellido2.add(res.getString(5));
        precedencia.add(res.getString(7) + ", " + res.getString(6));
        hora.add(res.getString(8));
        oficina.add(res.getString(10));
        if(res.getString(1) != null)cont++;
      }
      res.close();
      conn.closeConnection();
      listTotalVisitas.add(Integer.toString(cont));
      List<String> dateL= new LinkedList<>();
      dateL.add(ddate);
      hashDataSource.put("dpi", dpi);
      hashDataSource.put("fecha", dateL);
      hashDataSource.put("nombre1", nombre1);
      hashDataSource.put("nombre2", nombre2);
      hashDataSource.put("apellido1", apellido1);
      hashDataSource.put("apellido2", apellido2);
      hashDataSource.put("precedencia", precedencia);
      hashDataSource.put("hora", hora);
      hashDataSource.put("oficina", oficina);
      hashDataSource.put("total_visitas", listTotalVisitas);


      jasperPrint =
        JasperFillManager.fillReport(
          ClassLoader.getSystemResourceAsStream("dialy-report.jasper"),
          hashDataSource,
          new JREmptyDataSource()
        );
      JRPdfExporter exp = new JRPdfExporter();
      exp.setExporterInput(new SimpleExporterInput(jasperPrint));
      exp.setExporterOutput(
        new SimpleOutputStreamExporterOutput("reporte_" + ddate + ".pdf")
      );
      SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
      exp.setConfiguration(conf);
      exp.exportReport();
      showReport("reporte_" + ddate + ".pdf");
    } catch (SQLException | JRException | ClassNotFoundException ex) {
      JOptionPane.showMessageDialog(null, "Error: " + ex.toString());
    }
    dpi.clear();
    nombre1.clear();
    nombre2.clear();
    apellido1.clear();
    apellido2.clear();
    precedencia.clear();
    hora.clear();
    oficina.clear();
    listTotalVisitas.clear();
  }

  protected void generateReportBetweenDates(String date1, String date2)
    throws SQLException, ClassNotFoundException, JRException {
    try {
      conn = new connection();
      int cont=0;
      ResultSet res = conn.reportsBetweenDates(date1, date2);
      while (res.next()) {
        dpi.add(res.getString(1));
        nombre1.add(res.getString(2));
        nombre2.add(res.getString(3));
        apellido1.add(res.getString(4));
        apellido2.add(res.getString(5));
        precedencia.add(res.getString(7) + ", " + res.getString(6));
        hora.add(res.getString(9) + " " + res.getString(8));
        oficina.add(res.getString(10));
        if(res.getString(1) != null)cont++;
      }
      res.close();
      conn.closeConnection();
      listTotalVisitas.add(Integer.toString(cont));
      List<String> dptoL = new LinkedList<>();
      List<String> dateL = new LinkedList<>();
      dateL.add("Del " + date1 + " al " + date2);
      hashDataSource.put("departamento", dptoL);
      hashDataSource.put("fecha", dateL);
      hashDataSource.put("dpi", dpi);
      hashDataSource.put("nombre1", nombre1);
      hashDataSource.put("nombre2", nombre2);
      hashDataSource.put("apellido1", apellido1);
      hashDataSource.put("apellido2", apellido2);
      hashDataSource.put("precedencia", precedencia);
      hashDataSource.put("hora", hora);
      hashDataSource.put("oficina", oficina);
      hashDataSource.put("total_visitas", listTotalVisitas);
      jasperPrint =
        JasperFillManager.fillReport(
          ClassLoader.getSystemResourceAsStream("report-filtered-dates.jasper"),
          hashDataSource,
          new JREmptyDataSource()
        );
      JRPdfExporter exp = new JRPdfExporter();
      exp.setExporterInput(new SimpleExporterInput(jasperPrint));
      exp.setExporterOutput(
        new SimpleOutputStreamExporterOutput(
          "reporte_" + date1 + "_al_" + date2 + ".pdf"
        )
      );
      SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
      exp.setConfiguration(conf);
      exp.exportReport();
      showReport("reporte_" + date1 + "_al_" + date2 + ".pdf");
    } catch (SQLException | JRException | ClassNotFoundException ex) {
      JOptionPane.showMessageDialog(null, "Error: " + ex.toString());
    }
    dpi.clear();
    nombre1.clear();
    nombre2.clear();
    apellido1.clear();
    apellido2.clear();
    precedencia.clear();
    hora.clear();
    oficina.clear();
    listTotalVisitas.clear();
  }

  protected void generateReportOffice(
    int oficina,
    String nombreOficina,
    String date
  ) {
    try {
      conn = new connection();
      int cont=0;
      ResultSet res = conn.reportsByOffice(oficina, date);
      while (res.next()) {
        dpi.add(res.getString(1));
        nombre1.add(res.getString(2));
        nombre2.add(res.getString(3));
        apellido1.add(res.getString(4));
        apellido2.add(res.getString(5));
        precedencia.add(res.getString(7) + ", " + res.getString(6));
        hora.add(res.getString(8));
        this.oficina.add(res.getString(9));
        if(res.getString(1) != null)cont++;
      }
      res.close();
      conn.closeConnection();
      listTotalVisitas.add(Integer.toString(cont));
      List<String> ofL = new LinkedList<>();
      List<String> dateL = new LinkedList<>();
      ofL.add(nombreOficina);
      dateL.add(date);
      hashDataSource.put("oficina", ofL);
      hashDataSource.put("fecha", dateL);
      hashDataSource.put("dpi", dpi);
      hashDataSource.put("nombre1", nombre1);
      hashDataSource.put("nombre2", nombre2);
      hashDataSource.put("apellido1", apellido1);
      hashDataSource.put("apellido2", apellido2);
      hashDataSource.put("precedencia", precedencia);
      hashDataSource.put("hora", hora);
      hashDataSource.put("oficinas", this.oficina);
      hashDataSource.put("total_visitas", listTotalVisitas);
      jasperPrint =
        JasperFillManager.fillReport(
          ClassLoader.getSystemResourceAsStream("report-filtered-oficinas.jasper"),
          hashDataSource,
          new JREmptyDataSource()
        );
      JRPdfExporter exp = new JRPdfExporter();
      exp.setExporterInput(new SimpleExporterInput(jasperPrint));
      exp.setExporterOutput(
        new SimpleOutputStreamExporterOutput(
          "reporte_" + date + "_filtro_oficina" + ".pdf"
        )
      );
      SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
      exp.setConfiguration(conf);
      exp.exportReport();
      showReport("reporte_" + date + "_filtro_oficina" + ".pdf");
    } catch (SQLException | JRException | ClassNotFoundException ex) {
      JOptionPane.showMessageDialog(null, "Error: " + ex.toString());
    }

    dpi.clear();
    nombre1.clear();
    nombre2.clear();
    apellido1.clear();
    apellido2.clear();
    precedencia.clear();
    hora.clear();
    this.oficina.clear();
    listTotalVisitas.clear();
  }

  protected void generateReportBetweenDatesOffice(
    int oficina,
    String nombreOficina,
    String date1,
    String date2
  ) {
    List<String> fecha = new LinkedList<>();
    try {
      conn = new connection();
      int cont=0;
      ResultSet res = conn.reportsBetweenDatesOffice(oficina, date1, date2);
      while (res.next()) {
        dpi.add(res.getString(1));
        nombre1.add(res.getString(2));
        nombre2.add(res.getString(3));
        apellido1.add(res.getString(4));
        apellido2.add(res.getString(5));
        precedencia.add(res.getString(7) + ", " + res.getString(6));
        hora.add(res.getString(8));
        fecha.add(res.getString(9));
        this.oficina.add(res.getString(10));
        if(res.getString(1) != null)cont++;
      }
      res.close();
      conn.closeConnection();
      listTotalVisitas.add(Integer.toString(cont));
      List<String> ofL = new LinkedList<>();
      List<String> dateL = new LinkedList<>();
      ofL.add(nombreOficina);
      dateL.add("Del " + date1 + " al " + date2);
      hashDataSource.put("oficina", ofL);
      hashDataSource.put("fecha", dateL);
      hashDataSource.put("dpi", dpi);
      hashDataSource.put("nombre1", nombre1);
      hashDataSource.put("nombre2", nombre2);
      hashDataSource.put("apellido1", apellido1);
      hashDataSource.put("apellido2", apellido2);
      hashDataSource.put("precedencia", precedencia);
      hashDataSource.put("hora", hora);
      hashDataSource.put("oficinas", fecha);
      hashDataSource.put("total_visitas", listTotalVisitas);
      jasperPrint =
        JasperFillManager.fillReport(
          ClassLoader.getSystemResourceAsStream("report-filtered-oficinas.jasper"),
          hashDataSource,
          new JREmptyDataSource()
        );
      JRPdfExporter exp = new JRPdfExporter();
      exp.setExporterInput(new SimpleExporterInput(jasperPrint));
      exp.setExporterOutput(
        new SimpleOutputStreamExporterOutput(
          "reporte_" + date1 + "_al_" + date2 + "_filtro_oficina.pdf"
        )
      );
      SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
      exp.setConfiguration(conf);
      exp.exportReport();
      showReport("reporte_" + date1 + "_al_" + date2 + "_filtro_oficina.pdf");
    } catch (SQLException | JRException | ClassNotFoundException ex) {
      JOptionPane.showMessageDialog(null, "Error: " + ex.toString());
    }

    dpi.clear();
    nombre1.clear();
    nombre2.clear();
    apellido1.clear();
    apellido2.clear();
    precedencia.clear();
    hora.clear();
    this.oficina.clear();
    listTotalVisitas.clear();
  }

  protected void generateReportDptos(int dpto, String nombreDpto, String date) {
    try {
      conn = new connection();
      int cont=0;
      ResultSet res = conn.reportsByDpto(dpto, date);
      while (res.next()) {
        dpi.add(res.getString(1));
        nombre1.add(res.getString(2));
        nombre2.add(res.getString(3));
        apellido1.add(res.getString(4));
        apellido2.add(res.getString(5));
        precedencia.add(res.getString(7) + ", " + res.getString(6));
        hora.add(res.getString(9) +", " + res.getString(8));
        oficina.add(res.getString(10));
        if(res.getString(1) != null)cont++;
      }
      res.close();
      conn.closeConnection();
      listTotalVisitas.add(Integer.toString(cont));
      List<String> dptoL = new LinkedList<>();
      List<String> dateL = new LinkedList<>();
      dptoL.add(nombreDpto);
      dateL.add(date);
      hashDataSource.put("departamento", dptoL);
      //hashDataSource.put("fecha", dateL);
      hashDataSource.put("dpi", dpi);
      hashDataSource.put("nombre1", nombre1);
      hashDataSource.put("nombre2", nombre2);
      hashDataSource.put("apellido1", apellido1);
      hashDataSource.put("apellido2", apellido2);
      hashDataSource.put("precedencia", precedencia);
      hashDataSource.put("hora", hora);
      hashDataSource.put("oficina", oficina);
      hashDataSource.put("total_visitas", listTotalVisitas);
      jasperPrint =
        JasperFillManager.fillReport(
          ClassLoader.getSystemResourceAsStream("report-filtered-departamentos.jasper"),
          hashDataSource,
          new JREmptyDataSource()
        );
      JRPdfExporter exp = new JRPdfExporter();
      exp.setExporterInput(new SimpleExporterInput(jasperPrint));
      exp.setExporterOutput(
        new SimpleOutputStreamExporterOutput(
          "reporte_" + date + "_filtro_dpto" + ".pdf"
        )
      );
      SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
      exp.setConfiguration(conf);
      exp.exportReport();
      showReport("reporte_" + date + "_filtro_dpto" + ".pdf");
    } catch (SQLException | JRException | ClassNotFoundException ex) {
      JOptionPane.showMessageDialog(null, "Error: " + ex.toString());
    }

    dpi.clear();
    nombre1.clear();
    nombre2.clear();
    apellido1.clear();
    apellido2.clear();
    precedencia.clear();
    hora.clear();
    oficina.clear();
    listTotalVisitas.clear();
  }

  public void generateReportBetweenDatesDptos(
    int dpto,
    String nombreDpto,
    String date1,
    String date2
  ) {
    try {
      conn = new connection();
      int cont=0;
      ResultSet res = conn.reportsBetweenDatesDptos(dpto, date1, date2);
      while (res.next()) {
        dpi.add(res.getString(1));
        nombre1.add(res.getString(2));
        nombre2.add(res.getString(3));
        apellido1.add(res.getString(4));
        apellido2.add(res.getString(5));
        precedencia.add(res.getString(7) + ", " + res.getString(6));
        hora.add(res.getString(9) + " " + res.getString(8));
        oficina.add(res.getString(10));
        if(res.getString(1) != null)cont++;
      }
      res.close();
      conn.closeConnection();
      listTotalVisitas.add(Integer.toString(cont));
      List<String> dptoL = new LinkedList<>();
      List<String> dateL = new LinkedList<>();
      dptoL.add(nombreDpto);
      dateL.add("Del " + date1 + " al " + date2);
      hashDataSource.put("departamento", dptoL);
      hashDataSource.put("fecha", dateL);
      hashDataSource.put("dpi", dpi);
      hashDataSource.put("nombre1", nombre1);
      hashDataSource.put("nombre2", nombre2);
      hashDataSource.put("apellido1", apellido1);
      hashDataSource.put("apellido2", apellido2);
      hashDataSource.put("precedencia", precedencia);
      hashDataSource.put("hora", hora);
      hashDataSource.put("oficina", oficina);
      hashDataSource.put("total_visitas", listTotalVisitas);
      jasperPrint =
        JasperFillManager.fillReport(
          ClassLoader.getSystemResourceAsStream("report-filtered-departamentos.jasper"),
          hashDataSource,
          new JREmptyDataSource()
        );
      JRPdfExporter exp = new JRPdfExporter();
      exp.setExporterInput(new SimpleExporterInput(jasperPrint));
      exp.setExporterOutput(
        new SimpleOutputStreamExporterOutput(
          "reporte_" + date1 + "_al_" + date2 + "_filtro_dpto.pdf"
        )
      );
      SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
      exp.setConfiguration(conf);
      exp.exportReport();
      showReport("reporte_" + date1 + "_al_" + date2 + "_filtro_dpto.pdf");
    } catch (SQLException | JRException | ClassNotFoundException ex) {
      JOptionPane.showMessageDialog(null, "Error: " + ex.toString());
    }
    dpi.clear();
    nombre1.clear();
    nombre2.clear();
    apellido1.clear();
    apellido2.clear();
    precedencia.clear();
    hora.clear();
    oficina.clear();
    listTotalVisitas.clear();
  }

  protected void showReport(String reportPath){
    try {
      File report = new File(reportPath);
      Desktop.getDesktop().open(report);
    }catch(IOException ex){
      JOptionPane.showMessageDialog(null, ex.toString());
    }
  }

  protected void reset(){
    this.reportes.btnFecha.setEnabled(true);
    this.reportes.btnDepartamentos.setEnabled(true);
    this.reportes.btnOficina.setEnabled(true);
    this.reportes.cmbDepartamento.setVisible(false);
    this.reportes.cmbOficina.setVisible(false);
    this.reportes.btnGenerarReporte.setEnabled(false);
    this.reportes.rbtnDptos1Date.setSelected(false);
    this.reportes.rbtnDptos2Dates.setSelected(false);
    this.reportes.rbtnDptos1Date.setVisible(false);
    this.reportes.rbtnDptos2Dates.setVisible(false);
    this.reportes.rbtnOficinas2Dates.setSelected(false);
    this.reportes.rbtnOficinas1Date.setSelected(false);
    this.reportes.rbtnOficinas1Date.setVisible(false);
    this.reportes.rbtnOficinas2Dates.setVisible(false);
    this.reportes.rbtn1Date.setVisible(false);
    this.reportes.rbtn2Dates.setVisible(false);
    this.reportes.rbtn1Date.setSelected(false);
    this.reportes.rbtn2Dates.setSelected(false);
    this.listOficinas.clear();
    this.listDptos.clear();
  }
}
