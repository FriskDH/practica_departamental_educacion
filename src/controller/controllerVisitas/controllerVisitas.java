package controller.controllerVisitas;

import controller.scanner.fingerPrint;
import controller.scanner.verifyFingerPrint;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.Timer;
import javax.swing.*;

import model.connection.connection;
import model.personData.Data;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fonts.FontFamily;
import net.sf.jasperreports.engine.fonts.SimpleFontFace;
import net.sf.jasperreports.engine.fonts.SimpleFontFamily;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;
import view.menu.*;

public class controllerVisitas implements ActionListener {
    static LinkedList<Integer> listOficinas, listFunciones, listOficinasTemporalVisita, listFuncionesTemporalVisita;
    LinkedList<String> listOficinasNombreTemporalVisita;
    Visitas menuPrimario;
    connection conn = new connection();

    public controllerVisitas(Visitas menuPrimario)
            throws SQLException, ClassNotFoundException {
        this.menuPrimario = menuPrimario;
        this.menuPrimario.btnGuardarVisita.addActionListener(this);
        this.menuPrimario.btnRegistrarHuella.addActionListener(this);
        this.menuPrimario.btnVerificarHuella.addActionListener(this);
        this.menuPrimario.btnDesbloquear.addActionListener(this);
        this.menuPrimario.btnLimpiar.addActionListener(this);
        this.menuPrimario.btnImprimir.addActionListener(this);
        this.menuPrimario.btnBuscarDPI.addActionListener(this);
        this.menuPrimario.btnAgregarFuncion.addActionListener(this);
        this.listFunciones = new LinkedList<>();
        this.listOficinas = new LinkedList<>();
        /*Las 2 listas temporales de abajo son para almacenar todas las oficinas y funciones
         que van a ir a detalle_visita en la BD*/
        this.listFuncionesTemporalVisita = new LinkedList<>();
        this.listOficinasTemporalVisita = new LinkedList<>();
        this.listOficinasNombreTemporalVisita = new LinkedList<>();
        setFalseTxts();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        fingerPrint fPrint = null;
        verifyFingerPrint fPrintV = null;
        try {
            fPrintV = new verifyFingerPrint();
            fPrint = new fingerPrint();
        } catch (SQLException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Error " + ex.getMessage());
        }

    /*===============================================
    AGREGAR FUNCIONES Y OFICINAS PARA DETALLE VISITA
    ================================================*/
        if (this.menuPrimario.btnAgregarFuncion == e.getSource()) {
            this.listFuncionesTemporalVisita.add(this.listFunciones.get(this.menuPrimario.cmbFuncion.getSelectedIndex()));
            //this.listOficinasTemporalVisita.add(this.listOficinas.get(this.menuPrimario.cmbOficina.getSelectedIndex
            // ()));
            this.listOficinasTemporalVisita.add(this.listOficinas.get(this.listOficinas.size() - 1));
            this.listOficinasNombreTemporalVisita.add('\n'+this.menuPrimario.cmbOficina.getSelectedItem().toString()+
                    "_________________________"+'\n'+'\n');
            this.menuPrimario.txtSearchFuncion.setText("");
            //this.menuPrimario.txtFuncion.setText(this.menuPrimario.txtFuncion.getText()+'\n'+this.menuPrimario
            // .cmbFuncion.getSelectedItem());
            String temporalFuncionesString=
                    this.menuPrimario.lblListaFunciones.getText().replace("<html>", "").replace("</html>", "");
            this.menuPrimario.lblListaFunciones.setText("<html>"+temporalFuncionesString+
                    "<br>"+"*"+this.menuPrimario.cmbFuncion.getSelectedItem()+"</html>");

            //System.out.println(this.listOficinasTemporalVisita);
        }

    /*===============================
    IMPRIMIR REPORTES DEL DIA
    ===============================*/
        if (menuPrimario.btnImprimir == e.getSource()) {
            String ddate = JOptionPane.showInputDialog(
                    null,
                    "Ingrese fecha (2020-12-31): "
            );
            Map<String, Object> hashDataSource = new HashMap<String, Object>();
            JasperPrint jasperPrint;
            JasperDesign jasperDesign;
            JasperReport jasperReport;
            JasperViewer jasperViewer;
            List<String> dpi = new LinkedList<>();
            List<String> nombre1 = new LinkedList<>();
            List<String> nombre2 = new LinkedList<>();
            List<String> apellido1 = new LinkedList<>();
            List<String> apellido2 = new LinkedList<>();
            List<String> precedencia = new LinkedList<>();
            List<String> hora = new LinkedList<>();
            List<String> fecha = new LinkedList<>();
            List<String> oficina= new LinkedList<>();
            try {
                ResultSet res = conn.reportsByDate(ddate);
                fecha.add(ddate);

                while (res.next()) {
                    dpi.add(res.getString(1));
                    nombre1.add(res.getString(2));
                    nombre2.add(res.getString(3));
                    apellido1.add(res.getString(4));
                    apellido2.add(res.getString(5));
                    precedencia.add(res.getString(7) + ", " + res.getString(6));
                    hora.add(res.getString(8));
                    oficina.add(res.getString(9));
                }

                hashDataSource.put("dpi", dpi);
                hashDataSource.put("nombre1", nombre1);
                hashDataSource.put("nombre2", nombre2);
                hashDataSource.put("apellido1", apellido1);
                hashDataSource.put("apellido2", apellido2);
                hashDataSource.put("precedencia", precedencia);
                hashDataSource.put("hora", hora);
                hashDataSource.put("fecha", fecha);
                hashDataSource.put("oficina", oficina);

                jasperPrint =
                        JasperFillManager.fillReport(
                                ClassLoader.getSystemResourceAsStream("dialy-report.jasper"),
                                hashDataSource,
                                new JREmptyDataSource()
                        );
                JRPdfExporter exp = new JRPdfExporter();
                exp.setExporterInput(new SimpleExporterInput(jasperPrint));
                exp.setExporterOutput(
                        new SimpleOutputStreamExporterOutput("reporte_" + ddate + ".pdf")
                );
                SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
                exp.setConfiguration(conf);
                exp.exportReport();
                showReport("reporte_" + ddate + ".pdf");
            } catch (SQLException | JRException ex) {
                JOptionPane.showMessageDialog(null, ex.toString());
            }
        }

    /*====================================
    LIMIPIAR LAS CASILLAS Y COMBOBOX
    =====================================*/
        if (menuPrimario.btnLimpiar == e.getSource()) {
            setFalseTxts();
            try {
                verifyFingerPrint vFP= new verifyFingerPrint();
                vFP.stopPrinting();
            } catch (SQLException | ClassNotFoundException throwables) {
//                throwables.printStackTrace();
            }
            if (menuPrimario.cmbOficina.isEnabled()) {
                menuPrimario.cmbOficina.setEnabled(false);
                menuPrimario.cmbOficina.removeAllItems();
                menuPrimario.cmbFuncion.setEnabled(false);
                menuPrimario.cmbFuncion.removeAllItems();
            }
        }

    /*======================
    BUSCAR POR DPI
    =======================*/
        if (menuPrimario.btnBuscarDPI == e.getSource()) {
            String strDPI = JOptionPane.showInputDialog(null, "Ingrese DPI");
            findByDPI(strDPI);
        }

    /*========================
    VERIFICAR POR HUELLA
    ========================*/
        if (menuPrimario.btnVerificarHuella == e.getSource()) {
            menuPrimario.btnVerificarHuella.setEnabled(false);
            try {
                fPrintV.startPrinting();
                fPrintV.startVerification();
                menuPrimario.btnGuardarVisita.setEnabled(true);
                menuPrimario.btnAgregarFuncion.setEnabled(true);
            } catch (Exception ee) {
                JOptionPane.showMessageDialog(null, ee.getMessage());
                fPrintV.stopPrinting();
            }
        }

    /*========================================
    DESBLOQUEAR LOS CAMPOS PARA LLENAR DATOS
    ========================================*/
        if (menuPrimario.btnDesbloquear == e.getSource()) {
            this.menuPrimario.txtDPI.setEnabled(true);
            this.menuPrimario.txtNombre1.setEnabled(true);
            this.menuPrimario.txtNombre2.setEnabled(true);
            this.menuPrimario.txtApellido1.setEnabled(true);
            this.menuPrimario.txtApellido2.setEnabled(true);
            this.menuPrimario.cmbPrecedencia.setEnabled(true);
            this.menuPrimario.cmbHuella.setVisible(true);
            this.menuPrimario.cmbHuella.setEnabled(true);
            this.menuPrimario.lblHuella.setVisible(true);
            this.menuPrimario.btnRegistrarHuella.setEnabled(true);
            this.menuPrimario.btnVerificarHuella.setEnabled(false);
            this.menuPrimario.txtTelefono.setEnabled(true);

            try {
                ResultSet res = conn.getDptos();
                while (res.next()) {
                    this.menuPrimario.cmbPrecedencia.addItem(
                            res.getString(2) + " | " + res.getString(3)
                    );
                }
                menuPrimario.cmbPrecedencica2.setEnabled(true);
                res.close();

                this.menuPrimario.cmbPrecedencia.addActionListener(
                        new ActionListener() {

                            public void actionPerformed(ActionEvent e) {
                                try {
                                    menuPrimario.cmbPrecedencica2.removeAllItems();
                                    ResultSet res2 = conn.getMun(
                                            menuPrimario.cmbPrecedencia.getSelectedIndex() + 1
                                    );
                                    while (res2.next()) {
                                        menuPrimario.cmbPrecedencica2.addItem(
                                                res2.getString(1) + " | " + res2.getString(3)
                                        );
                                    }
                                    res2.close();
                                } catch (SQLException ex) {
                                    JOptionPane.showMessageDialog(null, ex.getMessage());
                                    try {
                                        conn.closeConnection();
                                    } catch (SQLException exc) {
                                        JOptionPane.showMessageDialog(null, exc.getMessage());
                                    }
                                }
                            }
                        }
                );
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
                try {
                    conn.closeConnection();
                } catch (SQLException exc) {
                    JOptionPane.showMessageDialog(null, exc.getMessage());
                }
            }
        }

    /*==================================
    REGISTRAR HUELLA, "fingerPrint"
    ===================================*/
        if (menuPrimario.btnRegistrarHuella == e.getSource()) {
            menuPrimario.btnVerificarHuella.setEnabled(false);
            if (
                    !this.menuPrimario.txtDPI.getText().equals("") &&
                            !this.menuPrimario.txtNombre1.getText().equals("") &&
                            !this.menuPrimario.txtNombre2.getText().equals("") &&
                            !this.menuPrimario.txtApellido1.getText().equals("") &&
                            !this.menuPrimario.txtApellido2.getText().equals("")
            ) {
                String[] tempMun = Objects
                        .requireNonNull(this.menuPrimario.cmbPrecedencica2.getSelectedItem())
                        .toString()
                        .split("|");

                String tempMenu2 = "";
                for (int j = 0; j < tempMun.length; j++) {
                    try {
                        Integer.parseInt(tempMun[j]);
                        tempMenu2 = tempMenu2 + Integer.parseInt(tempMun[j]);
                    } catch (Exception e22) {

                    }
                }

                int idMun = Integer.parseInt(tempMenu2);

                String dpi = new String(this.menuPrimario.txtDPI.getText());
                String nombre1 = this.menuPrimario.txtNombre1.getText();
                String nombre2 = this.menuPrimario.txtNombre2.getText();
                String apellido1 = this.menuPrimario.txtApellido1.getText();
                String apellido2 = this.menuPrimario.txtApellido2.getText();
                String tel= "";
                if(this.menuPrimario.txtTelefono.getText().length()!=8)
                {
                    try {
                        throw new Exception("Número de teléfono inválido");
                    } catch (Exception ig) {
                        JOptionPane.showMessageDialog(null, ig.getMessage());
                    }
                }
                else
                {
                    try
                    {
                        Integer.parseInt(this.menuPrimario.txtTelefono.getText());
                        tel= this.menuPrimario.txtTelefono.getText();
                    }
                    catch (Exception eT)
                    {
                        try {
                            throw new Exception("Error, el número de teléfono contiene caracteres no numéricos");
                        } catch (Exception ignore) {
                        }
                    }
                }


                if (this.menuPrimario.cmbHuella.getSelectedItem() == "Si") {
                    try {
                        fPrint.setData(
                                1,
                                0,
                                dpi,
                                nombre1,
                                nombre2,
                                apellido1,
                                apellido2,
                                idMun,
                                tel
                        );

                        setFalseTxts();
                        fPrint.start();
                        fPrint.startPrinting();
                        fPrint.fingerPrintStatus();

                    } catch (SQLException em) {
                        JOptionPane.showMessageDialog(null, "Error! " + em.getMessage());
                    }
                } else if (this.menuPrimario.cmbHuella.getSelectedItem() == "No") {
                    try {
                        connection conn3 = new connection();
                        conn3.visitantes(
                                1,
                                0,
                                dpi,
                                new byte[0],
                                nombre1,
                                nombre2,
                                apellido1,
                                apellido2,
                                idMun,
                                tel
                        );
                        conn3.closeConnection();
                        setFalseTxts();
                        JOptionPane.showMessageDialog(null, "Visitante Registrado");
                        findByDPI(dpi);
                    } catch (SQLException | ClassNotFoundException throwables) {
                        if (conn != null) {
                            try {
                                conn.getConn().rollback();
                            } catch (SQLException sqlException) { JOptionPane.showMessageDialog(null,sqlException.toString());
                            }
                        }
                        JOptionPane.showMessageDialog(null, "Error: " + throwables.toString());
                    }
                }

            } else {
                JOptionPane.showMessageDialog(null, "Todos los campos son necesarios!");
                setFalseTxts();
            }
        }

    /*===========================
    GUARDAR VISITA
    ===========================*/
        if (menuPrimario.btnGuardarVisita == e.getSource()) {
            String[] d = Data.getData();

            try {
                connection finalconn = new connection();
                finalconn.insertVisita(d[1]);
                JOptionPane.showMessageDialog(null, "Visita guardada");
                StringBuilder oficinasNameReporte = new StringBuilder();
                for (String ofName : this.listOficinasNombreTemporalVisita) oficinasNameReporte.append(ofName);

                try {
                    int temporalIDFuncion = -1;
                    connection finalconn2 = new connection();
                    ResultSet resxD = finalconn2.getLastVisita();
                    resxD.next();
                    temporalIDFuncion = resxD.getInt(1);
                    resxD.close();
                    finalconn2.closeConnection();


                    for (int i = 0; i < this.listFuncionesTemporalVisita.size(); i++) {
                        try {
                            connection temporalConn = new connection();
                            temporalConn.insertDetalleVisita(temporalIDFuncion, this.listFuncionesTemporalVisita.get(i), this.listOficinasTemporalVisita.get(i));
                            temporalConn.closeConnection();
                        } catch (Exception exc) {
                            JOptionPane.showMessageDialog(null, exc.toString());
                        }
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, ex.toString());
                }

                generateVisitReport(
                        Visitas.txtDPI.getText(),
                        Visitas.txtNombre1.getText(),
                        Visitas.txtNombre2.getText(),
                        Visitas.txtApellido1.getText(),
                        Visitas.txtApellido2.getText(),
                        oficinasNameReporte.toString()
                );
                finalconn.closeConnection();



            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex.toString());
            }
            setFalseTxts();

        }
    }

    /*==============================================
    BUSCAR POR DPI
    ==============================================*/
    public void findByDPI(String strDPI){
        /*=========================================
        REMOVE THE ACTION LISTENER FROM CMBDEPARTAMENTO
        ==========================================*/
        ActionListener[] l = Visitas.cmbPrecedencia.getActionListeners();
        if (l.length > 0) Visitas.cmbPrecedencia.removeActionListener(l[0]);
        try {
            connection conn2 = new connection();
            ResultSet resulttt = conn2.buscarVisitantesDPI(
                    5,
                    0,
                    strDPI,
                    0,
                    "",
                    "",
                    "",
                    "",
                    0,
                    ""
            );
            if (resulttt.next()) {
                Data d = new Data(
                        resulttt.getInt(1),
                        resulttt.getString(2),
                        resulttt.getBlob(3),
                        resulttt.getString(4),
                        resulttt.getString(5),
                        resulttt.getString(6),
                        resulttt.getString(7),
                        resulttt.getInt(8),
                        resulttt.getInt(9),
                        resulttt.getString(10),
                        resulttt.getString(11),
                        resulttt.getString(12)
                );

                resulttt.close();
                conn2.closeConnection();
                fillWithData();
                controller.controllerVisitas.controllerVisitas.enableFuncionOficina();
                this.menuPrimario.btnAgregarFuncion.setEnabled(true);
                this.menuPrimario.btnGuardarVisita.setEnabled(true);
            } else {
                JOptionPane.showMessageDialog(null, "Persona no registrada");
                resulttt.close();
                conn2.closeConnection();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }
    }

    /*==============================================
    BUSCAR POR DPI STATIC
    ==============================================*/
    public static void findByDPI2(String strDPI){
        /*=========================================
        REMOVE THE ACTION LISTENER FROM CMBDEPARTAMENTO
        ==========================================*/
        ActionListener[] l = Visitas.cmbPrecedencia.getActionListeners();
        if (l.length > 0) Visitas.cmbPrecedencia.removeActionListener(l[0]);
        try {
            connection conn2 = new connection();
            ResultSet resulttt = conn2.buscarVisitantesDPI(
                    5,
                    0,
                    strDPI,
                    0,
                    "",
                    "",
                    "",
                    "",
                    0,
                    ""
            );
            if (resulttt.next()) {
                Data d = new Data(
                        resulttt.getInt(1),
                        resulttt.getString(2),
                        resulttt.getBlob(3),
                        resulttt.getString(4),
                        resulttt.getString(5),
                        resulttt.getString(6),
                        resulttt.getString(7),
                        resulttt.getInt(8),
                        resulttt.getInt(9),
                        resulttt.getString(10),
                        resulttt.getString(11),
                        resulttt.getString(12)
                );

                resulttt.close();
                conn2.closeConnection();
                fillWithData();
                controller.controllerVisitas.controllerVisitas.enableFuncionOficina();
                Visitas.btnAgregarFuncion.setEnabled(true);
                Visitas.btnGuardarVisita.setEnabled(true);
            } else {
                JOptionPane.showMessageDialog(null, "Persona no registrada");
                resulttt.close();
                conn2.closeConnection();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }
    }

    /*=================================================================
    LLENAR LOS CAMPOS CON LOS DATOS (QUERY->(MODEL)DATA->FILLWITHDATA)
    =================================================================*/
    public static void fillWithData() {
        Visitas.cmbPrecedencia.removeAllItems();
        Visitas.cmbPrecedencica2.removeAllItems();
        String[] data = Data.getData();
        Visitas.txtDPI.setText(data[1]);
        Visitas.txtNombre1.setText(data[2]);
        Visitas.txtNombre2.setText(data[3]);
        Visitas.txtApellido1.setText(data[4]);
        Visitas.txtApellido2.setText(data[5]);
        Visitas.cmbPrecedencia.addItem(data[8]);
        Visitas.cmbPrecedencica2.addItem(data[9]);
        Visitas.txtTelefono.setText(data[10]);

        Visitas.cmbFuncion.setVisible(true);
        Visitas.cmbOficina.setVisible(true);
        Visitas.cmbFuncion.setEnabled(true);
        Visitas.txtSearchFuncion.setVisible(true);
        Visitas.txtSearchFuncion.setEnabled(true);
        Visitas.cmbOficina.setEnabled(false);
        Visitas.cmbHuella.setVisible(false);
        Visitas.cmbHuella.setEnabled(false);
        Visitas.lblHuella.setVisible(false);
    }

    /*======================================
    RESETEAR LOS CAMPOS
    ========================================*/
    public void setFalseTxts() {
        Visitas.txtDPI.setText("");
        Visitas.txtNombre1.setText("");
        Visitas.txtNombre2.setText("");
        Visitas.txtApellido1.setText("");
        Visitas.txtApellido2.setText("");
        Visitas.txtTelefono.setText("");
        this.menuPrimario.cmbPrecedencica2.removeAllItems();
        this.menuPrimario.cmbPrecedencia.removeAllItems();
        Visitas.cmbPrecedencia.removeAllItems();
        Visitas.cmbPrecedencica2.removeAllItems();
        Visitas.lblListaFunciones.setText("<html></html>");

        Visitas.txtDPI.setEnabled(false);
        Visitas.txtNombre1.setEnabled(false);
        Visitas.txtNombre2.setEnabled(false);
        Visitas.txtApellido1.setEnabled(false);
        Visitas.txtApellido2.setEnabled(false);
        Visitas.txtSearchFuncion.setVisible(false);
        Visitas.txtSearchFuncion.setEnabled(false);
        Visitas.cmbPrecedencia.setEnabled(false);
        Visitas.cmbPrecedencica2.setEnabled(false);
        Visitas.cmbFuncion.setEnabled(false);
        Visitas.cmbFuncion.setVisible(false);
        Visitas.cmbOficina.setEnabled(false);
        Visitas.cmbOficina.setVisible(false);
        Visitas.cmbSanitizacion.setVisible(false);
        Visitas.cmbHuella.setVisible(false);
        Visitas.lblHuella.setVisible(false);
        Visitas.txtTelefono.setEnabled(false);

        this.menuPrimario.btnVerificarHuella.setEnabled(true);
        this.menuPrimario.btnRegistrarHuella.setEnabled(false);
        this.menuPrimario.btnGuardarVisita.setEnabled(false);
        this.menuPrimario.btnAgregarFuncion.setEnabled(false);

        this.listOficinas.clear();
        this.listFunciones.clear();
        this.listOficinasTemporalVisita.clear();
        this.listFuncionesTemporalVisita.clear();
        this.listOficinasNombreTemporalVisita.clear();

    /*=========================================
    REMOVE THE ACTION LISTENER FROM CMBFUNCION
    ==========================================*/
        ActionListener[] l = Visitas.cmbFuncion.getActionListeners();
        if (l.length > 0) Visitas.cmbFuncion.removeActionListener(l[0]);
    }


  /*========================================
  HABILITAR COMBOBOX FUNCIONES Y OFICINAS
  ========================================*/
    public static void enableFuncionOficina() throws SQLException, ClassNotFoundException {

        Visitas.cmbFuncion.removeAllItems();
        Visitas.txtSearchFuncion.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                listFunciones.clear();
                try {
                    connection connStatic = new connection();
                    Visitas.cmbFuncion.removeAllItems();
                    ResultSet res;
                    PreparedStatement stmt = connStatic.getConn().prepareStatement("SELECT id, funcion FROM funciones WHERE funcion LIKE '%"+Visitas.txtSearchFuncion.getText()+"%'");
                    res=stmt.executeQuery();
                    while (res.next()) addItemList(res.getInt(1), res.getString(2));
                    res.close();
                    connStatic.closeConnection();
                    getOficinasByFuncion();

                } catch (SQLException | ClassNotFoundException throwables) {
                    JOptionPane.showMessageDialog(null, throwables.toString());
                }

            }
        });
    }

    /*================================================================================
    AGREGAR LAS FUNCIONES QUE COINCIDAN CON EL INPUT DEL TXT AL COMBOBOX Y A LA LISTA
    =================================================================================*/
    protected static void addItemList(int id, String item) {
        listFunciones.add(id);
        Visitas.cmbFuncion.addItem(item);
    }

    /*=============================================================
    OBTENER OFICINAS SEGUN LA FUNCION SELECCIONADA
    =============================================================*/
    protected static void getOficinasByFuncion() throws SQLException, ClassNotFoundException {
        Visitas.cmbFuncion.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        listOficinas.clear();
                        Visitas.cmbOficina.removeAllItems();
                        Visitas.cmbOficina.setEnabled(true);

                        if (listFunciones.size() > 0) {
                            try {
                                connection connStatic3 = new connection();
                                ResultSet res2 = connStatic3.getOficinas(listFunciones.get(Visitas.cmbFuncion.getSelectedIndex()));
                                while (res2.next()) {
                                    listOficinas.add(res2.getInt(2));
                                    Visitas.cmbOficina.addItem(res2.getString(4) + " (" + res2.getString(5) + ")");
                                }

                                res2.close();
                                connStatic3.closeConnection();

                            } catch (SQLException | ClassNotFoundException ex) {
                                JOptionPane.showMessageDialog(null, ex.getMessage());
                            }
                        }
                    }
                }
        );
        /*==========================================
        ENABLE COMBOBOX SANITIZACION
        ===========================================*/
        Visitas.cmbSanitizacion.setVisible(true);
    }


    /*==========================================
    GENERAR REPORTE SIMPLE PARA EL VISITANTE
    ==========================================*/
    public void generateVisitReport(
            String dpiIncome,
            String nombre1Income,
            String nombre2Income,
            String apellido1Income,
            String apellido2Income,
            String oficinaIncome
    ) {
        Map<String, Object> hashDataSource = new HashMap<String, Object>();
        JasperPrint jasperPrint;
        JasperDesign jasperDesign;
        JasperReport jasperReport;
        JasperViewer jasperViewer;
        List<String> dpi = new LinkedList<>();
        List<String> nombre1 = new LinkedList<>();
        List<String> nombre2 = new LinkedList<>();
        List<String> apellido1 = new LinkedList<>();
        List<String> apellido2 = new LinkedList<>();
        List<String> oficina = new LinkedList<>();
        List<String> fecha = new LinkedList<>();
        List<String> sanitizacion = new LinkedList<>();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        try {
            connection finalconn= new connection();
            finalconn.getConn().prepareStatement("INSERT INTO numero_reporte VALUES(GETDATE())").execute();
            ResultSet resNum= finalconn.getConn().prepareStatement("SELECT MAX(id) FROM numero_reporte").executeQuery();
            resNum.next();
            String numeroReporte= resNum.getString(1);
            resNum.close();
            finalconn.closeConnection();

            dpi.add(dpiIncome);
            nombre1.add(nombre1Income);
            nombre2.add(nombre2Income);
            apellido1.add(apellido1Income);
            apellido2.add(apellido2Income);
            oficina.add(oficinaIncome);
            fecha.add(dtf.format(now));
            sanitizacion.add(
                    this.menuPrimario.cmbSanitizacion.getSelectedItem().toString()
            );

            hashDataSource.put("dpi", dpi);
            hashDataSource.put("nombre1", nombre1);
            hashDataSource.put("nombre2", nombre2);
            hashDataSource.put("apellido1", apellido1);
            hashDataSource.put("apellido2", apellido2);
            hashDataSource.put("oficina", oficina);
            hashDataSource.put("fecha", fecha);
            hashDataSource.put("sanitizacion", sanitizacion);

            jasperPrint =
                    JasperFillManager.fillReport(
                            ClassLoader.getSystemResourceAsStream("report-visita.jasper"),
                            hashDataSource,
                            new JREmptyDataSource()
                    );


            JRPdfExporter exp = new JRPdfExporter();
            exp.setExporterInput(new SimpleExporterInput(jasperPrint));
            exp.setExporterOutput(
                    new SimpleOutputStreamExporterOutput("reporte_visita"+numeroReporte+".pdf")
            );
            SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
            exp.setConfiguration(conf);
            exp.exportReport();
            showReport("reporte_visita"+numeroReporte+".pdf");
        } catch (JRException | SQLException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }

    }

    /*===================================
    DESPLEGAR EL REPORTE EN PANTALLA
    ===================================*/
    protected void showReport(String reportPath) {
        try {
            File report = new File(reportPath);
            Desktop.getDesktop().open(report);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }
    }
}

