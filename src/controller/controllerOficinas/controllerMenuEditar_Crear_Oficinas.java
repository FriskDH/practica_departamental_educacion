package controller.controllerOficinas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Objects;
import javax.swing.*;
import model.connection.connection;
import view.menu.edicionOficinas;

public class controllerMenuEditar_Crear_Oficinas implements ActionListener {
  edicionOficinas edOf;
  connection conn, conn2;
  int idOficina;
  LinkedList<Integer> listEdificios;

  public controllerMenuEditar_Crear_Oficinas(
    edicionOficinas edOf,
    String mode,
    String oficina,
    int idOficina,
    String edificio

  ) {
    this.edOf = edOf;
    this.idOficina = idOficina;
    listEdificios = new LinkedList<>();


    if (mode.equals("C")) {
      this.edOf.btnCrear.addActionListener(this);
      this.edOf.btnGuardar.setVisible(false);
      this.edOf.btnCrear.setVisible(true);
    }
    if (mode.equals("E")) {
      this.edOf.btnGuardar.addActionListener(this);
      this.edOf.txtOficina.setText(oficina);
      this.edOf.btnGuardar.setVisible(true);
      this.edOf.btnCrear.setVisible(false);
      this.edOf.txtOficina.setText(oficina);
    }

    try{
      conn2= new connection();
      ResultSet res= conn2.crudEdificios(2, 0, "");
      while(res.next()){
        this.listEdificios.add(res.getInt(1));
        this.edOf.cmbEdificio.addItem(res.getString(2));
      }
    }catch (Exception e){
      JOptionPane.showMessageDialog(null, e.toString());
    }

    for(int i=0; i<this.edOf.cmbEdificio.getItemCount(); i++){
      if (this.edOf.cmbEdificio.getItemAt(i).equals(edificio)) this.edOf.cmbEdificio.setSelectedIndex(i);
    }

  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (this.edOf.btnCrear == e.getSource()) {
      /*
      try {

        String[] tempMun = Objects
                .requireNonNull(this.edOf.cmbEdificio.getSelectedItem())
                .toString()
                .split("|");

        String tempMenu2= "";
        for(int j=0; j< tempMun.length; j++){
          try{
            if (!tempMun[j].equals(" | ")){
              Integer.parseInt(tempMun[j]);
              tempMenu2=tempMenu2+Integer.parseInt(tempMun[j]);
            }
            j=tempMun.length;
          }catch (Exception e22){

          }
        }

        int idEd = Integer.parseInt(tempMenu2);
        */

      try {
        conn = new connection();
        if(conn.createOficinas(this.edOf.txtOficina.getText(), this.listEdificios.get(this.edOf.cmbEdificio.getSelectedIndex()))){
          this.edOf.framePrimario.dispose();
          conn.closeConnection();
        }
        else{
          if (conn != null) {
            try {
              conn.getConn().rollback();
            } catch (SQLException sqlException) {
              JOptionPane.showMessageDialog(null,sqlException.toString());
            }
          }
          JOptionPane.showMessageDialog(null, "Ha ocurrido un error, intente de nuevo.");
        }
      }
      catch (Exception throwables) {
        JOptionPane.showMessageDialog(null, throwables.toString());
      }

    }

    if (this.edOf.btnGuardar == e.getSource()) {
      try {
        /*
        String[] tempMun = Objects
                .requireNonNull(this.edOf.cmbEdificio.getSelectedItem())
                .toString()
                .split("|");

        String tempMenu2= "";
        for(int j=0; j< tempMun.length; j++){
          try{
            if (!tempMun[j].equals(" | ")){
              Integer.parseInt(tempMun[j]);
              tempMenu2=tempMenu2+Integer.parseInt(tempMun[j]);
            }
            j=tempMun.length;
          }catch (Exception e22){

          }
        }

        int idEd = Integer.parseInt(tempMenu2);*/
        conn = new connection();
        if(conn.updateOficinas(this.idOficina, this.edOf.txtOficina.getText(), this.listEdificios.get(this.edOf.cmbEdificio.getSelectedIndex()))){
          this.edOf.framePrimario.dispose();
          conn.closeConnection();
        }
        else{
          if (conn != null) {
            try {
              conn.getConn().rollback();
            } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
              JOptionPane.showMessageDialog(null,sqlException.toString());
            }
          }
          JOptionPane.showMessageDialog(null, "Ha ocurrido un error");
        }
      }  //        classNotFoundException.printStackTrace();
      catch (Exception throwables) {
        throwables.printStackTrace();
        JOptionPane.showMessageDialog(null, throwables.toString());
      }
    }
  }
}
