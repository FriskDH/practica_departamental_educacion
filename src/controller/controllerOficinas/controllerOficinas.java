package controller.controllerOficinas;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import model.connection.connection;
import view.menu.Oficinas;
import view.menu.edicionOficinas;

public class controllerOficinas implements ActionListener {
  Oficinas adminOficinas;
  DefaultTableModel oficinasTable, oficinasTableShow;
  connection conn;
  String[] rowStrings, rowStringsShow;

  public controllerOficinas(Oficinas adminOficinas) {
    this.adminOficinas = adminOficinas;
    this.adminOficinas.btnBuscar.addActionListener(this);
    this.adminOficinas.btnCrear.addActionListener(this);
    this.adminOficinas.btnEditar.addActionListener(this);
    this.adminOficinas.btnEliminar.addActionListener(this);
    rowStrings = new String[3];
    rowStringsShow = new String[2];
    oficinasTable = new DefaultTableModel();
    
    oficinasTableShow =
      new DefaultTableModel(
        new Object[][] { null },
        new String[] { "Oficina", "Edificio" }
      ) {

        //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
        @Override
        public boolean isCellEditable(int rowIndex, int colIndex) {
          return false;
        }
      };
    
    oficinasTable =
      new DefaultTableModel(
        new Object[][] { null },
        new String[] { "ID", "Oficina", "Edificio" }
      ) {

        //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
        @Override
        public boolean isCellEditable(int rowIndex, int colIndex) {
          return false;
        }
      };
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (this.adminOficinas.btnBuscar == e.getSource()) {
      deleteData();

      
      if (this.adminOficinas.txtOficina.getText().equals("")) {
        searchEmpty();
      }
      else {
        searchNotEmpty();
      }
    }

    if (this.adminOficinas.btnCrear == e.getSource()) {
      create();
    }

    if (this.adminOficinas.btnEditar == e.getSource()) {
      edit();
    }

    if (this.adminOficinas.btnEliminar == e.getSource()) {
      delete();
      deleteData();
    }
  }
  
  /*==========================
  BORRAR DATOS EN TABLA
  ===========================*/
  protected void deleteData() {
    for (
      int i = this.oficinasTable.getRowCount() - 1;
      i >= 0;
      i--
    ) {this.oficinasTable.removeRow(i); this.oficinasTableShow.removeRow(i);}
  }
  
  /*==========================
  CREAR
  ===========================*/
  protected void create() {
    edicionOficinas edOf = new edicionOficinas();
    controllerMenuEditar_Crear_Oficinas editarCrear = new controllerMenuEditar_Crear_Oficinas(
      edOf,
      "C",
      "",
      0,
      ""
    );

    edOf.framePrimario.addWindowListener(new WindowAdapter() {
      @Override
      public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
        Frame frame = (Frame) e.getSource();
        refreshTable();
      }
    });

    edOf.framePrimario.setVisible(true);
  }
  
  /*==========================
  EDITAR
  ===========================*/
  protected void edit() {
    edicionOficinas edOf = new edicionOficinas();
    
    controllerMenuEditar_Crear_Oficinas editarCrear = new controllerMenuEditar_Crear_Oficinas(
      edOf, "E", 
      this.oficinasTable.getValueAt(this.adminOficinas.tableOficinas.getSelectedRow(), 1).toString(),
      Integer.parseInt(this.oficinasTable.getValueAt(this.adminOficinas.tableOficinas.getSelectedRow(), 0).toString()),
      this.oficinasTableShow.getValueAt(this.adminOficinas.tableOficinas.getSelectedRow(), 1).toString()
      );

    edOf.framePrimario.addWindowListener(new WindowAdapter() {
      @Override
      public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
        Frame frame = (Frame) e.getSource();
        refreshTable();
      }
    });

    edOf.framePrimario.setVisible(true);
  }
  
  /*==========================
  BORRAR
  ===========================*/
  protected void delete() {
    try {
      this.conn = new connection();
      conn.deleteOficinas(
              Integer.parseInt(oficinasTable.getValueAt(
                      this.adminOficinas.tableOficinas.getSelectedRow(),
                      0
              ).toString())
      );
    } catch (Exception ex) {
      if (conn != null) {
        try {
          conn.getConn().rollback();
        } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
          JOptionPane.showMessageDialog(null,sqlException.toString());
        }
      }
      JOptionPane.showMessageDialog(null, ex.toString());
    }
  }

  /*=====================
  BUSCAR CON TXT VACIO
  =====================*/
  protected void searchEmpty(){
    try {

      conn = new connection();
      ResultSet res = conn.getOficinasList();
      while (res.next()) {
        rowStrings[0] = res.getString(1);
        rowStrings[1] = res.getString(2);
        rowStrings[2] = res.getString(3);
        rowStringsShow[0] = res.getString(2);
        rowStringsShow[1] = res.getString(4);
        this.oficinasTable.addRow(rowStrings);
        this.oficinasTableShow.addRow(rowStringsShow);
      }

      res.close();
      conn.closeConnection();
      this.adminOficinas.tableOficinas.setModel(oficinasTableShow);
    }

    catch (Exception exc) {
      JOptionPane.showMessageDialog(
              null,
              "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
      );
      //                    exc.printStackTrace();
    }
  }

  /*========================
  BUSCAR CON TXT NO VACIO
  ========================*/
  protected void searchNotEmpty(){
    try {
      conn = new connection();
      ResultSet res = conn.getOficinasByName(
              this.adminOficinas.txtOficina.getText()
      );
      if (res == null) {
        JOptionPane.showMessageDialog(
                null,
                "Error, la oficina buscada no existe."
        );
      } else {
        while (res.next()) {
          rowStrings[0] = res.getString(1);
          rowStrings[1] = res.getString(2);
          rowStrings[2] = res.getString(3);
          rowStringsShow[0] = res.getString(2);
          rowStringsShow[1] = res.getString(3);
          this.oficinasTable.addRow(rowStrings);
          this.oficinasTableShow.addRow(rowStringsShow);
        }
      }


      res.close();
      conn.closeConnection();
      this.adminOficinas.tableOficinas.setModel(oficinasTableShow);
    }


    catch (Exception exc) {
      JOptionPane.showMessageDialog(
              null,
              "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
      );
      //                   exc.printStackTrace();
    }
  }

  protected void refreshTable(){
    deleteData();
    searchEmpty();
  }
}
