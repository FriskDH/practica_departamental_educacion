package controller.controllerVisitantes;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import javax.swing.*;
import model.connection.connection;
import model.personData.Data;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;
import view.menu.Visitantes;
import view.menu.Reportes;
import view.menu.Visitas;

public class controllerVisitantes implements ActionListener {
  Visitantes consultaUsuarioo;
  connection conn;

  public controllerVisitantes(Visitantes consultaUsuarioo)
    throws SQLException, ClassNotFoundException {
    this.consultaUsuarioo = consultaUsuarioo;
    this.consultaUsuarioo.btnLimpiar.addActionListener(this);
    this.consultaUsuarioo.btnImprimir.addActionListener(this);
    this.consultaUsuarioo.btnBuscarDPI.addActionListener(this);
    setFalseTxts();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    //GENERAR REPORTE DE PERSONA
    if (consultaUsuarioo.btnImprimir == e.getSource()) {
      if(!this.consultaUsuarioo.txtDPI.getText().equals("")){
        try {
          conn = new connection();
          //                ResultSet res = conn.getVisitasSegunVisitante(Integer.parseInt(this.consultaUsuarioo.txtDPI.getText()));
          generateReport(
                  this.consultaUsuarioo.txtDPI.getText(),
                  this.consultaUsuarioo.txtNombre1.getText(),
                  this.consultaUsuarioo.txtNombre2.getText(),
                  this.consultaUsuarioo.txtApellido1.getText(),
                  this.consultaUsuarioo.txtApellido2.getText(),
                  this.consultaUsuarioo.cmbPrecedencia.getSelectedItem().toString(),
                  this.consultaUsuarioo.cmbPrecedencica2.getSelectedItem().toString()
          );
        } catch (SQLException | ClassNotFoundException throwables) {
          //                    throwables.printStackTrace();
          JOptionPane.showMessageDialog(null, "Error: " + throwables.toString());
        }
      }else{
        JOptionPane.showMessageDialog(null, "Error! No se ha buscado un visitante");
      }
      setFalseTxts();
      }


    /*=================================
    LIMIPIAR LAS CASILLAS Y COMBOBOX
    =================================*/
    if (consultaUsuarioo.btnLimpiar == e.getSource()) {
      setFalseTxts();
    }

    /*=================
    BUSCAR POR DPI
    =================*/
    if (consultaUsuarioo.btnBuscarDPI == e.getSource()) {
      String strDPI = JOptionPane.showInputDialog(null, "Ingrese DPI");
      try {
        connection conn2 = new connection();
        ResultSet resulttt = conn2.buscarVisitantesDPI(
          5,
          0,
          strDPI,
          0,
          "",
          "",
          "",
          "",
          0,
          ""
        );
        if (resulttt.next()) {
          this.consultaUsuarioo.txtDPI.setText(resulttt.getString(2));
          this.consultaUsuarioo.txtNombre1.setText(resulttt.getString(4));
          this.consultaUsuarioo.txtNombre2.setText(resulttt.getString(5));
          this.consultaUsuarioo.txtApellido1.setText(resulttt.getString(6));
          this.consultaUsuarioo.txtApellido2.setText(resulttt.getString(7));
          this.consultaUsuarioo.cmbPrecedencia.setSelectedItem(
              resulttt.getInt(8)
            );
          this.consultaUsuarioo.cmbPrecedencica2.setSelectedItem(
              resulttt.getInt(9)
            );

          Data d = new Data(
            resulttt.getInt(1),
            resulttt.getString(2),
            resulttt.getBlob(3),
            resulttt.getString(4),
            resulttt.getString(5),
            resulttt.getString(6),
            resulttt.getString(7),
            resulttt.getInt(8),
            resulttt.getInt(9),
            resulttt.getString(10),
            resulttt.getString(11),
            resulttt.getString(12)
          );

          fillWithData();
        } else {
          JOptionPane.showMessageDialog(null, "Persona no registrada");
          conn2.closeConnection();
        }
        resulttt.close();
        conn2.closeConnection();
        //                String im[] = Data.getData();
        //                System.out.println(Arrays.toString(im));
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  /*===================================
  LLENAR CON LOS DATOS DEL VISITANTE
  ===================================*/
  public static void fillWithData() {
    String[] data = Data.getData();
    Visitantes.txtDPI.setText(data[1]);
    Visitantes.txtNombre1.setText(data[2]);
    Visitantes.txtNombre2.setText(data[3]);
    Visitantes.txtApellido1.setText(data[4]);
    Visitantes.txtApellido2.setText(data[5]);
    Visitantes.cmbPrecedencia.addItem(data[8]);
    Visitantes.cmbPrecedencica2.addItem(data[9]);
    Visitantes.txtTelefono.setText(data[10]);
  }

  /*===================================
  DESACTIVAR/RESETEAR LOS CAMPOS
  ===================================*/
  public void setFalseTxts() {
    Visitantes.txtDPI.setText("");
    Visitantes.txtNombre1.setText("");
    Visitantes.txtNombre2.setText("");
    Visitantes.txtApellido1.setText("");
    Visitantes.txtApellido2.setText("");
    Visitantes.txtTelefono.setText("");
    Visitantes.cmbPrecedencia.removeAllItems();
    Visitantes.cmbPrecedencica2.removeAllItems();
    //        view.menu.consultaUsuarioo.cmbFuncion.removeAllItems();
    Visitantes.txtDPI.setEnabled(false);
    Visitantes.txtNombre1.setEnabled(false);
    Visitantes.txtNombre2.setEnabled(false);
    Visitantes.txtApellido1.setEnabled(false);
    Visitantes.txtApellido2.setEnabled(false);
    Visitantes.cmbPrecedencia.setEnabled(false);
    Visitantes.cmbPrecedencica2.setEnabled(false);
    Visitantes.txtTelefono.setEnabled(false);
  }

  /*===================================
  GENERAR REPORTE SIMPLE DEL VISITANTE
  ===================================*/
  protected void generateReport(
    String dpiIncome,
    String nombre1Income,
    String nombre2Income,
    String apellido1Income,
    String apellido2Income,
    String precedencia1Income,
    String precedencia2Income
  ) {
    Reportes reportes = new Reportes();
    Map<String, Object> hashDataSource = new HashMap<String, Object>();
    JasperPrint jasperPrint;
    JasperDesign jasperDesign;
    JasperReport jasperReport;
    JasperViewer jasperViewer;
    List<String> dpi = new LinkedList<>();
    List<String> nombre1 = new LinkedList<>();
    List<String> nombre2 = new LinkedList<>();
    List<String> apellido1 = new LinkedList<>();
    List<String> apellido2 = new LinkedList<>();
    List<String> precedencia = new LinkedList<>();
    List<String> funcion = new LinkedList<>();
    List<String> fecha = new LinkedList<>();
    List<String> hora = new LinkedList<>();
    List<String> oficina = new LinkedList<>();
    try {
      ResultSet res = conn.getVisitasSegunVisitante(
        this.consultaUsuarioo.txtDPI.getText()
      );
      conn = new connection();
      dpi.add(dpiIncome);
      nombre1.add(nombre1Income);
      nombre2.add(nombre2Income);
      apellido1.add(apellido1Income);
      apellido2.add(apellido2Income);
      precedencia.add(precedencia1Income + ", " + precedencia2Income);
      while (res.next()) {
        funcion.add(res.getString(1));
        fecha.add(res.getString(2));
        hora.add(res.getString(3));
        oficina.add(res.getString(4));
      }
      hashDataSource.put("dpi", dpi);
      hashDataSource.put("nombre1", nombre1);
      hashDataSource.put("nombre2", nombre2);
      hashDataSource.put("apellido1", apellido1);
      hashDataSource.put("apellido2", apellido2);
      hashDataSource.put("precedencia", precedencia);
      hashDataSource.put("funcion", funcion);
      hashDataSource.put("fecha", fecha);
      hashDataSource.put("oficina", oficina);
      hashDataSource.put("hora", hora);
      jasperPrint =
        JasperFillManager.fillReport(
          ClassLoader.getSystemResourceAsStream("report-filtered-persona.jasper"),
          hashDataSource,
          new JREmptyDataSource()
        );
      JRPdfExporter exp = new JRPdfExporter();
      exp.setExporterInput(new SimpleExporterInput(jasperPrint));
      exp.setExporterOutput(
        new SimpleOutputStreamExporterOutput(
          "reporte_persona_" + nombre1.get(0) + " " + apellido1.get(0) + ".pdf"
        )
      );
      SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
      exp.setConfiguration(conf);
      exp.exportReport();
      showReport("reporte_persona_" + nombre1.get(0) + " " + apellido1.get(0) + ".pdf");
    } catch (SQLException | JRException | ClassNotFoundException ex) {
                  ex.printStackTrace();
      JOptionPane.showMessageDialog(null, "Error: " + ex.toString());
    }
    dpi.clear();
    nombre1.clear();
    nombre2.clear();
    apellido1.clear();
    apellido2.clear();
    precedencia.clear();
    hora.clear();
  }

  protected void showReport(String reportPath){
    try {
      File report = new File(reportPath);
      Desktop.getDesktop().open(report);
    }catch(IOException ex){
      JOptionPane.showMessageDialog(null, ex.toString());
    }
  }
}
