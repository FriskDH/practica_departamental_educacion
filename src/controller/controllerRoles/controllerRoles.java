package controller.controllerRoles;

import controller.controllerFunciones.controllerMenuEditar_Crear_Funciones;
import model.connection.connection;
import view.menu.Funciones;
import view.menu.Roles;
import view.menu.edicionFunciones;
import view.menu.edicionRoles;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

public class controllerRoles implements ActionListener {
    DefaultTableModel rolesTable, rolesTableShow;
    connection conn;
    String[] rowStrings, rowStringsShow;
    Roles adminRoles;

    public controllerRoles(Roles adminRoles) {
        this.adminRoles = adminRoles;
        this.adminRoles.btnBuscar.addActionListener(this);
        this.adminRoles.btnCrear.addActionListener(this);
        this.adminRoles.btnEditar.addActionListener(this);
        this.adminRoles.btnEliminar.addActionListener(this);
        rowStrings = new String[2];
        rowStringsShow = new String[1];
        rolesTable = new DefaultTableModel();
        rolesTable =
                new DefaultTableModel(
                        new Object[][]{null},
                        new String[]{"ID", "Rol"}
                ) {

                    //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };

        rolesTableShow = new DefaultTableModel();
        rolesTableShow =
                new DefaultTableModel(
                        new Object[][]{null},
                        new String[]{"Rol"}
                ) {

                    //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.adminRoles.btnBuscar == e.getSource()) {
            deleteData();

            if (this.adminRoles.txtRol.getText().equals("")) {
                searchEmpty();
            } else {
                searchNotEmpty();
            }
        }

        if (this.adminRoles.btnCrear == e.getSource()) {
            create();
        }

        if (this.adminRoles.btnEditar == e.getSource()) {
            edit();
        }

        if (this.adminRoles.btnEliminar == e.getSource()) {
            try {
                delete();
                deleteData();
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(null, exception.toString());
            }
        }
    }


    /*===============
    BORRAR
    ===============*/
    protected void delete() throws Exception {
        try {
            this.conn = new connection();
            conn.assignRolesToModulos(4,
                    Integer.parseInt(rolesTable.getValueAt(
                    this.adminRoles.tableRoles.getSelectedRow(), 0
            ).toString()),
                    0
            );
            conn.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            if (conn != null) {
                try {
                    conn.getConn().rollback();
                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                    JOptionPane.showMessageDialog(null,sqlException.toString());
                }
            }
            JOptionPane.showMessageDialog(null, ex.toString());
//      ex.printStackTrace();
        }
        try {
            this.conn = new connection();
            conn.deleteRoles(Integer.parseInt(rolesTable.getValueAt(this.adminRoles.tableRoles.getSelectedRow(), 0).toString()));
        } catch (SQLException | ClassNotFoundException ex) {
            if (conn != null) {
                try {
                    conn.getConn().rollback();
                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                    JOptionPane.showMessageDialog(null,sqlException.toString());
                }
            }
            JOptionPane.showMessageDialog(null, ex.toString());
//      ex.printStackTrace();
        }
    }

    /*============================
    BORRAR DATOS EN TABLA
    ============================*/
    protected void deleteData() {
        for (
                int i = this.rolesTable.getRowCount() - 1;
                i >= 0;
                i--
        ){
            this.rolesTable.removeRow(i);
            this.rolesTableShow.removeRow(i);}
    }


    /*===============
    CREAR
    ===============*/
    protected void create() {
        edicionRoles edRl = new edicionRoles();
        controllerMenuEditar_Crear_Roles editarCrear = new controllerMenuEditar_Crear_Roles(
                edRl,
                "C",
                "",
                0
        );

        edRl.framePrimario.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
                Frame frame = (Frame) e.getSource();
                refreshTable();
            }
        });


        edRl.framePrimario.setVisible(true);
    }


    /*===============
    EDITAR
    ===============*/
    protected void edit() {
        edicionRoles edRl = new edicionRoles();

        controllerMenuEditar_Crear_Roles editarCrear = new controllerMenuEditar_Crear_Roles(
                edRl,
                "E",
                this.rolesTable.getValueAt(
                        this.adminRoles.tableRoles.getSelectedRow(),
                        1
                )
                        .toString(),
                Integer.parseInt(
                        this.rolesTable.getValueAt(
                                this.adminRoles.tableRoles.getSelectedRow(),
                                0
                        )
                                .toString()
                )
        );

        edRl.framePrimario.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
                Frame frame = (Frame) e.getSource();
                refreshTable();
            }
        });

        edRl.framePrimario.setVisible(true);
    }

    /*=========================
    BUSCAR CUANDO TXT VACIO
    =========================*/
    protected void searchEmpty(){
        try {
            conn = new connection();
            ResultSet res = conn.crudRoles(2, 0, "");
            while (res.next()) {
                rowStrings[0] = res.getString(1);
                rowStrings[1] = res.getString(2);
                rowStringsShow[0]= res.getString(2);
                this.rolesTable.addRow(rowStrings);
                this.rolesTableShow.addRow(rowStringsShow);
            }
            res.close();
            conn.closeConnection();
            this.adminRoles.tableRoles.setModel(rolesTableShow);
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(
                    null,
                    "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
            );
            //                    exc.printStackTrace();
        }
    }

    /*=========================
    BUSCAR CUANDO TXT NO VACIO
    =========================*/
    protected void searchNotEmpty(){
        try {
            conn = new connection();
            ResultSet res = conn.crudRoles(5, 0,
                    this.adminRoles.txtRol.getText()
            );
            if (res == null) {
                JOptionPane.showMessageDialog(
                        null,
                        "Error, el rol buscado no existe."
                );
            } else {
                while (res.next()) {
                    rowStrings[0] = res.getString(1);
                    rowStrings[1] = res.getString(2);
                    rowStringsShow[0]= res.getString(2);
                    this.rolesTable.addRow(rowStrings);
                    this.rolesTableShow.addRow(rowStringsShow);
                }
            }
            res.close();
            conn.closeConnection();
            this.adminRoles.tableRoles.setModel(rolesTable);
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(
                    null,
                    "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
            );
            //                   exc.printStackTrace();
        }
    }

    /*=========================
    REFRESCAR TABLA
    =========================*/
    protected void refreshTable(){
        deleteData();
        searchEmpty();
    }
}

