package controller.controllerRoles;

import model.connection.connection;
import view.menu.edicionFunciones;
import view.menu.edicionRoles;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class controllerMenuEditar_Crear_Roles implements ActionListener {
    edicionRoles edRl;
    connection conn;
    int idRol;
    DefaultTableModel tableRoles, tableRolesShow;
    List<Integer> listModulosAprobados;
    List<Object> listSendNewModulos;
    List<Object> listIndexSendNewModulos;
    Vector<Object> indexModulosOriginales;
    Vector<Object> modulosCambiados;
    Vector<Object> funcionesOriginales;
 
    public controllerMenuEditar_Crear_Roles(edicionRoles edRl,
                                            String mode,
                                            String funcion,
                                            int idRol
    ) {
        this.edRl = edRl;
        this.idRol = idRol;

        listModulosAprobados = new LinkedList<>();
        listIndexSendNewModulos = new LinkedList<>();
        listSendNewModulos = new LinkedList<>();
        tableRoles = new DefaultTableModel();
        tableRoles =
                new DefaultTableModel(
                        new Object[][] { null },
                        new String[] { "ID", "Modulo", "Si/No" }
                ) {

                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        switch (colIndex) {
                            case 0:
                                return false;
                            case 1:
                                return false;
                            case 2:
                                return true;
                            default:
                                return false;
                        }
                    }


                    public Class<?> getColumnClass(int column) {
                        switch (column) {
                            case 0:
                                return String.class;
                            case 1:
                                return String.class;
                            case 2:
                                return Boolean.class;
                            default:
                                return String.class;
                        }
                    }
                };

        tableRolesShow = new DefaultTableModel();
        tableRolesShow =
                new DefaultTableModel(
                        new Object[][] { null },
                        new String[] { "Modulo", "Si/No" }
                ) {

                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        switch (colIndex) {
                            case 0:
                                return false;
                            case 1:
                                return true;
                            default:
                                return false;
                        }
                    }


                    public Class<?> getColumnClass(int column) {
                        switch (column) {
                            case 0:
                                return String.class;
                            case 1:
                                return Boolean.class;
                            default:
                                return String.class;
                        }
                    }
                };

        if (mode.equals("C")) {
            this.edRl.btnCrear.addActionListener(this);
            this.edRl.btnGuardar.setVisible(false);
            this.edRl.btnCrear.setVisible(true);
        }
        if (mode.equals("E")) {
            this.edRl.btnGuardar.addActionListener(this);
            this.edRl.txtFuncion.setText(funcion);
            this.edRl.btnGuardar.setVisible(true);
            this.edRl.btnCrear.setVisible(false);
//            this.edRl.txtFuncion.setText(funcion);
        }

        fillTable();
        setOficinasTrueOrFalse();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.edRl.btnCrear == e.getSource()) {
            boolean verificationNewFunciones = false;
            boolean verificationFunciones = false;
            this.listIndexSendNewModulos.clear();
            this.listSendNewModulos.clear();

            //CREAR ROL
            if (verifyFields()) {
                try {

                    conn = new connection();
                    conn.createRoles(this.edRl.txtFuncion.getText());
                    conn.closeConnection();

                } catch (Exception throwables) {
                    if (conn != null) {
                        try {
                            conn.getConn().rollback();
                        } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                            JOptionPane.showMessageDialog(null,sqlException.toString());
                        }
                    }
                    JOptionPane.showMessageDialog(null, throwables.toString());
                }

                for (int i = 0; i <= this.tableRoles.getRowCount() - 1; i++) {
                    //INSERTAR MODULOS NUEVOS
                    if (
                            this.edRl.tableRolees.getValueAt(i, 1)
                                    .toString()
                                    .equals("true")
                    ) {
                        try {

                            conn = new connection();
                            conn.assignRolesToModulos(7, 0, Integer.parseInt(this.tableRoles.getValueAt(i, 0).toString()));
                            conn.closeConnection();

                        } catch (Exception throwables) {if (conn != null) {
                            try {
                                conn.getConn().rollback();
                            } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                                JOptionPane.showMessageDialog(null,sqlException.toString());
                            }
                        }

                            JOptionPane.showMessageDialog(null, throwables.toString());
                        }
                    }
                }

                this.edRl.framePrimario.dispose();
            } else {
                JOptionPane.showMessageDialog(
                        null,
                        "Todos los campos son requeridos, y se debe asignar al menos un módulo"
                );
            }
        }

        //GUARDAR
        if (this.edRl.btnGuardar == e.getSource()) {
            boolean verificationOficina = false;
            boolean verificationNewFunciones = false;
            boolean verificationFunciones = false;
            this.modulosCambiados.clear();
            this.listIndexSendNewModulos.clear();
            this.listSendNewModulos.clear();

            //EDITAR ROL
            try{
                conn= new connection();
                conn.updateRoles(idRol, this.edRl.txtFuncion.getText());
                conn.closeConnection();
            } catch (Exception throwables) {
                if (conn != null) {
                    try {
                        conn.getConn().rollback();
                    } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                        JOptionPane.showMessageDialog(null,sqlException.toString());
                    }
                }
                JOptionPane.showMessageDialog(null, throwables.toString());
            }

            //CICLO PARA AGREGAR LOS PERMISOS NUEVOS DEL ROL A LA LISTA
            for (int i = 0; i <= tableRoles.getRowCount() - 1; i++) {
                this.modulosCambiados.add(this.edRl.tableRolees.getValueAt(i, 1));
            }

            for (int i = 0; i <= indexModulosOriginales.size() - 1; i++) {
                if (funcionesOriginales.get(i) != modulosCambiados.get(i)) {
                    this.listIndexSendNewModulos.add(indexModulosOriginales.get(i));
                    this.listSendNewModulos.add(this.modulosCambiados.get(i));
                }
            }



            //CICLO PARA INSERTAR/BORRAR
            for (int i = 0; i <= this.listIndexSendNewModulos.size() - 1; i++) {

                //INSERTAR MODULOS NUEVOS
                if (this.listSendNewModulos.get(i).toString().equals("true")) {
                    try {
                        conn = new connection();
                        conn.assignRolesToModulos(
                                1,
                                this.idRol,
                                Integer.parseInt(this.listIndexSendNewModulos.get(i).toString())
                        );
                        conn.closeConnection();
                    } catch (Exception throwables) {
                        if (conn != null) {
                            try {
                                conn.getConn().rollback();
                            } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                                JOptionPane.showMessageDialog(null,sqlException.toString());
                            }
                        }
                        JOptionPane.showMessageDialog(null, throwables.toString());
                    }
                }
                //BORRAR MODULOS
                else {
                    try {
                        conn = new connection();
                        conn.assignRolesToModulos(
                                8,
                                this.idRol,
                                Integer.parseInt(this.listIndexSendNewModulos.get(i).toString())
                        );
                        conn.closeConnection();
                    } catch (Exception throwables) {
                        if (conn != null) {
                            try {
                                conn.getConn().rollback();
                            } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                                JOptionPane.showMessageDialog(null,sqlException.toString());
                            }
                        }
                        JOptionPane.showMessageDialog(null, throwables.toString());
                    }
                }
            }
            this.edRl.framePrimario.dispose();
        }
    }

    protected boolean verifyFields() {
        int tempCont = 0;
        for (int i = 0; i <= this.edRl.tableRolees.getRowCount() - 1; i++) {
            if (
                    this.edRl.tableRolees.getValueAt(i, 1).toString().equals("true")
            ) {
                tempCont += 1;
            }
        }
        if (this.edRl.txtFuncion.getText().equals("") || (tempCont == 0)) {
            return false;
        }
        return true;
    }

    protected void fillTable() {
        Object[] rowFunciones = new Object[3];
        Object[] rowFuncionesShow= new Object[2];

        try {
            conn = new connection();
            ResultSet res = conn.crudModulos(2, 0, "");
            while (res.next()) {
                rowFunciones[0] = res.getString(1);
                rowFunciones[1] = res.getString(2);
                rowFunciones[2] = false;
                rowFuncionesShow[0]= res.getString(2);
                rowFuncionesShow[1]= false;
                tableRoles.addRow(rowFunciones);
                tableRolesShow.addRow(rowFuncionesShow);
            }
            res.close();
            conn.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }

        tableRoles.removeRow(0);
        tableRolesShow.removeRow(0);
        this.edRl.tableRolees.setModel(tableRolesShow);
    }

    protected void setOficinasTrueOrFalse() {
        this.modulosCambiados = new Vector<>();
        this.funcionesOriginales = new Vector<>();
        this.indexModulosOriginales = new Vector<>();

        //OBTENER FUNCIONES APROBADAS Y ALMACENARLAS EN LA LINKED LIST
        try {
            conn = new connection();
            ResultSet res = conn.getModulosRoles(this.idRol);
            while (res.next()) {
                this.listModulosAprobados.add(res.getInt(1));
            }
            res.close();
            conn.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }

        //CICLO PARA PALOMEAR LAS OFICINAS ASIGNADAS A LA TABLA
        for (int i = 0; i <= this.tableRoles.getRowCount() - 1; i++) {
            for (int j = 0; j <= this.listModulosAprobados.size() - 1; j++) {
                if (
                        Integer.parseInt(this.tableRoles.getValueAt(i, 0).toString()) ==
                                this.listModulosAprobados.get(j)
                ) {
                    tableRoles.setValueAt(true, i, 2);
                    tableRolesShow.setValueAt(true, i, 1);
                }
            }
        }

        this.edRl.tableRolees.setModel(this.tableRolesShow);

        //CICLO PARA AGREGAR LAS ACTUALES DE LA FUNCION A LA LISTA
        for (int i = 0; i <= this.tableRoles.getRowCount() - 1; i++) {
            indexModulosOriginales.add(this.tableRoles.getValueAt(i, 0));
            this.funcionesOriginales.add(this.tableRoles.getValueAt(i, 2));
        }
    }
}

