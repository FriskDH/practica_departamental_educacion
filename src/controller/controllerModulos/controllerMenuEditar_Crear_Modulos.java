package controller.controllerModulos;

import model.connection.connection;
import view.menu.edicionModulos;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class controllerMenuEditar_Crear_Modulos implements ActionListener {
    edicionModulos edMd;
    connection conn;
    int idmodulo;

    public controllerMenuEditar_Crear_Modulos(
            edicionModulos edMd,
            String mode,
            String modulo,
            int idmodulo
    ) {
        this.edMd = edMd;
        this.idmodulo = idmodulo;

        if (mode.equals("C")) {
            this.edMd.btnCrear.addActionListener(this);
            this.edMd.btnGuardar.setVisible(false);
            this.edMd.btnCrear.setVisible(true);
        }
        if (mode.equals("E")) {
            this.edMd.btnGuardar.addActionListener(this);
            this.edMd.txtModulo.setText(modulo);
            this.edMd.btnGuardar.setVisible(true);
            this.edMd.btnCrear.setVisible(false);
            this.edMd.txtModulo.setText(modulo);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.edMd.btnCrear == e.getSource()) {
            try {
                conn = new connection();
                conn.createModulo(this.edMd.txtModulo.getText());
                conn.closeConnection();
                this.edMd.framePrimario.dispose();

            }  //        classNotFoundException.printStackTrace();
            catch (Exception throwables) {
                if (conn != null) {
                    try {
                        conn.getConn().rollback();
                    } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                        JOptionPane.showMessageDialog(null,sqlException.toString());
                    }
                }
                JOptionPane.showMessageDialog(null, throwables.toString());
            }
        }

        if (this.edMd.btnGuardar == e.getSource()) {
            try {
                conn = new connection();
                conn.updateModulo(this.idmodulo, this.edMd.txtModulo.getText());
                conn.closeConnection();
                this.edMd.framePrimario.dispose();

            }  //        classNotFoundException.printStackTrace();
            catch (Exception throwables) {
                if (conn != null) {
                    try {
                        conn.getConn().rollback();
                    } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                        JOptionPane.showMessageDialog(null,sqlException.toString());
                    }
                }
                JOptionPane.showMessageDialog(null, throwables.toString());
            }
        }
    }
}
