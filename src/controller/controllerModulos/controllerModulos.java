package controller.controllerModulos;
import controller.controllerOficinas.controllerMenuEditar_Crear_Oficinas;
import model.connection.connection;
import view.menu.Modulos;
import view.menu.edicionModulos;
import view.menu.edicionModulos;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

public class controllerModulos implements ActionListener {
    Modulos adminModulos;
    DefaultTableModel modulosTable, modulosTableShow;
    connection conn;
    String[] rowStrings, rowStringsShow;

    public controllerModulos(Modulos adminModulos){
        this.adminModulos= adminModulos;
        this.adminModulos.btnBuscar.addActionListener(this);
        this.adminModulos.btnCrear.addActionListener(this);
        this.adminModulos.btnEditar.addActionListener(this);
        this.adminModulos.btnEliminar.addActionListener(this);
        rowStrings = new String[2];
        rowStringsShow= new String[1];
        modulosTable = new DefaultTableModel();
        modulosTable =
                new DefaultTableModel(
                        new Object[][] { null },
                        new String[] { "No.", "Modulo" }
                ) {

                    //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };

        modulosTableShow = new DefaultTableModel();
        modulosTableShow =
                new DefaultTableModel(
                        new Object[][] { null },
                        new String[] { "Modulo" }
                ) {

                    //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.adminModulos.btnBuscar == e.getSource()) {
            deleteData();

            if (this.adminModulos.txtModulo.getText().equals("")) {
                searchEmpty();
            } else {
                searchNotEmpty();
            }
        }

        if(this.adminModulos.btnCrear == e.getSource()){
            create();
        }

        if(this.adminModulos.btnEditar == e.getSource()){
            edit();
        }

        if (this.adminModulos.btnEliminar == e.getSource()){
            delete();
            deleteData();
        }
    }

    /*====================
    BORRAR DATOS DE TABLA
    ====================*/
    protected void deleteData() {
        for (
                int i = this.modulosTable.getRowCount() - 1;
                i >= 0;
                i--
        ) {this.modulosTable.removeRow(i); this.modulosTableShow.removeRow(i);}
    }

    /*===================
    CREAR
    ===================*/
    protected void create() {
        edicionModulos edMd = new edicionModulos();
        controllerMenuEditar_Crear_Modulos editarCrear = new controllerMenuEditar_Crear_Modulos(
                edMd,
                "C",
                "",
                0
        );

        edMd.framePrimario.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
                Frame frame = (Frame) e.getSource();
                refreshTable();
            }
        });

        edMd.framePrimario.setVisible(true);
    }

    /*====================
    EDITAR
    ====================*/
    protected void edit() {
        edicionModulos edMd = new edicionModulos();
        controllerMenuEditar_Crear_Modulos editarCrear = new controllerMenuEditar_Crear_Modulos(
                edMd,
                "E",
                this.modulosTable.getValueAt(
                        this.adminModulos.tableModulos.getSelectedRow(),
                        1
                )
                        .toString(),
                Integer.parseInt(
                        this.modulosTable.getValueAt(
                                this.adminModulos.tableModulos.getSelectedRow(),
                                0
                        )
                                .toString()
                )
        );

        edMd.framePrimario.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
//        super.windowDeactivated(e);
                Frame frame = (Frame) e.getSource();
                refreshTable();
            }
        });

        edMd.framePrimario.setVisible(true);
    }

    /*=====================
    BORRAR
    ====================*/
    protected void delete() {
        try {
            this.conn = new connection();
            conn.deleteModulo(
                    Integer.parseInt(this.modulosTable.getValueAt(
                            this.adminModulos.tableModulos.getSelectedRow(),
                            0
                    ).toString())
            );

            refreshTable();

        } catch (Exception ex) {
            if (conn != null) {
                try {
                    conn.getConn().rollback();
                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                    JOptionPane.showMessageDialog(null,sqlException.toString());
                }
            }
            JOptionPane.showMessageDialog(null, ex.toString());
        }
    }

    /*=====================
    BUSCAR CON TXT VACIO
    ====================*/
    protected void searchEmpty(){
        try {
            conn = new connection();
            ResultSet res = conn.crudModulos(2, 0, "");
            while (res.next()) {
                rowStrings[0] = res.getString(1);
                rowStrings[1] = res.getString(2);
                rowStringsShow[0]= res.getString(2);
                this.modulosTable.addRow(rowStrings);
                this.modulosTableShow.addRow(rowStringsShow);
            }
            res.close();
            conn.closeConnection();
            this.adminModulos.tableModulos.setModel(modulosTableShow);
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(
                    null,
                    "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
            );
            //                    exc.printStackTrace();
        }
    }

    /*=============================
    BUSCAR CON UN VALOR EN EL TXT
    =============================*/
    protected void searchNotEmpty(){
        try {
            conn = new connection();
            ResultSet res = conn.crudModulos(5, 0,
                    this.adminModulos.txtModulo.getText()
            );
            if (res == null) {
                JOptionPane.showMessageDialog(
                        null,
                        "Error, el módulo buscado no existe."
                );
            } else {
                while (res.next()) {
                    rowStrings[0] = res.getString(1);
                    rowStrings[1] = res.getString(2);
                    rowStringsShow[0]= res.getString(2);
                    this.modulosTable.addRow(rowStrings);
                    this.modulosTableShow.addRow(rowStringsShow);
                }
            }
            res.close();
            conn.closeConnection();
            this.adminModulos.tableModulos.setModel(modulosTableShow);
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(
                    null,
                    "Ha ocurrido un error. \n Detalle del error: " + exc.toString()
            );
            //                   exc.printStackTrace();
        }
    }

    protected void refreshTable(){
        deleteData();
        searchEmpty();
    }
}
