package controller.controllerLogin;
//import com.sun.xml.internal.ws.api.message.ExceptionHasMessage;
import controller.Main;
import controller.controllerMenus.controllerMenuPrincipal;
import controller.viewsController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.swing.*;

//import jdk.nashorn.internal.scripts.JO;
import model.connection.*;
import model.userData.Data;
import view.login.*;
import view.menu.menuPrincipal;

//here basicalle if the login is true, then hide the login menu, verify which kind of user is logged in (getting the access
// level and setting it to a "session variable" which is in model.userData.Data)
//
public class controllerLogin implements ActionListener {
  login login;

  /*============================================================================
  AQUI CREO 2 CONEXIONES DEBIDO A QUE LAS USO PARA DIFERENTES STORED PROCEDURES
  ============================================================================*/
  private connection conn, conn2;

  public controllerLogin(login login) {
    this.login = login;
    login.btnLogin.addActionListener(this);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (login.btnLogin == e.getSource()) {
      try {
        conn = new connection();
        conn2 = new connection();
        String pass, user;
        try {
          user = new String(login.txtUser.getText());
          pass = new String(login.txtPass.getPassword());
          try {
            ResultSet result = conn.verifyUser(
              6,
              0,
              user,
              "null",
              "null",
              "null",
              "null",
              0,
              pass,
              0
            );


            String[] tArray;
            result.next();
            tArray = new String[6];
            for (int i = 0; i < 6; i++) tArray[i] = result.getString(i + 1);

            Data data = new Data(
                    result.getInt(1),
                    result.getString(4),
                    result.getString(2),
                    result.getString(3),
                    result.getString(5),
                    result.getInt(6),
                    result.getString(7)

            );
            /*==================================================================
            OBTENER LOS PERMISOS (MODULOS) ASIGNADOS SEGUN EL ROL DEL USUARIO
            ==================================================================*/
            try{
              ResultSet res2= conn2.getAccessLvl(6, 0, result.getInt(6));
              while(res2.next()){
                Data.setAccess(res2.getInt(1), res2.getString(2));
              }
              res2.close();
              conn2.closeConnection();

            }catch (SQLException ex){
              JOptionPane.showMessageDialog(null, ex.toString());
            }

            result.close();
            conn.closeConnection();

//            System.out.println(Data.getAccessHashMap());
            this.login.frameLogin.dispose();
            this.startMenuPrincipal();

          } catch (Exception exx) {
//            exx.printStackTrace();
            JOptionPane.showMessageDialog(
              null,
              "Error, datos no registrados! "/* + exx.toString()*/
            );
          }
        } catch (Exception ex) {
          JOptionPane.showMessageDialog(
            null,
            "Error, en el tipo de datos ingresado! " + ex.getMessage()
          );
          user = "0";
        }
        conn.closeConnection();
      } catch (SQLException | ClassNotFoundException ex) {
//        ex.printStackTrace();
        JOptionPane.showMessageDialog(null, ex.toString());
      }
    }
  }

  public void startMenuPrincipal() {
    login.frameLogin.setVisible(false);
    menuPrincipal mPrincipal = new menuPrincipal();
    controllerMenuPrincipal contMPrincipal = new controllerMenuPrincipal(
      mPrincipal
    );
    mPrincipal.framePrincipal.setVisible(true);
  }
}
