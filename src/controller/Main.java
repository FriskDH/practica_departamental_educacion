package controller;

import com.formdev.flatlaf.FlatLightLaf;
import controller.viewsController;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;

import view.menu.*;

public class Main {

  public static void main(String[] args) throws MalformedURLException {
    FlatLightLaf.install();
    viewsController views = new viewsController();
  }
}
