package controller.scanner;

import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import java.awt.*;
import java.beans.PropertyChangeSupport;
import java.sql.SQLException;
import javax.swing.*;

import controller.controllerVisitas.controllerVisitas;
import model.connection.*;
import net.sf.jasperreports.components.iconlabel.IconTextFieldFactory;
import view.menu.Visitas;

public class fingerPrint {
  private DPFPCapture detectDevice = DPFPGlobal
    .getCaptureFactory()
    .createCapture();
  private DPFPEnrollment saveFingerP = DPFPGlobal
    .getEnrollmentFactory()
    .createEnrollment();
  private DPFPVerification verifyFingerP = DPFPGlobal
    .getVerificationFactory()
    .createVerification();
  private DPFPTemplate fingerTemplate;
  private byte[] bFingerP;
  public String template = "template";
  private int opc, id, mun;
  private String dpi, tel;
  private String nombre1, nombre2, apellido1, apellido2;

  public DPFPFeatureSet readNsaveFinger;
  connection conn;

  /*========================================================
  EMPIEZA EL PROCESO PARA REGISTRAR A UNA NUEVA PERSONA
  ========================================================*/
  public void start() {
    detectDevice.addDataListener(
            new DPFPDataAdapter() {

              public void dataAcquired(final DPFPDataEvent e) {
                try {
                  processNewFingerPrint(e.getSample());
                } catch (Exception exc) {
                  JOptionPane.showMessageDialog(null, "Error! " + exc.getMessage());
                }
              }
            }
    );

    detectDevice.addReaderStatusListener(
            new DPFPReaderStatusAdapter() {

              public void readerConnected(final DPFPReaderStatusEvent e) {}

              public void readerDisconnected(final DPFPReaderStatusEvent e) {
                JOptionPane.showMessageDialog(null, "Error, lector no activado");
                Visitas.lblNoHuella.setVisible(false);
                stopPrinting();
              }
            }
    );

    detectDevice.addErrorListener(
            new DPFPErrorAdapter() {

              public void errorReader(final DPFPErrorEvent e) {
                JOptionPane.showMessageDialog(null, "Error: " + e.getError());
              }
            }
    );
  }

  public fingerPrint() throws SQLException, ClassNotFoundException {
  }

  public void setData(
    int opc,
    int id,
    String dpi,
    String nombre1,
    String nombre2,
    String apellido1,
    String apellido2,
    int mun,
    String tel
  ) {
    this.apellido1 = apellido1;
    this.apellido2 = apellido2;
    this.nombre1 = nombre1;
    this.nombre2 = nombre2;
    this.opc = opc;
    this.id = id;
    this.dpi = dpi;
    this.mun = mun;
    this.tel= tel;
  }

  public void startPrinting() throws SQLException {
    detectDevice.startCapture();
    Visitas.lblNoHuella.setVisible(true);
  }

  public void stopPrinting() {
    detectDevice.stopCapture();
  }

  public boolean fingerPrintStatus() {
    Visitas.lblNoHuella.setText(
      "HUELLAS DACTILARES RESTANTES: " +
      Integer.toString(saveFingerP.getFeaturesNeeded())
    );
    if (saveFingerP.getFeaturesNeeded() == 0)return true;
    return false;
  }

  public void processNewFingerPrint(DPFPSample sample) throws SQLException, ClassNotFoundException {
    conn = new connection();
    readNsaveFinger =
      getFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);

    /*==============================================
    AQUI SE HACE LA INSERCION DE LA HUELLA
    ==============================================*/
    if (readNsaveFinger != null) {
      try {
        saveFingerP.addFeatures(readNsaveFinger);
        drawFingerPrint(createPrintImg(sample));
      } catch (DPFPImageQualityException e) {
        JOptionPane.showMessageDialog(null, e.getMessage());
      } finally {
        fingerPrintStatus();
        switch (saveFingerP.getTemplateStatus()) {
          case TEMPLATE_STATUS_READY:
            stopPrinting();
            setTemplate(saveFingerP.getTemplate());
            bFingerP = fingerTemplate.serialize();
            try {
              conn.visitantes(
                this.opc,
                this.id,
                this.dpi,
                bFingerP,
                this.nombre1,
                this.nombre2,
                this.apellido1,
                this.apellido2,
                this.mun,
                this.tel
              );
                conn.closeConnection();
                Image img = createPrintImg(sample);
                ImageIcon icc = drawFingerPrint(img);
                JOptionPane.showMessageDialog(
                        null,
                        "Huella Guardada Exitosamente",
                        "Huella",
                        JOptionPane.INFORMATION_MESSAGE,
                        icc
                );
                Visitas.lblNoHuella.setVisible(false);
                controllerVisitas.findByDPI2(this.dpi);

            } catch (Exception eee) {
              if (conn != null) {
                try {
                  conn.getConn().rollback();
                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                  JOptionPane.showMessageDialog(null,sqlException.toString());
                }
              }
              JOptionPane.showMessageDialog(null, "Error: " + eee.getMessage());
            }

            break;
          case TEMPLATE_STATUS_FAILED:
            saveFingerP.clear();
            stopPrinting();
            fingerPrintStatus();
            setTemplate(null);
            JOptionPane.showMessageDialog(
              null,
              "Error en el proceso, por favor repitalo"
            );
            break;
        }
      }
    }
  }

  public Image createPrintImg(DPFPSample sample) {
    return DPFPGlobal.getSampleConversionFactory().createImage(sample);
  }

  public ImageIcon drawFingerPrint(Image image) {
    ImageIcon ic = new ImageIcon(
      image.getScaledInstance(150, 150, Image.SCALE_DEFAULT)
    );
    return ic;
  }

  public void setTemplate(DPFPTemplate template) {
    DPFPTemplate old = this.fingerTemplate;
    this.fingerTemplate = template;
    PropertyChangeSupport pCh = new PropertyChangeSupport(this);
    pCh.firePropertyChange(this.template, old, template);
  }

  public DPFPFeatureSet getFeatures(DPFPSample sample, DPFPDataPurpose motive) {
    DPFPFeatureExtraction extracttor = DPFPGlobal
      .getFeatureExtractionFactory()
      .createFeatureExtraction();
    try {
      return extracttor.createFeatureSet(sample, motive);
    } catch (DPFPImageQualityException e) {
      JOptionPane.showMessageDialog(
        null,
        "Error al extraer caracteristicas: " + e.getMessage()
      );
      return null;
    }
  }

}
