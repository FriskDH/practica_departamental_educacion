package controller.scanner;

import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import com.digitalpersona.onetouch.verification.DPFPVerificationResult;
import controller.controllerVisitas.controllerVisitas;
import java.awt.*;
import java.beans.PropertyChangeSupport;
import java.sql.*;
import javax.swing.*;
import model.connection.*;
import model.personData.Data;

public class verifyFingerPrint {
  private DPFPVerification matcher = DPFPGlobal
    .getVerificationFactory()
    .createVerification();
  private DPFPCapture detectDevice = DPFPGlobal
    .getCaptureFactory()
    .createCapture();
  private DPFPEnrollment saveFingerP = DPFPGlobal
    .getEnrollmentFactory()
    .createEnrollment(); //reclutador
  private DPFPVerification verifyFingerP = DPFPGlobal
    .getVerificationFactory()
    .createVerification();
  private DPFPTemplate fingerTemplate; //template
  private byte[] bFingerP;
  public String template = "template";
  public DPFPFeatureSet readNverifyFinger; //featureSetVerification
  private DPFPTemplate tempTemplate; //template que toma la deserializacion de la data 'retrieved' de la bd
  connection conn, conn2;
  private ResultSet res2;

  public verifyFingerPrint() throws SQLException, ClassNotFoundException {
    matcher.setFARRequested(DPFPVerification.HIGH_SECURITY_FAR);
  }

  public void startPrinting() {
    detectDevice.startCapture();
  }

  public void stopPrinting() {
    detectDevice.stopCapture();
  }

  public Image createPrintImg(DPFPSample sample) {
    return DPFPGlobal.getSampleConversionFactory().createImage(sample);
  }

  public ImageIcon drawFingerPrint(Image image) {
    ImageIcon ic = new ImageIcon(
      image.getScaledInstance(50, 50, Image.SCALE_DEFAULT)
    );
    return ic;
  }

  public void setTemplate(DPFPTemplate template) {
    DPFPTemplate old = this.fingerTemplate;
    this.fingerTemplate = template;
    PropertyChangeSupport pCh = new PropertyChangeSupport(this);
    pCh.firePropertyChange(this.template, old, template);
  }

  public DPFPFeatureSet getFeatures(
    DPFPSample sample,
    DPFPDataPurpose purpose
  ) {
    DPFPFeatureExtraction extracttor = DPFPGlobal
      .getFeatureExtractionFactory()
      .createFeatureExtraction();
    try {
      return extracttor.createFeatureSet(sample, purpose);
    } catch (DPFPImageQualityException ex) {
      JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
      return null;
    }
  }

  public DPFPTemplate deserialize(byte[] bytee) {
    DPFPTemplate teeer = DPFPGlobal.getTemplateFactory().createTemplate();
    teeer.deserialize(bytee);
    return teeer;
  }

  public void processFingerPrint(DPFPSample sample) throws SQLException, ClassNotFoundException {
    conn = new connection();
    readNverifyFinger =getFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);
    ResultSet res = conn.selectFingers();
    DPFPVerificationResult yes_No_Verified;
    byte[] arrayByte;
    while (res.next()) {
      Blob dbBlobArray = res.getBlob(2);
      arrayByte = dbBlobArray.getBytes(1, (int) dbBlobArray.length());
      try {
        tempTemplate = deserialize(arrayByte);
        yes_No_Verified = matcher.verify(readNverifyFinger, tempTemplate);
        if (yes_No_Verified.isVerified()) {
          stopPrinting();
          conn2 = new connection();
          res2 = conn2.selectVisitanteData(res.getInt(1));
          res2.next();
          //System.out.println(res2.getString(1));
          Data d = new Data(
            res2.getInt(1),
            res2.getString(2),
            res2.getBlob(3),
            res2.getString(4),
            res2.getString(5),
            res2.getString(6),
            res2.getString(7),
            res2.getInt(9),
            res2.getInt(8),
            res2.getString(11),
            res2.getString(10),
            res2.getString(12)
          );
          controllerVisitas.fillWithData();
          controllerVisitas.enableFuncionOficina();
          res2.close();
          conn2.closeConnection();
        } else {
          stopPrinting();
        }
      } catch (SQLException | ClassNotFoundException e) {
        stopPrinting();
        JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
      }
    }
    res.close();
  }

  public void startVerification() {
    detectDevice.addDataListener(
      new DPFPDataAdapter() {

        public void dataAcquired(final DPFPDataEvent e) {
          try {
            processFingerPrint(e.getSample());
          } catch (Exception exc) {
            JOptionPane.showMessageDialog(null, "Error: " + exc.getMessage());
          }
        }
      }
    );

    detectDevice.addReaderStatusListener(
      new DPFPReaderStatusAdapter() {

        public void readerConnected(final DPFPReaderStatusEvent e) {}

        public void readerDisconnected(final DPFPReaderStatusEvent e) {
          JOptionPane.showMessageDialog(null, "Lector Desactivado!: ");
          stopPrinting();
        }
      }
    );

    detectDevice.addErrorListener(
      new DPFPErrorAdapter() {

        public void errorReader(final DPFPErrorEvent e) {
          JOptionPane.showMessageDialog(null, "Error:: " + e.getError());
        }
      }
    );
  }
}
