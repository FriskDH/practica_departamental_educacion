package controller.controllerFuncionarios;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

//import jdk.nashorn.internal.scripts.JO;
import model.connection.connection;
import view.menu.edicionUsuario;

public class controllerMenuEditar_Crear_Funcionarios implements ActionListener {
    edicionUsuario edicionUsuario;
    connection conn;
    DefaultTableModel tableFunciones;
    int idRol, idOficina;
    Long dpi;
    String nombre1, nombre2, apellido1, apellido2, oficina, rol;
    //CREACION DE LISTAS PARA COMPARAR PERMISOS
    List<Integer> listFuncionesAprobadas;
    List<Object> listSendNewFunciones;
    List<Object> listIndexSendNewFunciones;
    List<Integer> listIdOficinas;
    List<Integer> listIdRoles;
    Vector<Object> indexFuncionesOriginales;
    Vector<Object> funcionesCambiadas;
    Vector<Object> funcionesOriginales;

    public controllerMenuEditar_Crear_Funcionarios(
            edicionUsuario edicionUsuario,
            char option,
            Long dpi,
            String nombre1,
            String nombre2,
            String apellido1,
            String apellido2,
            String rol
    ) {
        this.edicionUsuario = edicionUsuario;
        this.dpi = dpi;
        this.nombre1 = nombre1;
        this.nombre2 = nombre2;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.rol = rol;

        listFuncionesAprobadas = new LinkedList<>();
        listIndexSendNewFunciones = new LinkedList<>();
        listSendNewFunciones = new LinkedList<>();
        listIdOficinas = new LinkedList<>();
        listIdRoles= new LinkedList<>();
        tableFunciones = new DefaultTableModel();
        tableFunciones =
                new DefaultTableModel(
                        new Object[][]{null},
                        new String[]{"No.", "Funcion", "Si/No"}
                ) {

                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        switch (colIndex) {
                            case 0:
                                return false;
                            case 1:
                                return false;
                            case 2:
                                return true;
                            default:
                                return false;
                        }
                    }

                    public Class<?> getColumnClass(int column) {
                        switch (column) {
                            case 0:
                                return String.class;
                            case 1:
                                return String.class;
                            case 2:
                                return Boolean.class;
                            default:
                                return String.class;
                        }
                    }
                };

        //EDITAR USUARIO
        if (option == 'E') {
            edicion();
        }

        //CREAR USUARIO
        if (option == 'C') {
            creacion();
        }
    }

    /*============================

    ============================*/
    protected void edicion() {
        this.edicionUsuario.btnCrear.setVisible(false);
        this.edicionUsuario.btnGuardar.addActionListener(this);
        this.edicionUsuario.txtDPI.setEnabled(false);
        this.edicionUsuario.txtDPI.setText(Long.toString(dpi));
        this.edicionUsuario.txtNombre1.setText(nombre1);
        this.edicionUsuario.txtNombre2.setText(nombre2);
        this.edicionUsuario.txtApellido1.setText(apellido1);
        this.edicionUsuario.txtApellido2.setText(apellido2);
        view.menu.edicionUsuario.lblRolActual.setText("Rol Actual: " + rol);

        //VECTOR PARA NOMBRE DE LA FUNCION(EN LA TABLA)
        Object[] rowFunciones = new Object[3];

        this.funcionesCambiadas = new Vector<>();
        this.funcionesOriginales = new Vector<>();
        this.indexFuncionesOriginales = new Vector<>();

        //OBTENER LAS OFICINAS
        try {
            conn = new connection();
            ResultSet res = conn.getOficinasInner();
            ResultSet res2 = conn.getFuncionarioOficina(this.dpi.toString());
//      System.out.println(this.dpi.toString());
//      res2.next();
            if (res2.next()) {
                this.idOficina = res2.getInt(1);
                this.edicionUsuario.lblOficinaActual.setText("Oficina Actual: "+res2.getString(2) + "(" + res2.getString(3) + ")");
            }

            while (res.next()) {
                this.listIdOficinas.add(res.getInt(1));
                this.edicionUsuario.cmbOficina.addItem(
                        res.getString(2) + "(" + res.getString(4) + ")"
                );
            }
            res.close();
            res2.close();
            conn.closeConnection();

            for (int i = 0; i < this.listIdOficinas.size();i++) {
                if (this.listIdOficinas.get(i) == this.idOficina) {
                    this.edicionUsuario.cmbOficina.setSelectedItem(
                            this.edicionUsuario.cmbOficina.getItemAt(i)
                    );
                }
            }
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            JOptionPane.showMessageDialog(null, throwables.toString());
        } catch (Exception e) {
//      e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.toString());
        }

        //OBTENER ROLES
        try {
            conn = new connection();
            ResultSet res = conn.crudRoles(2, 0, "");
            ResultSet res2 = conn.getFuncionarioRol(this.dpi.toString());
            res2.next();
            int idRolFunc= res2.getInt(1);
            while (res.next()) {
                listIdRoles.add(res.getInt(1));
                this.edicionUsuario.cmbRol.addItem(
                        /*res.getString(1) + " | " + */res.getString(2)
                );
            }
            res.close();
            res2.close();
            conn.closeConnection();
            for (int i = 0; i <= this.edicionUsuario.cmbRol.getItemCount() - 1; i++) {
                if (this.listIdRoles.get(i) == idRolFunc) {
                    this.edicionUsuario.cmbRol.setSelectedItem(this.edicionUsuario.cmbRol.getItemAt(i)
                    );
                }
            }
        } catch (SQLException | ClassNotFoundException throwables) {
            JOptionPane.showMessageDialog(null, throwables.toString());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }

        //OBTENER FUNCIONES
        try {
            conn = new connection();
            ResultSet res = conn.getFunciones();
            while (res.next()) {
                rowFunciones[0] = res.getString(1);
                rowFunciones[1] = res.getString(2);
                rowFunciones[2] = false;
                tableFunciones.addRow(rowFunciones);
            }
            res.close();
            conn.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }

        //OBTENER FUNCIONES APROBADAS Y ALMACENARLAS EN LA LINKED LIST
        try {
            conn = new connection();
            ResultSet res = conn.getFuncionesFuncionario(
                    6,
                    this.edicionUsuario.txtDPI.getText(),
                    0,
                    0
            );
            while (res.next()) {
//                System.out.println(res.getInt(1));
                listFuncionesAprobadas.add(res.getInt(1));
            }
            res.close();
            conn.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }

        tableFunciones.removeRow(0);

        //CICLO PARA AGREGAR LOS PERMISOS OBTENIDOS DE LA BD A LA TABLA
        for (int i = 0; i <= tableFunciones.getRowCount() - 1; i++) {
            for (int j = 0; j <= listFuncionesAprobadas.size() - 1; j++) {
                if (Integer.parseInt(tableFunciones.getValueAt(i, 0).toString()) == listFuncionesAprobadas.get(j)) {
                    tableFunciones.setValueAt(true, i, 2);
                }
            }
        }

        this.edicionUsuario.tableFuncionees.setModel(tableFunciones);
        this.edicionUsuario.tableFuncionees.getColumnModel().getColumn(0).setPreferredWidth(50);
        this.edicionUsuario.tableFuncionees.getColumnModel().getColumn(1).setPreferredWidth(400);
        this.edicionUsuario.tableFuncionees.getColumnModel().getColumn(2).setPreferredWidth(50);

        //CICLO PARA AGREGAR LOS PERMISOS ACTUALES DEL FUNCIONARIO A LA LISTA
        for (int i = 0; i <= tableFunciones.getRowCount() - 1; i++) {
            indexFuncionesOriginales.add(tableFunciones.getValueAt(i, 0));
            funcionesOriginales.add(tableFunciones.getValueAt(i, 2));
        }
    }

    /*============================
    CREAR
    ============================*/
    protected void creacion() {
        this.edicionUsuario.btnGuardar.setVisible(false);
        this.edicionUsuario.btnCrear.addActionListener(this);
        try {
            conn = new connection();
            ResultSet res = conn.getOficinasInner();
            ResultSet res2 = conn.getFuncionarioOficina(this.dpi.toString());

            if (res2.next()) {
                this.idOficina = res2.getInt(1);
                this.edicionUsuario.lblOficinaActual.setText("Oficina Actual: "+res2.getString(2) + "(" + res2.getString(3) + ")");
            }
            while (res.next()) {
                this.listIdOficinas.add(res.getInt(1));
                this.edicionUsuario.cmbOficina.addItem(res.getString(2) + "(" + res.getString(4) + ")");
            }
            res.close();
            res2.close();
            conn.closeConnection();
        } catch (Exception throwables) {
            JOptionPane.showMessageDialog(null, throwables.toString());
        }//            e.printStackTrace();


        //OBTENER ROLES
        try {
            conn = new connection();
            ResultSet res = conn.crudRoles(2, 0, "");

            while (res.next()) {
                listIdRoles.add(res.getInt(1));
                this.edicionUsuario.cmbRol.addItem(/*res.getString(1) + " | " + */res.getString(2));
            }
            res.close();
            conn.closeConnection();
            for (int i = 0; i <= this.edicionUsuario.cmbRol.getItemCount() - 1; i++) {
                if (this.edicionUsuario.cmbRol.getItemAt(i).equals(oficina)) {
                    this.edicionUsuario.cmbRol.setSelectedItem(
                            this.edicionUsuario.cmbRol.getItemAt(i)
                    );
                }
            }
        } catch (SQLException | ClassNotFoundException throwables) {
            JOptionPane.showMessageDialog(null, throwables.toString());
        }

        Object[] rowFunciones = new Object[3];

        //OBTENER FUNCIONES
        try {
            conn = new connection();
            ResultSet res = conn.getFunciones();
            while (res.next()) {
                rowFunciones[0] = res.getString(1);
                rowFunciones[1] = res.getString(2);
                rowFunciones[2] = false;
                tableFunciones.addRow(rowFunciones);
            }
            res.close();
            conn.closeConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }

        tableFunciones.removeRow(0);
        this.edicionUsuario.tableFuncionees.setModel(tableFunciones);
        this.edicionUsuario.tableFuncionees.getColumnModel().getColumn(0).setPreferredWidth(50);
        this.edicionUsuario.tableFuncionees.getColumnModel().getColumn(1).setPreferredWidth(400);
        this.edicionUsuario.tableFuncionees.getColumnModel().getColumn(2).setPreferredWidth(50);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    /*===============
    EDITAR
    ===============*/
        if (this.edicionUsuario.btnGuardar == e.getSource()) {
            boolean verificationOficina = false;
            boolean verificationNewFunciones = false;
            boolean verificationFunciones = false;
            this.funcionesCambiadas.clear();
            this.listIndexSendNewFunciones.clear();
            this.listSendNewFunciones.clear();

            //CICLO PARA AGREGAR LOS PERMISOS NUEVOS DEL FUNCIONARIO A LA LISTA
            for (int i = 0; i <= tableFunciones.getRowCount() - 1; i++) {
                this.funcionesCambiadas.add(
                        this.edicionUsuario.tableFuncionees.getValueAt(i, 2)
                );
            }

            for (int i = 0; i <= indexFuncionesOriginales.size() - 1; i++) {
                if (funcionesOriginales.get(i) != funcionesCambiadas.get(i)) {
                    this.listIndexSendNewFunciones.add(indexFuncionesOriginales.get(i));
                    this.listSendNewFunciones.add(this.funcionesCambiadas.get(i));
                }
            }

//            Iterator it=this.listIndexSendNewFunciones.iterator();
//            while (it.hasNext()) System.out.println(it.next());

            //CICLO PARA INSERTAR/BORRAR
            for (int i = 0; i <= this.listIndexSendNewFunciones.size() - 1; i++) {
                //INSERTAR FUNCIONES NUEVAS
                if (this.listSendNewFunciones.get(i).toString().equals("true")) {
                    try {
                        conn = new connection();
                        verificationNewFunciones =
                                conn.setFuncionesFuncionarios(
                                        1,
                                        0,
                                        Integer.parseInt(
                                                this.listIndexSendNewFunciones.get(i).toString()
                                        ),
                                        this.edicionUsuario.txtDPI.getText()
                                );

                        conn.closeConnection();
                    } catch (Exception throwables) {
                        if (conn != null) {
                            try {
                                conn.getConn().rollback();
                            } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                                JOptionPane.showMessageDialog(null,sqlException.toString());
                            }
                        }
                        JOptionPane.showMessageDialog(null, throwables.toString());
                    }
                }
                //BORRAR FUNCIONES
                else {
                    try {
                        conn = new connection();
                        verificationFunciones =
                                conn.setFuncionesFuncionarios(
                                        5,
                                        0,
                                        Integer.parseInt(
                                                this.listIndexSendNewFunciones.get(i).toString()
                                        ),
                                        this.edicionUsuario.txtDPI.getText()
                                );
                        conn.closeConnection();
                    } catch (Exception throwables) {
                        if (conn != null) {
                            try {
                                conn.getConn().rollback();
                            } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                                JOptionPane.showMessageDialog(null,sqlException.toString());
                            }
                        }
                        JOptionPane.showMessageDialog(null, throwables.toString());
                    }
                }
            }

            //GUARDAR LA OFICINA Y ROL SELECCIONADOS
            try {

                int idOf= this.listIdOficinas.get(this.edicionUsuario.cmbOficina.getSelectedIndex());


                /*String[] idRoll = Objects
                        .requireNonNull(this.edicionUsuario.cmbRol.getSelectedItem())
                        .toString()
                        .split("|");

                String idRol = "";
                for (int i = 0; i <= idRoll.length; i++) {
                    if (idRoll[i].equals("|")) {
                        i = idRoll.length;
                    } else {
                        try {
                            Integer.parseInt(idRoll[i]);
                            idRol = idRol + (idRoll[i]);
                        } catch (Exception ignored) {
                        }
                    }

                }*/
                conn = new connection();

                conn.setOficinaFuncionarios(
                        7,
                        0,
                        this.edicionUsuario.txtDPI.getText(),
                        this.edicionUsuario.txtNombre1.getText(),
                        this.edicionUsuario.txtNombre2.getText(),
                        this.edicionUsuario.txtApellido1.getText(),
                        this.edicionUsuario.txtApellido2.getText(),
                        idOf,
                        listIdRoles.get(this.edicionUsuario.cmbRol.getSelectedIndex())
                );

                conn.closeConnection();
            } catch (Exception throwables) {
                if (conn != null) {
                    try {
                        conn.getConn().rollback();
                    } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                        JOptionPane.showMessageDialog(null,sqlException.toString());
                    }
                }
                JOptionPane.showMessageDialog(null, throwables.toString());
            }

            //GUARDAR LA PASSWORD EN CASO DE QUE EL TXT NO ESTE VACIO
            if (!new String(this.edicionUsuario.txtPassword.getPassword()).equals("")) {
                try {
                    conn = new connection();
                    conn.updateUserPass(
                            this.edicionUsuario.txtDPI.getText(),
                            new String(this.edicionUsuario.txtPassword.getPassword())
                    );

                    conn.closeConnection();
                } catch (Exception throwables) {
                    if (conn != null) {
                        try {
                            conn.getConn().rollback();
                        } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                            JOptionPane.showMessageDialog(null,sqlException.toString());
                        }
                    }
                    JOptionPane.showMessageDialog(null, throwables.toString());
                }
            }

            this.edicionUsuario.framePrimario.dispose();
        }

    /*================
    CREAR
    ================*/
        if (this.edicionUsuario.btnCrear == e.getSource()) {
            boolean verificationNewFunciones = false;
            boolean verificationFunciones = false;
            this.listIndexSendNewFunciones.clear();
            this.listSendNewFunciones.clear();

            //CREAR USUARIO
            if (verifyFields()) {
                try {
                    conn = new connection();
                    /*
                    String[] idOf = Objects
                            .requireNonNull(this.edicionUsuario.cmbOficina.getSelectedItem())
                            .toString()
                            .split("|");

                     */

                    int idOf= this.listIdOficinas.get(this.edicionUsuario.cmbOficina.getSelectedIndex());

                    String[] idRol = Objects
                            .requireNonNull(this.edicionUsuario.cmbRol.getSelectedItem())
                            .toString()
                            .split("|");

                    conn.createUser(
                            1,
                            0,
                            this.edicionUsuario.txtDPI.getText(),
                            this.edicionUsuario.txtNombre1.getText(),
                            this.edicionUsuario.txtNombre2.getText(),
                            this.edicionUsuario.txtApellido1.getText(),
                            this.edicionUsuario.txtApellido2.getText(),
                            idOf,
                            new String(this.edicionUsuario.txtPassword.getPassword()),
                            listIdRoles.get(this.edicionUsuario.cmbRol.getSelectedIndex())
                    );

                    conn.closeConnection();
                } catch (SQLException | ClassNotFoundException throwables) {
                    if (conn != null) {
                        try {
                            conn.getConn().rollback();
                        } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                            JOptionPane.showMessageDialog(null,sqlException.toString());
                        }
                    }
                    JOptionPane.showMessageDialog(null, throwables.toString());
                }

                for (int i = 0; i <= this.tableFunciones.getRowCount() - 1; i++) {
                    //INSERTAR FUNCIONES NUEVAS
                    if (
                            this.edicionUsuario.tableFuncionees.getValueAt(i, 2)
                                    .toString()
                                    .equals("true")
                    ) {
                        try {
                            conn = new connection();
                            verificationNewFunciones =
                                    conn.setFuncionesFuncionarios(
                                            1,
                                            0,
                                            Integer.parseInt(
                                                    this.tableFunciones.getValueAt(i, 0).toString()
                                            ),
                                            this.edicionUsuario.txtDPI.getText()
                                    );

                            conn.closeConnection();
                        } catch (Exception throwables) {
                            if (conn != null) {
                                try {
                                    conn.getConn().rollback();
                                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                                    JOptionPane.showMessageDialog(null,sqlException.toString());
                                }
                            }
                            JOptionPane.showMessageDialog(null, throwables.toString());
                        }
                    }
                }
                this.edicionUsuario.framePrimario.dispose();
            } else {
                JOptionPane.showMessageDialog(
                        null,
                        "Todos los campos son requeridos, y se debe asignar al menos una función"
                );
            }
        }
    }

    protected boolean verifyFields() {
        int tempCont = 0;
        for (
                int i = 0;
                i <= this.edicionUsuario.tableFuncionees.getRowCount() - 1;
                i++
        ) {
            if (
                    this.edicionUsuario.tableFuncionees.getValueAt(i, 2)
                            .toString()
                            .equals("true")
            ) {
                tempCont += 1;
            }
        }

        if (
                this.edicionUsuario.txtNombre1.getText().equals("") ||
                        this.edicionUsuario.txtNombre2.getText().equals("") ||
                        this.edicionUsuario.txtApellido1.getText().equals("") ||
                        this.edicionUsuario.txtApellido2.getText().equals("") ||
                        this.edicionUsuario.txtDPI.getText().equals("") ||
                        String
                                .valueOf(this.edicionUsuario.txtPassword.getPassword())
                                .equals("") ||
                        (tempCont == 0)
        ) {
            return false;
        }
        return true;
    }
}
