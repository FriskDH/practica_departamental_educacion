package controller.controllerFuncionarios;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import model.connection.connection;
import view.menu.Funcionarios;
import view.menu.edicionUsuario;

public class controllerFuncionarios implements ActionListener {
    Funcionarios adminUsuarios;
    connection conn;
    connection conn2;
    DefaultTableModel usersTable;
    String[] rowStrings;
    String oficinaID, oficinaNombre;
    LinkedList<Integer> listOficinas;

    public controllerFuncionarios(Funcionarios adminUsuarios) {
        this.adminUsuarios = adminUsuarios;
        this.adminUsuarios.btnBuscar.addActionListener(this);
        this.adminUsuarios.btnCrear.addActionListener(this);
        this.adminUsuarios.btnEditar.addActionListener(this);
        this.adminUsuarios.btnEliminar.addActionListener(this);
        this.adminUsuarios.cmbOficina.addItem("0 | Todas");
        this.adminUsuarios.cmbOficina.setSelectedItem("0 | Todas");
        loadOficinas();
        rowStrings = new String[7];

        usersTable = new DefaultTableModel();
        usersTable =
                new DefaultTableModel(
                        new Object[][]{null},
                        new String[]{
                                "ID",
                                "P. NOMBRE",
                                "S. NOMBRE",
                                "P. APELLIDO",
                                "S. APELLIDO",
                                "OFICINA",
                                "ROL",
                        }
                ) {

                    //PARA EVITAR QUE SE PUEDE EDITAR LA TABLA
                    @Override
                    public boolean isCellEditable(int rowIndex, int colIndex) {
                        return false;
                    }
                };
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (this.adminUsuarios.btnBuscar == e.getSource()) {
            deleteData();
            //SEARCH ALL USERS WITHOUT ANY FILTER
            if ((this.adminUsuarios.txtDPI.getText().equals("")) && (this.adminUsuarios.cmbOficina.getSelectedItem() == "0 | Todas")) {
               searchEmpty();
            }

            //SEARCH ALL OFICINAS AND ALL FUNCIONES FILTERED ONLY BY DPI
            if ((!this.adminUsuarios.txtDPI.getText().equals("")) && (this.adminUsuarios.cmbOficina.getSelectedItem() == "0 | Todas")) {
                searchByDPI();
            }

            //SEARCH ALL FUNCIONES FILTERED BY OFICINA AND DPI
            if (!(this.adminUsuarios.txtDPI.getText().equals("")) && !(this.adminUsuarios.cmbOficina.getSelectedItem() == "0 | Todas")) {
                searchByOficinaAndDPI();
            }

            //SEARCH ONLY BY OFICINA
            if ((this.adminUsuarios.txtDPI.getText().equals("")) && !(this.adminUsuarios.cmbOficina.getSelectedItem() == "0 | Todas")) {
                searchByOficina();
            }
        }

        if (this.adminUsuarios.btnEditar == e.getSource()) {
            edit();
        }

        if (this.adminUsuarios.btnCrear == e.getSource()) {
            //            Object dpiSelectedthis= this.adminUsuarios.tableUsers.getValueAt(this.adminUsuarios.tableUsers.getSelectedRow(), 0);
            create();
        }

        if (this.adminUsuarios.btnEliminar == e.getSource()) {
            delete();
        }
    }

    protected void delete(){
        try {
            deleteUser();
            deleteUserFunc();
            JOptionPane.showMessageDialog(null, "Usuario borrado exitosamente");
            deleteData();
        } catch (SQLException | ClassNotFoundException throwables) {
            if (conn != null) {
                try {
                    conn.getConn().rollback();
                } catch (SQLException sqlException) {
//                        sqlException.printStackTrace();
                    JOptionPane.showMessageDialog(null,sqlException.toString());
                }
            }
            JOptionPane.showMessageDialog(null, "Error: " + throwables.toString());
        }
    }

    /*========================
    EDITAR
    =========================*/
    protected void edit(){
        edicionUsuario edicionUsuario = new edicionUsuario();
        try {
            Object dpiSelectedthis =
                    this.adminUsuarios.tableUsers.getValueAt(
                            this.adminUsuarios.tableUsers.getSelectedRow(),
                            0
                    );
            controllerMenuEditar_Crear_Funcionarios contEditarCrear = new controllerMenuEditar_Crear_Funcionarios(
                    edicionUsuario,
                    'E',
                    Long.valueOf(dpiSelectedthis.toString()),
                    this.adminUsuarios.tableUsers.getValueAt(
                            this.adminUsuarios.tableUsers.getSelectedRow(),
                            1
                    )
                            .toString(),
                    this.adminUsuarios.tableUsers.getValueAt(
                            this.adminUsuarios.tableUsers.getSelectedRow(),
                            2
                    )
                            .toString(),
                    this.adminUsuarios.tableUsers.getValueAt(
                            this.adminUsuarios.tableUsers.getSelectedRow(),
                            3
                    )
                            .toString(),
                    this.adminUsuarios.tableUsers.getValueAt(
                            this.adminUsuarios.tableUsers.getSelectedRow(),
                            4
                    )
                            .toString(),


                    this.adminUsuarios.tableUsers.getValueAt(
                            this.adminUsuarios.tableUsers.getSelectedRow(),
                            6
                    )
                            .toString()
            );
            contEditarCrear.edicionUsuario.framePrimario.setVisible(true);
            edicionUsuario.framePrimario.addWindowListener(new WindowAdapter(){
                @Override
                public void windowDeactivated(WindowEvent e) {
                    Frame frame = (Frame) e.getSource();
                    refreshTable();
                }
            });

        } catch (Exception exc) {
            JOptionPane.showMessageDialog(
                    null,
                    "Error, no se ha seleccionado un usuario "
            );
            JOptionPane.showMessageDialog(null, exc.toString());
        }
    }

    /*========================
    CREAR
    =========================*/
    protected void create(){
        edicionUsuario edicionUsuario = new edicionUsuario();
        controllerMenuEditar_Crear_Funcionarios contEditarCrear = new controllerMenuEditar_Crear_Funcionarios(
                edicionUsuario,
                'C',
                Long.valueOf(0),
                "",
                "",
                "",
                "",
                ""
        );
        contEditarCrear.edicionUsuario.framePrimario.setVisible(true);
        edicionUsuario.framePrimario.addWindowListener(new WindowAdapter(){
            @Override
            public void windowDeactivated(WindowEvent e) {
                Frame frame = (Frame) e.getSource();
                refreshTable();
            }
        });
    }


/*
  protected void loadFunciones() {
    try {
      conn = new connection();
      ResultSet res = conn.getFunciones();

      while (res.next()) {
        this.adminUsuarios.cmbFuncion.addItem(
            res.getString(1) + " | " + res.getString(2)
          );
      }
      res.close();
      conn.closeConnection();
    } catch (SQLException | ClassNotFoundException throwables) {
      JOptionPane.showMessageDialog(null, throwables.toString());
    }
  }*/

    /*=======================
    OBTENER OFICINAS
    ========================*/
    protected void loadOficinas() {
        this.listOficinas= new LinkedList<Integer>();
        try {
            conn = new connection();
            ResultSet res = conn.getOficinasInner();

            while (res.next()) {
                this.listOficinas.add(res.getInt(1));
                this.adminUsuarios.cmbOficina.addItem(
                        res.getString(2) + " (" +res.getString(4)+")"
                );
            }
            res.close();
            conn.closeConnection();
        } catch (SQLException | ClassNotFoundException throwables) {
            JOptionPane.showMessageDialog(null, throwables.toString());
        }
    }


    /*=======================
    BORRAR DATOS DE LA TABLA
    ========================*/
    protected void deleteData() {
        for (
                int i = this.usersTable.getRowCount() - 1;
                i >= 0;
                i--
        )
            this.usersTable.removeRow(i);
        this.oficinaID="";
        this.oficinaNombre="";
    }

    /*=====================
    BORRAR
    =====================*/
    protected void deleteUser() throws SQLException, ClassNotFoundException {
        try {
            this.conn = new connection();
            conn.deleteUser(
                    this.usersTable.getValueAt(
                            this.adminUsuarios.tableUsers.getSelectedRow(),
                            0
                    )
                            .toString()
            );
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }
        refreshTable();
    }

    /*===========================
    BORRAR FUNCIONES DEL USUARIO
    ===========================*/
    protected void deleteUserFunc() throws SQLException, ClassNotFoundException {
        try {
            this.conn = new connection();
            conn.deleteUserFunciones(
                    this.adminUsuarios.tableUsers.getValueAt(
                            this.adminUsuarios.tableUsers.getSelectedRow(),
                            0
                    )
                            .toString()
            );
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
    }

    /*=============================
    BUSCAR CUANDO TXT VACIO
    =============================*/
    protected void searchEmpty(){
        try {
            conn = new connection();
            ResultSet res = conn.getListaFuncionarios(1, "0", 0, 0);
            while (res.next()) {
                rowStrings[0] = res.getString(1);
                rowStrings[1] = res.getString(2);
                rowStrings[2] = res.getString(3);
                rowStrings[3] = res.getString(4);
                rowStrings[4] = res.getString(5);
                rowStrings[5] = res.getString(6);
                rowStrings[6] = res.getString(9);
                usersTable.addRow(rowStrings);
            }
            conn.closeConnection();
            this.adminUsuarios.tableUsers.setModel(usersTable);
        } catch (SQLException | ClassNotFoundException throwables) {
            JOptionPane.showMessageDialog(
                    null,
                    "Error: " + throwables.toString()
            );
        }
    }

    /*=============================
    BUSCAR POR DPI
    =============================*/
    protected void searchByDPI(){
        try {
            conn = new connection();
            ResultSet res = conn.getListaFuncionarios(
                    2,
                    this.adminUsuarios.txtDPI.getText(),
                    0,
                    0
            );
            while (res.next()) {
                rowStrings[0] = res.getString(1);
                rowStrings[1] = res.getString(2);
                rowStrings[2] = res.getString(3);
                rowStrings[3] = res.getString(4);
                rowStrings[4] = res.getString(5);
                rowStrings[5] = res.getString(6);
                rowStrings[6] = res.getString(9);
                usersTable.addRow(rowStrings);
            }
            conn.closeConnection();
            this.adminUsuarios.tableUsers.setModel(usersTable);
        } catch (SQLException | ClassNotFoundException throwables) {
            JOptionPane.showMessageDialog(
                    null,
                    "Error: " + throwables.toString()
            );
        }
    }

    /*=============================
    BUSCAR POR OFICINA Y DPI
    =============================*/
    protected void searchByOficinaAndDPI(){
        try {
            conn = new connection();
            ResultSet res = conn.getListaFuncionarios(
                    4,
                    this.adminUsuarios.txtDPI.getText(),
                    0,
                    this.listOficinas.get(this.adminUsuarios.cmbOficina.getSelectedIndex()+1)
            );
            while (res.next()) {
                rowStrings[0] = res.getString(1);
                rowStrings[1] = res.getString(2);
                rowStrings[2] = res.getString(3);
                rowStrings[3] = res.getString(4);
                rowStrings[4] = res.getString(5);
                rowStrings[5] = res.getString(6);
                rowStrings[6] = res.getString(9);
                usersTable.addRow(rowStrings);
            }
            conn.closeConnection();
            this.adminUsuarios.tableUsers.setModel(usersTable);
        } catch (SQLException | ClassNotFoundException throwables) {
            JOptionPane.showMessageDialog(
                    null,
                    "Error: " + throwables.toString()
            );
        }
    }

    /*=============================
    BUSCAR POR OFICINA
    =============================*/
    protected void searchByOficina(){
        try {
            conn = new connection();
            ResultSet res = conn.getListaFuncionarios(
                    5,
                    "0",
                    0,
                    this.listOficinas.get(this.adminUsuarios.cmbOficina.getSelectedIndex()-1)
            );

            while (res.next()) {
                rowStrings[0] = res.getString(1);
                rowStrings[1] = res.getString(2);
                rowStrings[2] = res.getString(3);
                rowStrings[3] = res.getString(4);
                rowStrings[4] = res.getString(5);
                rowStrings[5] = res.getString(6);
                rowStrings[6] = res.getString(9);
                usersTable.addRow(rowStrings);
            }
            conn.closeConnection();
            this.adminUsuarios.tableUsers.setModel(usersTable);
        } catch (SQLException | ClassNotFoundException throwables) {
            JOptionPane.showMessageDialog(
                    null,
                    "Error: " + throwables.toString()
            );
        }
    }

    /*=============================
    REFRESH TABLE
    =============================*/
    public void refreshTable(){
        deleteData();
        searchEmpty();
    }
}
