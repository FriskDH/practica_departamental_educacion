package model.connection;

import com.microsoft.sqlserver.jdbc.*;
//import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.*;
import java.math.BigInteger;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import javax.swing.*;
import javax.xml.transform.Result;

import model.userData.*;

public class connection {
    Connection conn;

    public connection() throws SQLException, ClassNotFoundException {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        String connectionUrl =
                "jdbc:sqlserver://SERVERDFMH2018:1433;" +
                        "databaseName=proyecto;" +
                        "user=sa;" +
                        "password=SQLdfmh131176;";
        /*String connectionUrl =
                "jdbc:sqlserver://LAPTOP-4VMH9TT6\\SQLEXPRESS:1433;" +
                        "databaseName=proyecto;" +
                        "user=root;" +
                        "password=toorbot123;";*/
                        

        try {
            conn = DriverManager.getConnection(connectionUrl);
        } catch (Exception ee) {
            System.out.println(ee);
        }
    }

    public Connection getConn() {
        return this.conn;
    }

    public void closeConnection() throws SQLException {
        this.conn.close();
    }

    public ResultSet getAccessLvl(
            int opc,
            int idModulo,
            int idRol
    )
            throws SQLException {
        String sql =
                "EXEC crudModulos_Roles @opc=?, @idModulo=?, @idRol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, idModulo);
        stmt.setInt(3, idRol);
        return stmt.executeQuery();
    }

    public ResultSet verifyUser(
            int opc,
            int id,
            String dpi,
            String nombre1,
            String nombre2,
            String apellido1,
            String apellido2,
            int oficina,
            String pass,
            int idRol
    )
            throws SQLException {
        String sql =
                "EXEC crudFuncionarios @opc=?,@id=?,@dpi=?,@nombre1=?,@nombre2=?,@apellido1=?,@apellido2=?,@oficina=?,@passProv=?,@idRol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, id);
        stmt.setLong(3, new Long(dpi));
        stmt.setString(4, nombre1);
        stmt.setString(5, nombre2);
        stmt.setString(6, apellido1);
        stmt.setString(7, apellido2);
        stmt.setInt(8, oficina);
        stmt.setString(9, pass);
        stmt.setInt(10, idRol);
        return stmt.executeQuery();
    }

    /*============================================
    CRUD USUARIOS
    ============================================*/
    public boolean createUser(
            int opc,
            int id,
            String dpi,
            String nombre1,
            String nombre2,
            String apellido1,
            String apellido2,
            int oficina,
            String pass,
            int rol
    )
            throws SQLException {
        String sql =
                "EXEC crudFuncionarios @opc=?,@id=?,@dpi=?,@nombre1=?,@nombre2=?,@apellido1=?,@apellido2=?,@oficina=?,@passProv=?,@idRol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, id);
        stmt.setLong(3, new Long(dpi));
        stmt.setString(4, nombre1);
        stmt.setString(5, nombre2);
        stmt.setString(6, apellido1);
        stmt.setString(7, apellido2);
        stmt.setInt(8, oficina);
        stmt.setString(9, pass);
        stmt.setInt(10, rol);
        return stmt.execute();
    }

    public boolean updateUserPass(String dpi, String pass) throws Exception {
        PreparedStatement stmt = conn.prepareStatement(
                "EXEC crudFuncionarios @opc=?,@id=?,@dpi=?,@nombre1=?,@nombre2=?,@apellido1=?,@apellido2=?,@oficina=?,@passProv=?, @idRol=?"
        );
        stmt.setInt(1, 8);
        stmt.setInt(2, 0);
        stmt.setLong(3, new Long(dpi));
        stmt.setString(4, "");
        stmt.setString(5, "");
        stmt.setString(6, "");
        stmt.setString(7, "");
        stmt.setString(8, "");
        stmt.setString(9, pass);
        stmt.setInt(10, 0);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public boolean deleteUser(String dpi) throws Exception {
        PreparedStatement stmt = conn.prepareStatement(
                "EXEC crudFuncionarios @opc=?,@id=?,@dpi=?,@nombre1=?,@nombre2=?,@apellido1=?,@apellido2=?,@oficina=?,@passProv=?,@idRol=?"
        );
        stmt.setInt(1, 4);
        stmt.setInt(2, 0);
        stmt.setLong(3, new Long(dpi));
        stmt.setString(4, "");
        stmt.setString(5, "");
        stmt.setString(6, "");
        stmt.setString(7, "");
        stmt.setString(8, "");
        stmt.setString(9, "");
        stmt.setInt(10, 0);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }


    }

    public boolean deleteUserFunciones(String dpi) throws Exception {
        PreparedStatement stmt = conn.prepareStatement(
                "EXEC crudFunciones_Funcionarios @opc=?,@id=?, @idFuncion=?, @dpiFuncionario=?"
        );
        stmt.setInt(1, 6);
        stmt.setInt(2, 0);
        stmt.setInt(3, 0);
        stmt.setLong(4, new Long(dpi));
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public ResultSet getListaFuncionarios(
            int opc,
            String dpi,
            int funcion,
            int oficina
    )
            throws SQLException {
        String sql = "EXEC adminUsuarios @opc=?, @dpi=?, @funcion=?, @oficina=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setLong(2, new Long(dpi));
        stmt.setInt(3, funcion);
        stmt.setInt(4, oficina);
        return stmt.executeQuery();
    }

    public ResultSet getFuncionarioOficina(String dpi) throws Exception{
        String sql =
                "EXEC crudFuncionarios @opc=?, @id=?, @dpi=?, @nombre1=?, @nombre2=?, @apellido1=?," +
                        "@apellido2=?, @oficina=?, @passProv=?, @idRol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 9);
        stmt.setInt(2, 0);
        stmt.setLong(3, new Long(dpi));
        stmt.setString(4, "");
        stmt.setString(5, "");
        stmt.setString(6, "");
        stmt.setString(7, "");
        stmt.setInt(8, 0);
        stmt.setString(9, "");
        stmt.setInt(10, 0);
        try {
            return stmt.executeQuery();
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public ResultSet getFuncionarioRol(String dpi) throws Exception{
        String sql =
                "SELECT fk_id_rol FROM funcionarios WHERE dpi=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setLong(1, new Long(dpi));
        try {
            return stmt.executeQuery();
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }


    public ResultSet getFuncionesFuncionario(
            int opc,
            String dpi,
            int funcion,
            int oficina
    )
            throws SQLException {
        String sql = "EXEC adminUsuarios @opc=?, @dpi=?, @funcion=?, @oficina=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setLong(2, new Long(dpi));
        stmt.setInt(3, funcion);
        stmt.setInt(4, oficina);
        return stmt.executeQuery();
    }

    public Boolean setFuncionesFuncionarios(
            int opc,
            int id,
            int funcion,
            String dpi
    )
            throws Exception {
        String sql =
                "EXEC crudFunciones_Funcionarios @opc=?, @id=?, @idFuncion=?, @dpiFuncionario=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, id);
        stmt.setLong(4, new Long(dpi));
        stmt.setInt(3, funcion);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public Boolean setOficinaFuncionarios(
            int opc,
            int id,
            String dpi,
            String nombre1,
            String nombre2,
            String apellido1,
            String apellido2,
            int oficina,
            int idRol
    )
            throws Exception {

        String sql =
                "EXEC crudFuncionarios @opc=?, @id=?, @dpi=?, @nombre1=?, @nombre2=?, @apellido1=?," +
                        "@apellido2=?, @oficina=?, @passProv=?, @idRol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, id);
        stmt.setLong(3, new Long(dpi));
        stmt.setString(4, nombre1);
        stmt.setString(5, nombre2);
        stmt.setString(6, apellido1);
        stmt.setString(7, apellido2);
        stmt.setInt(8, oficina);
        stmt.setString(9, "");
        stmt.setInt(10, idRol);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

  /*===========================================
  END CRUD USUARIOS
  ===========================================*/

    /*===========================================
    CRUD MODULOS
    ===========================================*/
    public ResultSet crudModulos(int opc, int id, String modulo) throws SQLException {
        String sql = "EXEC crudModulos @opc=?, @id=?, @modulo=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, id);
        stmt.setString(3, modulo);
        return stmt.executeQuery();
    }

    public boolean deleteModulo(int id) throws Exception {
        String sql = "EXEC crudModulos @opc=?, @id=?, @modulo=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 4);
        stmt.setInt(2, id);
        stmt.setString(3, "");
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public boolean createModulo(String modulo) throws Exception {
        String sql = "EXEC crudModulos @opc=?, @id=?, @modulo=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 1);
        stmt.setInt(2, 0);
        stmt.setString(3, modulo);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public boolean updateModulo(int id, String modulo) throws Exception {
        String sql = "EXEC crudModulos @opc=?, @id=?, @modulo=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 3);
        stmt.setInt(2, id);
        stmt.setString(3, modulo);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

  /*============================
  END CRUD MODULOS
  =============================*/

  /*============================
  CRUD ROLES
  =============================*/

    public ResultSet crudRoles(int opc, int id, String rol) throws SQLException {
        String sql = "EXEC crudRoles @opc=?, @id=?, @rol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, id);
        stmt.setString(3, rol);
        return stmt.executeQuery();
    }

    public boolean assignRolesToModulos(int opc, int idRol, int idModulo) throws Exception {
        String sql = "EXEC crudModulos_Roles @opc=?, @idModulo=?, @idRol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, idModulo);
        stmt.setInt(3, idRol);
        try {
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            throw new Exception(ex.toString());
        }
    }

    public boolean deleteRoles(int idRol) throws Exception {
        String sql = "EXEC crudRoles @opc=?, @id=?, @rol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 4);
        stmt.setInt(2, idRol);
        stmt.setString(3, "");
        try {
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            throw new Exception(ex.toString());
        }
    }

    public ResultSet getModulosRoles(int idRol) throws SQLException {
        String sql = "EXEC crudModulos_Roles @opc=?, @idModulo=?, @idRol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 6);
        stmt.setInt(2, 0);
        stmt.setInt(3, idRol);
        return stmt.executeQuery();
    }

    public boolean createRoles(String rol) throws Exception {
        String sql = "EXEC crudRoles @opc=?, @id=?, @rol=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 1);
        stmt.setInt(2, 0);
        stmt.setString(3, rol);
        try {
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            throw new Exception(ex.toString());
        }
    }

        public boolean updateRoles(int id, String rol) throws Exception {
            String sql = "EXEC crudRoles @opc=?, @id=?, @rol=?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            stmt.setInt(2, id);
            stmt.setString(3, rol);
            try {
                stmt.execute();
                return true;
            } catch (SQLException ex) {
                throw new Exception(ex.toString());
            }
    }
  /*============================
  END CRUD ROLES
  =============================*/

    public ResultSet selectFingers() throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(
                "select id, huella from visitantes;"
        );
        return stmt.executeQuery();
    }

    public ResultSet selectVisitanteData(int id) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(
                "EXEC crudVisitantes @opc=?, @id=?, @dpi=?, @huella=?, @nombre1=?, @nombre2=?, @apellido1=?, " +
                        "@apellido2=?, @idMun=?, @tel=?"
        );
        stmt.setInt(1, 6);
        stmt.setInt(2, id);
        stmt.setInt(3, 0);
        stmt.setInt(4, 0);
        stmt.setString(5, "");
        stmt.setString(6, "");
        stmt.setString(7, "");
        stmt.setString(8, "");
        stmt.setInt(9, 0);
        stmt.setString(10, "");
        return stmt.executeQuery();
    }

    public boolean visitantes(
            int opc,
            int id,
            String dpi,
            byte[] huella,
            String nombre1,
            String nombre2,
            String apellido1,
            String apellido2,
            int mun,
            String tel
    )
            throws SQLException {
//        System.out.println("conn" + mun);
        String sql =
                "EXEC crudVisitantes @opc=?,@id=?,@dpi=?,@huella=?,@nombre1=?,@nombre2=?,@apellido1=?,@apellido2=?,@idMun=?,@tel=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, id);
        stmt.setLong(3, new Long(dpi));
        stmt.setBlob(4, new ByteArrayInputStream(huella));
        stmt.setString(5, nombre1);
        stmt.setString(6, nombre2);
        stmt.setString(7, apellido1);
        stmt.setString(8, apellido2);
        stmt.setInt(9, mun);
        stmt.setString(10, tel);
        try {
            stmt.execute();
            stmt.close();
            return true;
        } catch (SQLException e) {
            if (e.getErrorCode() == 2627) {
//                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Error, el DPI ingresado ya existe.");
            } else {
                JOptionPane.showMessageDialog(null, e.getMessage());
                return false;
            }
            return false;
        }
    }

    public ResultSet buscarVisitantesDPI(
            int opc,
            int id,
            String dpi,
            int huella,
            String nombre1,
            String nombre2,
            String apellido1,
            String apellido2,
            int mun,
            String tel
    )
            throws SQLException {
        String sql =
                "EXEC crudVisitantes @opc=?,@id=?,@dpi=?,@huella=?,@nombre1=?,@nombre2=?,@apellido1=?,@apellido2=?,@idMun=?,@tel=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, id);
        stmt.setLong(3, new Long(dpi));
        stmt.setInt(4, huella);
        stmt.setString(5, nombre1);
        stmt.setString(6, nombre2);
        stmt.setString(7, apellido1);
        stmt.setString(8, apellido2);
        stmt.setInt(9, mun);
        stmt.setString(10, tel);
        return stmt.executeQuery();
    }

    public ResultSet getDptos() throws SQLException {
        String sql = "SELECT * FROM departamentos";
        PreparedStatement stmt = conn.prepareStatement(sql);
        return stmt.executeQuery();
    }

    public ResultSet getMun(int dpto) throws SQLException {
        String sql = "SELECT * FROM municipios WHERE fk_id_departamentos=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, dpto);
        return stmt.executeQuery();
    }

    public ResultSet getFunciones() throws SQLException {
        String sql = "SELECT * FROM funciones";
        PreparedStatement stmt = conn.prepareStatement(sql);
        return stmt.executeQuery();
    }


    /*=================================
    CRUD OFICINAS
    ==================================*/
    public ResultSet getOficinas(int funcion) throws SQLException {
        String sql =
                "EXEC crudFunciones_Oficinas @opc=?,@id=?,@idFuncion=?, @idOficina=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 5);
        stmt.setInt(2, 0);
        stmt.setInt(3, funcion);
        stmt.setInt(4, 0);
        return stmt.executeQuery();
    }

    public boolean createOficinas(String oficina, int idEdificio) throws Exception {
        String sql =
                "EXEC crudOficinas @opc=?,@id=?,@oficina=?, @idEdificio=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 1);
        stmt.setInt(2, 0);
        stmt.setString(3, oficina);
        stmt.setInt(4, idEdificio);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public boolean updateOficinas(int idOficina, String oficina, int idEdificio) throws Exception {
        String sql =
                "EXEC crudOficinas @opc=?,@id=?,@oficina=?, @idEdificio=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 3);
        stmt.setInt(2, idOficina);
        stmt.setString(3, oficina);
        stmt.setInt(4, idEdificio);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
//            e.printStackTrace();
            throw new Exception(e.toString());
        }
    }

    public boolean deleteOficinas(int idOficina) throws Exception {
        String sql =
                "EXEC crudOficinas @opc=?,@id=?,@oficina=?, @idEdificio=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 4);
        stmt.setInt(2, idOficina);
        stmt.setString(3, "");
        stmt.setInt(4, 0);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public ResultSet getOficinasList() throws SQLException {
        String sql = "SELECT oficinas.id, oficina, fk_id_edificio, edificio FROM oficinas inner join edificios on fk_id_edificio=edificios.id";
        PreparedStatement stmt = conn.prepareStatement(sql);
        return stmt.executeQuery();
    }

    public ResultSet getOficinas() throws SQLException {
        String sql = "SELECT * FROM oficinas";
        PreparedStatement stmt = conn.prepareStatement(sql);
        return stmt.executeQuery();
    }


    public ResultSet getOficinasInner() throws SQLException {
        String sql = "SELECT oficinas.id, oficina, fk_id_edificio, edificio FROM oficinas INNER JOIN edificios ON fk_id_edificio=edificios.id ";
        PreparedStatement stmt = conn.prepareStatement(sql);
        return stmt.executeQuery();
    }

    public ResultSet getOficinasByName(String oficina) throws SQLException {
        String sql = "SELECT oficinas.id, oficina, fk_id_edificio, edificio FROM oficinas inner join edificios on fk_id_edificio=edificios.id WHERE oficina=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, oficina);
        return stmt.executeQuery();
    }

    /*=================================
    END CRUD OFICINAS
    ==================================*/
  /*===============================
  CRUD FUNCIONES
  ===============================*/
    public ResultSet getFunciones(int funcion) throws SQLException {
        String sql =
                "EXEC crudFunciones @opc=?,@id=?, @funcion=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 5);
        stmt.setInt(2, 0);
        stmt.setInt(3, 0);
        return stmt.executeQuery();
    }

    public ResultSet getFuncionesByName(String funcion) throws SQLException {
        String sql =
                "EXEC crudFunciones @opc=?,@id=?, @funcion=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 6);
        stmt.setInt(2, 0);
        stmt.setString(3, "%"+funcion+"%");
        return stmt.executeQuery();
    }

    public boolean createFunciones(String funcion) throws Exception {
        String sql =
                "EXEC crudFunciones @opc=?,@id=?,@funcion=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 1);
        stmt.setInt(2, 0);
        stmt.setString(3, funcion);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public boolean updateFunciones(int idFuncion, String funcion) throws Exception {
        String sql =
                "EXEC crudFunciones @opc=?,@id=?,@funcion=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 3);
        stmt.setInt(2, idFuncion);
        stmt.setString(3, funcion);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public boolean deleteFunciones(int idFuncion) throws Exception {
        String sql =
                "EXEC crudFunciones @opc=?,@id=?,@funcion=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 4);
        stmt.setInt(2, idFuncion);
        stmt.setString(3, "");
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public boolean assignFuncionesToOficinas(int opc, int idFuncion, int idOficina) throws Exception {
        String sql =
                "EXEC crudFunciones_Oficinas @opc=?,@id=?,@idFuncion=?, @idOficina=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, 0);
        stmt.setInt(3, idFuncion);
        stmt.setInt(4, idOficina);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public ResultSet getFuncionesOficinas(int idFuncion, int idOficina) throws SQLException {
        String sql =
                "EXEC crudFunciones_Oficinas @opc=?,@id=?,@idFuncion=?, @idOficina=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 6);
        stmt.setInt(2, 0);
        stmt.setInt(3, idFuncion);
        stmt.setInt(4, idOficina);
        return stmt.executeQuery();
    }

  /*================================================
  END CRUD FUNCIONES
  ================================================*/

  /*================================================
  CRUD EDIFICIOS
  ================================================*/

    public boolean createEdificio(String edificio) throws Exception {
        String sql = "EXEC crudEdificios @opc=?, @id=?, @edificio=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 1);
        stmt.setInt(2, 0);
        stmt.setString(3, edificio);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public boolean deleteEdificio(int id) throws Exception {
        String sql = "EXEC crudEdificios @opc=?, @id=?, @edificio=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 4);
        stmt.setInt(2, id);
        stmt.setString(3, "");
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public boolean updateEdificio(int id, String edificio) throws Exception {
        String sql = "EXEC crudEdificios @opc=?, @id=?, @edificio=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 3);
        stmt.setInt(2, id);
        stmt.setString(3, edificio);
        try {
            stmt.execute();
            return true;
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    public ResultSet crudEdificios(int opc, int id, String edificio) throws SQLException {
        String sql = "EXEC crudEdificios @opc=?, @id=?, @edificio=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, opc);
        stmt.setInt(2, id);
        stmt.setString(3, edificio);
        return stmt.executeQuery();
    }

  /*================================================
  END CRUD EDIFICIOS
  ================================================*/

  /*======================================================================
  EXTRAS
  =======================================================================*/

    public ResultSet getRequerimientos(int funcion) throws SQLException {
        String sql =
                "EXEC curdFunciones_Requerimientos @opc=?, @id=?, @idFuncion=?, @idRequerimiento=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 5);
        stmt.setInt(2, 0);
        stmt.setInt(3, funcion);
        stmt.setInt(4, 0);
        return stmt.executeQuery();
    }

    public ResultSet getVisitasSegunVisitante(String dpi) throws SQLException {
        String sql =
                "EXEC crudVisitas @opc=?, @id=?, @dpi=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 2);
        stmt.setInt(2, 0);
        stmt.setLong(3, new Long(dpi));
        return stmt.executeQuery();
    }

    public boolean insertVisita(String dpi)
            throws Exception {
        String sql =
                "EXEC crudVisitas @opc=?, @id=?, @dpi=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 1);
        stmt.setInt(2, 0);
        stmt.setLong(3, new Long(dpi));
        try {
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            throw new Exception(ex.toString());
        }
    }


    public ResultSet getLastVisita() throws Exception{
        String sql= "EXEC crudVisitas @opc=?, @id=?, @dpi=?";
        PreparedStatement stmt= conn.prepareStatement(sql);
        stmt.setInt(1, 3);
        stmt.setInt(2, 0);
        stmt.setInt(3, 0);
        return stmt.executeQuery();
    }

    public boolean insertDetalleVisita(int idVisita, int funcion, int oficina)
            throws Exception {
        String sql =
                "EXEC crudDetalleVisitas @opc=?, @idVisita=?, @idOficina=?, @idFuncion=?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, 1);
        stmt.setInt(2, idVisita);
        stmt.setInt(3, oficina);
        stmt.setInt(4, funcion);
        try {
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            throw new Exception(ex.toString());
        }
    }

    /*==================================
    QUERYS PARA REPORTES
    =================================*/
    public ResultSet reportsByDate(String date) throws SQLException {
        /*String sql =
                "SELECT dpi, nombre1, nombre2, apellido1, apellido2, municipio, departamento, hora_entrada, oficina FROM detalle_visitas\n" +
                        "inner join visitas on fk_id_visitas=visitas.id " +
                        "inner join oficinas on fk_id_oficina=oficinas.id "+
                        "inner join visitantes on fk_dpi_visitantes=visitantes.dpi " +
                        "inner join municipios on fk_id_municipio=municipios.id " +
                        "inner join departamentos on fk_id_departamentos=departamentos.id " +
                        "WHERE fecha=? ORDER BY hora_entrada";*/
        String sql= "DECLARE @list_visitas TABLE \n" +
                "  ( \n" +
                "     dpi          BIGINT, \n" +
                "     nombre1      VARCHAR(50), \n" +
                "     nombre2      VARCHAR(50), \n" +
                "     apellido1    VARCHAR(50), \n" +
                "     apellido2    VARCHAR(50), \n" +
                "     municipio    VARCHAR(50), \n" +
                "     departamento VARCHAR(50), \n" +
                "     hora_entrada TIME(7), \n" +
                "     fecha        DATE, \n" +
                "     oficina      VARCHAR(max) \n" +
                "  ) \n" +
                "DECLARE @list_oficinas TABLE \n" +
                "  ( \n" +
                "     id      INT, \n" +
                "     oficina VARCHAR(250) \n" +
                "  ) \n" +
                "DECLARE @dpi          BIGINT, \n" +
                "        @nombre1      VARCHAR(50), \n" +
                "        @nombre2      VARCHAR(50), \n" +
                "        @apellido1    VARCHAR(50), \n" +
                "        @apellido2    VARCHAR(50), \n" +
                "        @municipio    VARCHAR(50), \n" +
                "        @departamento VARCHAR(50) \n" +
                "DECLARE @hora_entrada TIME(7) \n" +
                "DECLARE @fecha DATE \n" +
                "DECLARE @iterator_visitas INT \n" +
                "DECLARE @iterator_oficinas INT \n" +
                "DECLARE @temporal_id_visita INT \n" +
                "DECLARE @temporal_oficina VARCHAR(250) \n" +
                "\n" +
                "SET @iterator_visitas=1 \n" +
                "\n" +
                "DECLARE cursor_visitas CURSOR static read_only FOR \n" +
                "  SELECT dpi, \n" +
                "         nombre1, \n" +
                "         nombre2, \n" +
                "         apellido1, \n" +
                "         apellido2, \n" +
                "         municipio, \n" +
                "         departamento, \n" +
                "         hora_entrada, \n" +
                "         fecha, \n" +
                "         visitas.id \n" +
                "  FROM   visitas \n" +
                "         INNER JOIN visitantes \n" +
                "                 ON fk_dpi_visitantes = visitantes.dpi \n" +
                "         INNER JOIN municipios \n" +
                "                 ON fk_id_municipio = municipios.id \n" +
                "         INNER JOIN departamentos \n" +
                "                 ON fk_id_departamentos = departamentos.id \n" +
                "  WHERE  fecha =? \n" +
                "  ORDER  BY hora_entrada \n" +
                "\n" +
                "OPEN cursor_visitas \n" +
                "\n" +
                "WHILE @iterator_visitas <= (SELECT Count(*) \n" +
                "                            FROM   visitas WHERE fecha=?) \n" +
                "  BEGIN \n" +
                "      FETCH next FROM cursor_visitas INTO @dpi, @nombre1, @nombre2, @apellido1, \n" +
                "      @apellido2, @municipio, @departamento, @hora_entrada, @fecha, \n" +
                "      @temporal_id_visita \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      SET @iterator_oficinas=1 \n" +
                "\n" +
                "      DECLARE cursor_oficinas CURSOR static read_only FOR \n" +
                "        SELECT oficina \n" +
                "        FROM   detalle_visitas \n" +
                "               INNER JOIN oficinas \n" +
                "                       ON fk_id_oficina = oficinas.id \n" +
                "        WHERE  fk_id_visitas = @temporal_id_visita \n" +
                "\n" +
                "      OPEN cursor_oficinas \n" +
                "\n" +
                "      WHILE @iterator_oficinas <= (SELECT @@CURSOR_ROWS) \n" +
                "        BEGIN \n" +
                "            FETCH next FROM cursor_oficinas INTO @temporal_oficina \n" +
                "\n" +
                "            INSERT INTO @list_oficinas \n" +
                "            VALUES      (@iterator_oficinas, \n" +
                "                         @temporal_oficina) \n" +
                "\n" +
                "            SET @iterator_oficinas=@iterator_oficinas + 1 \n" +
                "        END \n" +
                "\n" +
                "      CLOSE cursor_oficinas \n" +
                "\n" +
                "      DEALLOCATE cursor_oficinas \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --END CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      DECLARE @string_oficinas VARCHAR(max) \n" +
                "      DECLARE @it INT \n" +
                "\n" +
                "      SET @it=1 \n" +
                "\n" +
                "      WHILE @it <= (SELECT Count(*) \n" +
                "                    FROM   @list_oficinas) \n" +
                "        BEGIN \n" +
                "            SET @string_oficinas=Concat(@string_oficinas, \n" +
                "                                 + ' ' \n" +
                "                                 + (SELECT oficina \n" +
                "                                    FROM   @list_oficinas \n" +
                "                                    WHERE  id = @it) \n" +
                "                                 + ',') \n" +
                "            SET @it=@it + 1 \n" +
                "        END \n" +
                "\n" +
                "      INSERT INTO @list_visitas \n" +
                "      VALUES      (@dpi, \n" +
                "                   @nombre1, \n" +
                "                   @nombre2, \n" +
                "                   @apellido1, \n" +
                "                   @apellido2, \n" +
                "                   @municipio, \n" +
                "                   @departamento, \n" +
                "                   @hora_entrada, \n" +
                "                   @fecha, \n" +
                "                   @string_oficinas ) \n" +
                "\n" +
                "      DELETE FROM @list_oficinas \n" +
                "\n" +
                "      SET @string_oficinas='' \n" +
                "      SET @iterator_visitas=@iterator_visitas + 1 \n" +
                "  END \n" +
                "\n" +
                "SELECT * \n" +
                "FROM   @list_visitas \n" +
                "\n" +
                "CLOSE cursor_visitas \n" +
                "\n" +
                "DEALLOCATE cursor_visitas ";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setDate(1, Date.valueOf(date));
        stmt.setDate(2, Date.valueOf(date));
        return stmt.executeQuery();
    }

    public ResultSet reportsBetweenDates(String dateBeginning, String dateEnding)
            throws SQLException {

/*
        String sql =
                "SELECT dpi, nombre1, nombre2, apellido1, apellido2, municipio, departamento, hora_entrada, fecha, oficina FROM detalle_visitas\n" +
                        "inner join visitas on fk_id_visitas=visitas.id " +
                        "inner join oficinas on fk_id_oficina=oficinas.id "+
                        "inner join visitantes on fk_dpi_visitantes=visitantes.dpi " +
                        "inner join municipios on fk_id_municipio=municipios.id " +
                        "inner join departamentos on fk_id_departamentos=departamentos.id " +
                        "WHERE  fecha>=? and fecha<=? ORDER BY fecha";

 */
        String sql= "DECLARE @list_visitas TABLE \n" +
                "  ( \n" +
                "     dpi          BIGINT, \n" +
                "     nombre1      VARCHAR(50), \n" +
                "     nombre2      VARCHAR(50), \n" +
                "     apellido1    VARCHAR(50), \n" +
                "     apellido2    VARCHAR(50), \n" +
                "     municipio    VARCHAR(50), \n" +
                "     departamento VARCHAR(50), \n" +
                "     hora_entrada TIME(7), \n" +
                "     fecha        DATE, \n" +
                "     oficina      VARCHAR(max) \n" +
                "  ) \n" +
                "DECLARE @list_oficinas TABLE \n" +
                "  ( \n" +
                "     id      INT, \n" +
                "     oficina VARCHAR(250) \n" +
                "  ) \n" +
                "DECLARE @dpi          BIGINT, \n" +
                "        @nombre1      VARCHAR(50), \n" +
                "        @nombre2      VARCHAR(50), \n" +
                "        @apellido1    VARCHAR(50), \n" +
                "        @apellido2    VARCHAR(50), \n" +
                "        @municipio    VARCHAR(50), \n" +
                "        @departamento VARCHAR(50) \n" +
                "DECLARE @hora_entrada TIME(7) \n" +
                "DECLARE @fecha DATE \n" +
                "DECLARE @iterator_visitas INT \n" +
                "DECLARE @iterator_oficinas INT \n" +
                "DECLARE @temporal_id_visita INT \n" +
                "DECLARE @temporal_oficina VARCHAR(250) \n" +
                "\n" +
                "SET @iterator_visitas=1 \n" +
                "\n" +
                "DECLARE cursor_visitas CURSOR static read_only FOR \n" +
                "  SELECT dpi, \n" +
                "         nombre1, \n" +
                "         nombre2, \n" +
                "         apellido1, \n" +
                "         apellido2, \n" +
                "         municipio, \n" +
                "         departamento, \n" +
                "         hora_entrada, \n" +
                "         fecha, \n" +
                "         visitas.id \n" +
                "  FROM   visitas \n" +
                "         INNER JOIN visitantes \n" +
                "                 ON fk_dpi_visitantes = visitantes.dpi \n" +
                "         INNER JOIN municipios \n" +
                "                 ON fk_id_municipio = municipios.id \n" +
                "         INNER JOIN departamentos \n" +
                "                 ON fk_id_departamentos = departamentos.id \n" +
                "  WHERE  fecha >=? \n" +
                "         AND fecha <=? \n" +
                "  ORDER  BY fecha \n" +
                "\n" +
                "OPEN cursor_visitas \n" +
                "\n" +
                "WHILE @iterator_visitas <= (SELECT Count(*) \n" +
                "                            FROM   visitas WHERE  fecha >=? AND fecha<=?) \n" +
                "  BEGIN \n" +
                "      FETCH next FROM cursor_visitas INTO @dpi, @nombre1, @nombre2, @apellido1, \n" +
                "      @apellido2, @municipio, @departamento, @hora_entrada, @fecha, \n" +
                "      @temporal_id_visita \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      SET @iterator_oficinas=1 \n" +
                "\n" +
                "      DECLARE cursor_oficinas CURSOR static read_only FOR \n" +
                "        SELECT oficina \n" +
                "        FROM   detalle_visitas \n" +
                "               INNER JOIN oficinas \n" +
                "                       ON fk_id_oficina = oficinas.id \n" +
                "        WHERE  fk_id_visitas = @temporal_id_visita \n" +
                "\n" +
                "      OPEN cursor_oficinas \n" +
                "\n" +
                "      WHILE @iterator_oficinas <= (SELECT @@CURSOR_ROWS) \n" +
                "        BEGIN \n" +
                "            FETCH next FROM cursor_oficinas INTO @temporal_oficina \n" +
                "\n" +
                "            INSERT INTO @list_oficinas \n" +
                "            VALUES      (@iterator_oficinas, \n" +
                "                         @temporal_oficina) \n" +
                "\n" +
                "            SET @iterator_oficinas=@iterator_oficinas + 1 \n" +
                "        END \n" +
                "\n" +
                "      CLOSE cursor_oficinas \n" +
                "\n" +
                "      DEALLOCATE cursor_oficinas \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --END CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      DECLARE @string_oficinas VARCHAR(max) \n" +
                "      DECLARE @it INT \n" +
                "\n" +
                "      SET @it=1 \n" +
                "\n" +
                "      WHILE @it <= (SELECT Count(*) \n" +
                "                    FROM   @list_oficinas) \n" +
                "        BEGIN \n" +
                "            SET @string_oficinas=Concat(@string_oficinas, \n" +
                "                                 + ' ' \n" +
                "                                 + (SELECT oficina \n" +
                "                                    FROM   @list_oficinas \n" +
                "                                    WHERE  id = @it) \n" +
                "                                 + ',') \n" +
                "            SET @it=@it + 1 \n" +
                "        END \n" +
                "\n" +
                "      INSERT INTO @list_visitas \n" +
                "      VALUES      (@dpi, \n" +
                "                   @nombre1, \n" +
                "                   @nombre2, \n" +
                "                   @apellido1, \n" +
                "                   @apellido2, \n" +
                "                   @municipio, \n" +
                "                   @departamento, \n" +
                "                   @hora_entrada, \n" +
                "                   @fecha, \n" +
                "                   @string_oficinas ) \n" +
                "\n" +
                "      DELETE FROM @list_oficinas \n" +
                "\n" +
                "      SET @string_oficinas='' \n" +
                "      SET @iterator_visitas=@iterator_visitas + 1 \n" +
                "  END \n" +
                "\n" +
                "SELECT * \n" +
                "FROM   @list_visitas \n" +
                "\n" +
                "CLOSE cursor_visitas \n" +
                "\n" +
                "DEALLOCATE cursor_visitas ";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setDate(1, Date.valueOf(dateBeginning));
        stmt.setDate(2, Date.valueOf(dateEnding));
        stmt.setDate(3, Date.valueOf(dateBeginning));
        stmt.setDate(4, Date.valueOf(dateEnding));
        return stmt.executeQuery();
    }

    public ResultSet reportsByOffice(int oficina, String date)
            throws SQLException {
        String sql =
                "SELECT dpi, nombre1, nombre2, apellido1, apellido2, municipio, departamento, hora_entrada, fecha, oficina FROM detalle_visitas\n" +
                        "inner join visitas on fk_id_visitas=visitas.id\n" +
                        "inner join oficinas on fk_id_oficina=oficinas.id "+
                        "inner join visitantes on fk_dpi_visitantes=visitantes.dpi\n" +
                        "inner join municipios on fk_id_municipio=municipios.id\n" +
                        "inner join departamentos on fk_id_departamentos=departamentos.id \n" +
                        "where fk_id_oficina=? and fecha=? ORDER BY fecha";
         /*
        String sql= "DECLARE @list_visitas TABLE \n" +
                "  ( \n" +
                "     dpi          BIGINT, \n" +
                "     nombre1      VARCHAR(50), \n" +
                "     nombre2      VARCHAR(50), \n" +
                "     apellido1    VARCHAR(50), \n" +
                "     apellido2    VARCHAR(50), \n" +
                "     municipio    VARCHAR(50), \n" +
                "     departamento VARCHAR(50), \n" +
                "     hora_entrada TIME(7), \n" +
                "     fecha        DATE, \n" +
                "     oficina      VARCHAR(max) \n" +
                "  ) \n" +
                "DECLARE @list_oficinas TABLE \n" +
                "  ( \n" +
                "     id      INT, \n" +
                "     oficina VARCHAR(250) \n" +
                "  ) \n" +
                "DECLARE @dpi          BIGINT, \n" +
                "        @nombre1      VARCHAR(50), \n" +
                "        @nombre2      VARCHAR(50), \n" +
                "        @apellido1    VARCHAR(50), \n" +
                "        @apellido2    VARCHAR(50), \n" +
                "        @municipio    VARCHAR(50), \n" +
                "        @departamento VARCHAR(50) \n" +
                "DECLARE @hora_entrada TIME(7) \n" +
                "DECLARE @fecha DATE \n" +
                "DECLARE @iterator_visitas INT \n" +
                "DECLARE @iterator_oficinas INT \n" +
                "DECLARE @temporal_id_visita INT \n" +
                "DECLARE @temporal_oficina VARCHAR(250) \n" +
                "\n" +
                "SET @iterator_visitas=1 \n" +
                "\n" +
                "DECLARE cursor_visitas CURSOR static read_only FOR \n" +
                "  SELECT dpi, \n" +
                "         nombre1, \n" +
                "         nombre2, \n" +
                "         apellido1, \n" +
                "         apellido2, \n" +
                "         municipio, \n" +
                "         departamento, \n" +
                "         hora_entrada, \n" +
                "         fecha, \n" +
                "         visitas.id \n" +
                "  FROM   detalle_visitas \n" +
                "         INNER JOIN visitas \n" +
                "                 ON fk_id_visitas = visitas.id \n" +
                "         INNER JOIN visitantes \n" +
                "                 ON fk_dpi_visitantes = visitantes.dpi \n" +
                "         INNER JOIN oficinas \n" +
                "                 ON fk_id_oficina = oficinas.id \n" +
                "         INNER JOIN municipios \n" +
                "                 ON fk_id_municipio = municipios.id \n" +
                "         INNER JOIN departamentos \n" +
                "                 ON fk_id_departamentos = departamentos.id \n" +
                "  WHERE  fk_id_oficina=? \n" +
                "  AND fecha=?" +
                "\n" +
                "OPEN cursor_visitas \n" +
                "\n" +
                "WHILE @iterator_visitas <= (SELECT Count(*) \n" +
                "                            FROM  detalle_visitas WHERE fk_id_oficina=? AND fecha=?) \n" +
                "  BEGIN \n" +
                "      FETCH next FROM cursor_visitas INTO @dpi, @nombre1, @nombre2, @apellido1, \n" +
                "      @apellido2, @municipio, @departamento, @hora_entrada, @fecha, \n" +
                "      @temporal_id_visita \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      SET @iterator_oficinas=1 \n" +
                "\n" +
                "      DECLARE cursor_oficinas CURSOR static read_only FOR \n" +
                "        SELECT oficina \n" +
                "        FROM   detalle_visitas \n" +
                "               INNER JOIN oficinas \n" +
                "                       ON fk_id_oficina = oficinas.id \n" +
                "        WHERE  fk_id_visitas = @temporal_id_visita \n" +
                "\n" +
                "      OPEN cursor_oficinas \n" +
                "\n" +
                "      WHILE @iterator_oficinas <= (SELECT @@CURSOR_ROWS) \n" +
                "        BEGIN \n" +
                "            FETCH next FROM cursor_oficinas INTO @temporal_oficina \n" +
                "\n" +
                "            INSERT INTO @list_oficinas \n" +
                "            VALUES      (@iterator_oficinas, \n" +
                "                         @temporal_oficina) \n" +
                "\n" +
                "            SET @iterator_oficinas=@iterator_oficinas + 1 \n" +
                "        END \n" +
                "\n" +
                "      CLOSE cursor_oficinas \n" +
                "\n" +
                "      DEALLOCATE cursor_oficinas \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --END CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      DECLARE @string_oficinas VARCHAR(max) \n" +
                "      DECLARE @it INT \n" +
                "\n" +
                "      SET @it=1 \n" +
                "\n" +
                "      WHILE @it <= (SELECT Count(*) \n" +
                "                    FROM   @list_oficinas) \n" +
                "        BEGIN \n" +
                "            SET @string_oficinas=Concat(@string_oficinas, \n" +
                "                                 + ' ' \n" +
                "                                 + (SELECT oficina \n" +
                "                                    FROM   @list_oficinas \n" +
                "                                    WHERE  id = @it) \n" +
                "                                 + ',') \n" +
                "            SET @it=@it + 1 \n" +
                "        END \n" +
                "\n" +
                "      INSERT INTO @list_visitas \n" +
                "      VALUES      (@dpi, \n" +
                "                   @nombre1, \n" +
                "                   @nombre2, \n" +
                "                   @apellido1, \n" +
                "                   @apellido2, \n" +
                "                   @municipio, \n" +
                "                   @departamento, \n" +
                "                   @hora_entrada, \n" +
                "                   @fecha, \n" +
                "                   @string_oficinas ) \n" +
                "\n" +
                "      DELETE FROM @list_oficinas \n" +
                "\n" +
                "      SET @string_oficinas='' \n" +
                "      SET @iterator_visitas=@iterator_visitas + 1 \n" +
                "  END \n" +
                "\n" +
                "SELECT * \n" +
                "FROM   @list_visitas \n" +
                "\n" +
                "CLOSE cursor_visitas \n" +
                "\n" +
                "DEALLOCATE cursor_visitas ";*/

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, oficina);
        stmt.setDate(2, Date.valueOf(date));
        //stmt.setDate(3, Date.valueOf(date));
        return stmt.executeQuery();
    }

    public ResultSet reportsBetweenDatesOffice(
            int oficina,
            String dateBeginning,
            String dateEnding
    )
            throws SQLException {

        String sql =
                "SELECT dpi, nombre1, nombre2, apellido1, apellido2, municipio, departamento, hora_entrada, fecha, oficina FROM detalle_visitas\n" +
                        "inner join visitas on fk_id_visitas=visitas.id\n" +
                        "inner join oficinas on fk_id_oficina=oficinas.id "+
                        "inner join visitantes on fk_dpi_visitantes=visitantes.dpi\n" +
                        "inner join municipios on fk_id_municipio=municipios.id\n" +
                        "inner join departamentos on fk_id_departamentos=departamentos.id \n" +
                        "where fk_id_oficina=? and fecha>=? and fecha<=? ORDER BY fecha";

        /*
        String sql= "DECLARE @list_visitas TABLE \n" +
                "  ( \n" +
                "     dpi          BIGINT, \n" +
                "     nombre1      VARCHAR(50), \n" +
                "     nombre2      VARCHAR(50), \n" +
                "     apellido1    VARCHAR(50), \n" +
                "     apellido2    VARCHAR(50), \n" +
                "     municipio    VARCHAR(50), \n" +
                "     departamento VARCHAR(50), \n" +
                "     hora_entrada TIME(7), \n" +
                "     fecha        DATE, \n" +
                "     oficina      VARCHAR(max) \n" +
                "  ) \n" +
                "DECLARE @list_oficinas TABLE \n" +
                "  ( \n" +
                "     id      INT, \n" +
                "     oficina VARCHAR(250) \n" +
                "  ) \n" +
                "DECLARE @dpi          BIGINT, \n" +
                "        @nombre1      VARCHAR(50), \n" +
                "        @nombre2      VARCHAR(50), \n" +
                "        @apellido1    VARCHAR(50), \n" +
                "        @apellido2    VARCHAR(50), \n" +
                "        @municipio    VARCHAR(50), \n" +
                "        @departamento VARCHAR(50) \n" +
                "DECLARE @hora_entrada TIME(7) \n" +
                "DECLARE @fecha DATE \n" +
                "DECLARE @iterator_visitas INT \n" +
                "DECLARE @iterator_oficinas INT \n" +
                "DECLARE @temporal_id_visita INT \n" +
                "DECLARE @temporal_oficina VARCHAR(250) \n" +
                "\n" +
                "SET @iterator_visitas=1 \n" +
                "\n" +
                "DECLARE cursor_visitas CURSOR static read_only FOR \n" +
                "  SELECT dpi, \n" +
                "         nombre1, \n" +
                "         nombre2, \n" +
                "         apellido1, \n" +
                "         apellido2, \n" +
                "         municipio, \n" +
                "         departamento, \n" +
                "         hora_entrada, \n" +
                "         fecha, \n" +
                "         visitas.id \n" +
                "  FROM   detalle_visitas \n" +
                "         INNER JOIN visitas \n" +
                "                 ON fk_id_visitas = visitas.id \n" +
                "         INNER JOIN visitantes \n" +
                "                 ON fk_dpi_visitantes = visitantes.dpi \n" +
                "         INNER JOIN oficinas \n" +
                "                 ON fk_id_oficina = oficinas.id \n" +
                "         INNER JOIN municipios \n" +
                "                 ON fk_id_municipio = municipios.id \n" +
                "         INNER JOIN departamentos \n" +
                "                 ON fk_id_departamentos = departamentos.id \n" +
                "  WHERE  fk_id_oficina=? \n" +
                "         AND fecha >=? \n" +
                "         AND fecha <=? \n" +
                "  ORDER  BY fecha \n" +
                "\n" +
                "OPEN cursor_visitas \n" +
                "\n" +
                "WHILE @iterator_visitas <= (SELECT Count(*) \n" +
                "                            FROM   visitas WHERE fk_id_oficina=? AND fecha>=? AND fecha<=?) \n" +
                "  BEGIN \n" +
                "      FETCH next FROM cursor_visitas INTO @dpi, @nombre1, @nombre2, @apellido1, \n" +
                "      @apellido2, @municipio, @departamento, @hora_entrada, @fecha, \n" +
                "      @temporal_id_visita \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      SET @iterator_oficinas=1 \n" +
                "\n" +
                "      DECLARE cursor_oficinas CURSOR static read_only FOR \n" +
                "        SELECT oficina \n" +
                "        FROM   detalle_visitas \n" +
                "               INNER JOIN oficinas \n" +
                "                       ON fk_id_oficina = oficinas.id \n" +
                "        WHERE  fk_id_visitas = @temporal_id_visita \n" +
                "\n" +
                "      OPEN cursor_oficinas \n" +
                "\n" +
                "      WHILE @iterator_oficinas <= (SELECT @@CURSOR_ROWS) \n" +
                "        BEGIN \n" +
                "            FETCH next FROM cursor_oficinas INTO @temporal_oficina \n" +
                "\n" +
                "            INSERT INTO @list_oficinas \n" +
                "            VALUES      (@iterator_oficinas, \n" +
                "                         @temporal_oficina) \n" +
                "\n" +
                "            SET @iterator_oficinas=@iterator_oficinas + 1 \n" +
                "        END \n" +
                "\n" +
                "      CLOSE cursor_oficinas \n" +
                "\n" +
                "      DEALLOCATE cursor_oficinas \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --END CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      DECLARE @string_oficinas VARCHAR(max) \n" +
                "      DECLARE @it INT \n" +
                "\n" +
                "      SET @it=1 \n" +
                "\n" +
                "      WHILE @it <= (SELECT Count(*) \n" +
                "                    FROM   @list_oficinas) \n" +
                "        BEGIN \n" +
                "            SET @string_oficinas=Concat(@string_oficinas, \n" +
                "                                 + ' ' \n" +
                "                                 + (SELECT oficina \n" +
                "                                    FROM   @list_oficinas \n" +
                "                                    WHERE  id = @it) \n" +
                "                                 + ',') \n" +
                "            SET @it=@it + 1 \n" +
                "        END \n" +
                "\n" +
                "      INSERT INTO @list_visitas \n" +
                "      VALUES      (@dpi, \n" +
                "                   @nombre1, \n" +
                "                   @nombre2, \n" +
                "                   @apellido1, \n" +
                "                   @apellido2, \n" +
                "                   @municipio, \n" +
                "                   @departamento, \n" +
                "                   @hora_entrada, \n" +
                "                   @fecha, \n" +
                "                   @string_oficinas ) \n" +
                "\n" +
                "      DELETE FROM @list_oficinas \n" +
                "\n" +
                "      SET @string_oficinas='' \n" +
                "      SET @iterator_visitas=@iterator_visitas + 1 \n" +
                "  END \n" +
                "\n" +
                "SELECT * \n" +
                "FROM   @list_visitas \n" +
                "\n" +
                "CLOSE cursor_visitas \n" +
                "\n" +
                "DEALLOCATE cursor_visitas ";
         */
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, oficina);
        stmt.setDate(2, Date.valueOf(dateBeginning));
        stmt.setDate(3, Date.valueOf(dateEnding));
        /*stmt.setInt(4, oficina);
        stmt.setDate(5, Date.valueOf(dateBeginning));
        stmt.setDate(6, Date.valueOf(dateEnding));*/
        return stmt.executeQuery();
    }

    public ResultSet reportsByDpto(int dpto, String date) throws SQLException {
        /*String sql =
                "SELECT dpi, nombre1, nombre2, apellido1, apellido2, municipio, departamento, hora_entrada, fecha, oficina FROM detalle_visitas\n" +
                        "inner join visitas on fk_id_visitas=visitas.id " +
                        "inner join oficinas on fk_id_oficina=oficinas.id "+
                        "inner join visitantes on fk_dpi_visitantes=visitantes.dpi " +
                        "inner join municipios on fk_id_municipio=municipios.id " +
                        "inner join departamentos on fk_id_departamentos=departamentos.id " +
                        "WHERE  fk_id_departamentos=? and fecha=? ORDER BY fecha";
         */

        String sql= "DECLARE @list_visitas TABLE \n" +
                "  ( \n" +
                "     dpi          BIGINT, \n" +
                "     nombre1      VARCHAR(50), \n" +
                "     nombre2      VARCHAR(50), \n" +
                "     apellido1    VARCHAR(50), \n" +
                "     apellido2    VARCHAR(50), \n" +
                "     municipio    VARCHAR(50), \n" +
                "     departamento VARCHAR(50), \n" +
                "     hora_entrada TIME(7), \n" +
                "     fecha        DATE, \n" +
                "     oficina      VARCHAR(max) \n" +
                "  ) \n" +
                "DECLARE @list_oficinas TABLE \n" +
                "  ( \n" +
                "     id      INT, \n" +
                "     oficina VARCHAR(250) \n" +
                "  ) \n" +
                "DECLARE @dpi          BIGINT, \n" +
                "        @nombre1      VARCHAR(50), \n" +
                "        @nombre2      VARCHAR(50), \n" +
                "        @apellido1    VARCHAR(50), \n" +
                "        @apellido2    VARCHAR(50), \n" +
                "        @municipio    VARCHAR(50), \n" +
                "        @departamento VARCHAR(50) \n" +
                "DECLARE @hora_entrada TIME(7) \n" +
                "DECLARE @fecha DATE \n" +
                "DECLARE @iterator_visitas INT \n" +
                "DECLARE @iterator_oficinas INT \n" +
                "DECLARE @temporal_id_visita INT \n" +
                "DECLARE @temporal_oficina VARCHAR(250) \n" +
                "\n" +
                "SET @iterator_visitas=1 \n" +
                "\n" +
                "DECLARE cursor_visitas CURSOR static read_only FOR \n" +
                "  SELECT dpi, \n" +
                "         nombre1, \n" +
                "         nombre2, \n" +
                "         apellido1, \n" +
                "         apellido2, \n" +
                "         municipio, \n" +
                "         departamento, \n" +
                "         hora_entrada, \n" +
                "         fecha, \n" +
                "         visitas.id \n" +
                "  FROM   visitas \n" +
                "         INNER JOIN visitantes \n" +
                "                 ON fk_dpi_visitantes = visitantes.dpi \n" +
                "         INNER JOIN municipios \n" +
                "                 ON fk_id_municipio = municipios.id \n" +
                "         INNER JOIN departamentos \n" +
                "                 ON fk_id_departamentos = departamentos.id \n" +
                "  WHERE  fk_id_departamentos=? and fecha=? ORDER BY fecha \n" +
                "\n" +
                "OPEN cursor_visitas \n" +
                "\n" +
                "WHILE @iterator_visitas <= (SELECT Count(*) \n" +
                "                            FROM   visitas WHERE fk_id_departamentos=? AND fecha=?) \n" +
                "  BEGIN \n" +
                "      FETCH next FROM cursor_visitas INTO @dpi, @nombre1, @nombre2, @apellido1, \n" +
                "      @apellido2, @municipio, @departamento, @hora_entrada, @fecha, \n" +
                "      @temporal_id_visita \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      SET @iterator_oficinas=1 \n" +
                "\n" +
                "      DECLARE cursor_oficinas CURSOR static read_only FOR \n" +
                "        SELECT oficina \n" +
                "        FROM   detalle_visitas \n" +
                "               INNER JOIN oficinas \n" +
                "                       ON fk_id_oficina = oficinas.id \n" +
                "        WHERE  fk_id_visitas = @temporal_id_visita \n" +
                "\n" +
                "      OPEN cursor_oficinas \n" +
                "\n" +
                "      WHILE @iterator_oficinas <= (SELECT @@CURSOR_ROWS) \n" +
                "        BEGIN \n" +
                "            FETCH next FROM cursor_oficinas INTO @temporal_oficina \n" +
                "\n" +
                "            INSERT INTO @list_oficinas \n" +
                "            VALUES      (@iterator_oficinas, \n" +
                "                         @temporal_oficina) \n" +
                "\n" +
                "            SET @iterator_oficinas=@iterator_oficinas + 1 \n" +
                "        END \n" +
                "\n" +
                "      CLOSE cursor_oficinas \n" +
                "\n" +
                "      DEALLOCATE cursor_oficinas \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --END CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      DECLARE @string_oficinas VARCHAR(max) \n" +
                "      DECLARE @it INT \n" +
                "\n" +
                "      SET @it=1 \n" +
                "\n" +
                "      WHILE @it <= (SELECT Count(*) \n" +
                "                    FROM   @list_oficinas) \n" +
                "        BEGIN \n" +
                "            SET @string_oficinas=Concat(@string_oficinas, \n" +
                "                                 + ' ' \n" +
                "                                 + (SELECT oficina \n" +
                "                                    FROM   @list_oficinas \n" +
                "                                    WHERE  id = @it) \n" +
                "                                 + ',') \n" +
                "            SET @it=@it + 1 \n" +
                "        END \n" +
                "\n" +
                "      INSERT INTO @list_visitas \n" +
                "      VALUES      (@dpi, \n" +
                "                   @nombre1, \n" +
                "                   @nombre2, \n" +
                "                   @apellido1, \n" +
                "                   @apellido2, \n" +
                "                   @municipio, \n" +
                "                   @departamento, \n" +
                "                   @hora_entrada, \n" +
                "                   @fecha, \n" +
                "                   @string_oficinas ) \n" +
                "\n" +
                "      DELETE FROM @list_oficinas \n" +
                "\n" +
                "      SET @string_oficinas='' \n" +
                "      SET @iterator_visitas=@iterator_visitas + 1 \n" +
                "  END \n" +
                "\n" +
                "SELECT * \n" +
                "FROM   @list_visitas \n" +
                "\n" +
                "CLOSE cursor_visitas \n" +
                "\n" +
                "DEALLOCATE cursor_visitas ";

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, dpto);
        stmt.setDate(2, Date.valueOf(date));
        stmt.setInt(4, dpto);
        stmt.setDate(5, Date.valueOf(date));
        return stmt.executeQuery();
    }

    public ResultSet reportsBetweenDatesDptos(
            int dpto,
            String dateBeginning,
            String dateEnding
    )
            throws SQLException {
        /*String sql =
                "SELECT dpi, nombre1, nombre2, apellido1, apellido2, municipio, departamento, hora_entrada, fecha, oficina FROM detalle_visitas\n" +
                        "inner join visitas on fk_id_visitas=visitas.id " +
                        "inner join oficinas on fk_id_oficina=oficinas.id "+
                        "inner join visitantes on fk_dpi_visitantes=visitantes.dpi " +
                        "inner join municipios on fk_id_municipio=municipios.id " +
                        "inner join departamentos on fk_id_departamentos=departamentos.id " +
                        "WHERE  fk_id_departamentos=? and fecha>=? and fecha<=? ORDER BY fecha";
         */

        String sql= "DECLARE @list_visitas TABLE \n" +
                "  ( \n" +
                "     dpi          BIGINT, \n" +
                "     nombre1      VARCHAR(50), \n" +
                "     nombre2      VARCHAR(50), \n" +
                "     apellido1    VARCHAR(50), \n" +
                "     apellido2    VARCHAR(50), \n" +
                "     municipio    VARCHAR(50), \n" +
                "     departamento VARCHAR(50), \n" +
                "     hora_entrada TIME(7), \n" +
                "     fecha        DATE, \n" +
                "     oficina      VARCHAR(max) \n" +
                "  ) \n" +
                "DECLARE @list_oficinas TABLE \n" +
                "  ( \n" +
                "     id      INT, \n" +
                "     oficina VARCHAR(250) \n" +
                "  ) \n" +
                "DECLARE @dpi          BIGINT, \n" +
                "        @nombre1      VARCHAR(50), \n" +
                "        @nombre2      VARCHAR(50), \n" +
                "        @apellido1    VARCHAR(50), \n" +
                "        @apellido2    VARCHAR(50), \n" +
                "        @municipio    VARCHAR(50), \n" +
                "        @departamento VARCHAR(50) \n" +
                "DECLARE @hora_entrada TIME(7) \n" +
                "DECLARE @fecha DATE \n" +
                "DECLARE @iterator_visitas INT \n" +
                "DECLARE @iterator_oficinas INT \n" +
                "DECLARE @temporal_id_visita INT \n" +
                "DECLARE @temporal_oficina VARCHAR(250) \n" +
                "\n" +
                "SET @iterator_visitas=1 \n" +
                "\n" +
                "DECLARE cursor_visitas CURSOR static read_only FOR \n" +
                "  SELECT dpi, \n" +
                "         nombre1, \n" +
                "         nombre2, \n" +
                "         apellido1, \n" +
                "         apellido2, \n" +
                "         municipio, \n" +
                "         departamento, \n" +
                "         hora_entrada, \n" +
                "         fecha, \n" +
                "         visitas.id \n" +
                "  FROM   visitas \n" +
                "         INNER JOIN visitantes \n" +
                "                 ON fk_dpi_visitantes = visitantes.dpi \n" +
                "         INNER JOIN municipios \n" +
                "                 ON fk_id_municipio = municipios.id \n" +
                "         INNER JOIN departamentos \n" +
                "                 ON fk_id_departamentos = departamentos.id \n" +
                "  WHERE  fk_id_departamentos=? \n" +
                "         AND fecha >=? \n" +
                "         AND fecha <=? \n" +
                "  ORDER  BY fecha \n" +
                "\n" +
                "OPEN cursor_visitas \n" +
                "\n" +
                "WHILE @iterator_visitas <= (SELECT Count(*) \n" +
                "                            FROM   visitas WHERE fk_id_departamentos=? " +
                "                               AND fecha>=? " +
                "                               AND fecha<=?) \n" +
                "  BEGIN \n" +
                "      FETCH next FROM cursor_visitas INTO @dpi, @nombre1, @nombre2, @apellido1, \n" +
                "      @apellido2, @municipio, @departamento, @hora_entrada, @fecha, \n" +
                "      @temporal_id_visita \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      SET @iterator_oficinas=1 \n" +
                "\n" +
                "      DECLARE cursor_oficinas CURSOR static read_only FOR \n" +
                "        SELECT oficina \n" +
                "        FROM   detalle_visitas \n" +
                "               INNER JOIN oficinas \n" +
                "                       ON fk_id_oficina = oficinas.id \n" +
                "        WHERE  fk_id_visitas = @temporal_id_visita \n" +
                "\n" +
                "      OPEN cursor_oficinas \n" +
                "\n" +
                "      WHILE @iterator_oficinas <= (SELECT @@CURSOR_ROWS) \n" +
                "        BEGIN \n" +
                "            FETCH next FROM cursor_oficinas INTO @temporal_oficina \n" +
                "\n" +
                "            INSERT INTO @list_oficinas \n" +
                "            VALUES      (@iterator_oficinas, \n" +
                "                         @temporal_oficina) \n" +
                "\n" +
                "            SET @iterator_oficinas=@iterator_oficinas + 1 \n" +
                "        END \n" +
                "\n" +
                "      CLOSE cursor_oficinas \n" +
                "\n" +
                "      DEALLOCATE cursor_oficinas \n" +
                "\n" +
                "      ----------------------------------      \n" +
                "      --END CURSOR TEMPORAL DE OFICINAS      \n" +
                "      ----------------------------------      \n" +
                "      DECLARE @string_oficinas VARCHAR(max) \n" +
                "      DECLARE @it INT \n" +
                "\n" +
                "      SET @it=1 \n" +
                "\n" +
                "      WHILE @it <= (SELECT Count(*) \n" +
                "                    FROM   @list_oficinas) \n" +
                "        BEGIN \n" +
                "            SET @string_oficinas=Concat(@string_oficinas, \n" +
                "                                 + ' ' \n" +
                "                                 + (SELECT oficina \n" +
                "                                    FROM   @list_oficinas \n" +
                "                                    WHERE  id = @it) \n" +
                "                                 + ',') \n" +
                "            SET @it=@it + 1 \n" +
                "        END \n" +
                "\n" +
                "      INSERT INTO @list_visitas \n" +
                "      VALUES      (@dpi, \n" +
                "                   @nombre1, \n" +
                "                   @nombre2, \n" +
                "                   @apellido1, \n" +
                "                   @apellido2, \n" +
                "                   @municipio, \n" +
                "                   @departamento, \n" +
                "                   @hora_entrada, \n" +
                "                   @fecha, \n" +
                "                   @string_oficinas ) \n" +
                "\n" +
                "      DELETE FROM @list_oficinas \n" +
                "\n" +
                "      SET @string_oficinas='' \n" +
                "      SET @iterator_visitas=@iterator_visitas + 1 \n" +
                "  END \n" +
                "\n" +
                "SELECT * \n" +
                "FROM   @list_visitas \n" +
                "\n" +
                "CLOSE cursor_visitas \n" +
                "\n" +
                "DEALLOCATE cursor_visitas ";

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, dpto);
        stmt.setDate(2, Date.valueOf(dateBeginning));
        stmt.setDate(3, Date.valueOf(dateEnding));
        stmt.setInt(4, dpto);
        stmt.setDate(5, Date.valueOf(dateBeginning));
        stmt.setDate(6, Date.valueOf(dateEnding));
        return stmt.executeQuery();
    }
   /*=================================
  END REPORTES
  ==================================*/


}
