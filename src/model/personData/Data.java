package model.personData;

//import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import java.sql.Blob;

public class Data {
  protected static String[] data = new String[11];
  protected static Blob huellaa;

  public Data(
    int id,
    String dpi,
    Blob huella,
    String nombre1,
    String nombre2,
    String apellido1,
    String apellido2,
    int idDpto,
    int idMun,
    String dpto,
    String mun,
    String tel
  ) {
    huellaa = huella;
    data[0] = Integer.toString(id);
    data[1] = dpi;
    data[2] = nombre1;
    data[3] = nombre2;
    data[4] = apellido1;
    data[5] = apellido2;
    data[6] = Integer.toString(idDpto);
    data[7] = Integer.toString(idMun);
    data[8] = dpto;
    data[9] = mun;
    data[10] = tel;
  }

  public static String[] getData() {
    return data;
  }

  public static Blob getHuella() {
    return huellaa;
  }
}
