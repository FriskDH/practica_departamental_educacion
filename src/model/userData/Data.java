package model.userData;

import java.util.*;

public class Data {
  //    protected static Dictionary user;
  protected static HashMap<String, String> user;
  protected static List<Integer> permissionsList;
  protected static List<String> permissionsNameList;

  public Data(
    int id,
    String dpi,
    String nombre1,
    String apellido1,
    String oficina,
    int idRol,
    String rol
  ) {
    //        user= new Hashtable();
    user = new HashMap<>();
    user.put("ID", Integer.toString(id));
    user.put("DPI", dpi);
    user.put("Nombre", nombre1);
    user.put("Apellido", apellido1);
    user.put("Oficina", oficina);
    user.put("Rol", rol);
    permissionsList = new LinkedList<>();
    permissionsNameList = new LinkedList<>();
  }

  public static void setAccess(int key, String name) {
    permissionsList.add(key);
    permissionsNameList.add(name);
  }

  public static HashMap<String, String> getAccessHashT() {
    return user;
  }

  public static List<Integer> getAccessList() {
    return permissionsList;
  }

  public static List<String> getAccessNameList() {
    return permissionsNameList;
  }

  public static void clear() {
    user.clear();
    permissionsNameList.clear();
    permissionsList.clear();
  }

}
